/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "PainterPanel.cpp"
//! \brief Implementation file for PainterPanel class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \version    1.0
//! \date       05.01.2012 (last updated)
//!

#include "PainterPanel.h"
#include <QtGui/QEvent.h>


namespace Frapper {

Q_DECLARE_METATYPE(AbstractParameter *)


///
/// Constructors and Destructors
///


//!
//! Constructor of the PainterPanel class.
//!
//! \param parent The parent widget the created instance will be a child of.
//! \param flags Extra widget options.
//!
PainterPanel::PainterPanel ( QWidget *parent /* = 0 */, Qt::WindowFlags flags /* = 0 */ ) :
    ViewPanel(ViewPanel::T_Painter, parent, flags)
{
    setupUi(this);
   
    m_hboxLayout = new QHBoxLayout();
    m_hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));

    //m_hboxLayout->addWidget(m_dataTree);

	ui_vboxLayout->addLayout(m_hboxLayout);

	connect(ui_clearLayerAction, SIGNAL(triggered()), SLOT(clearLayer(int)));
	connect(ui_penColorAction, SIGNAL(triggered()), SLOT(cangePenColor(QColor)));
}


//!
//! Destructor of the PainterPanel class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
PainterPanel::~PainterPanel ()
{
}


///
/// Public Functions
///


//!
//! Fills the given tool bars with actions for the painter panel.
//!
//! \param mainToolBar The main tool bar to fill with actions.
//! \param panelToolBar The panel tool bar to fill with actions.
//!
void PainterPanel::fillToolBars ( QToolBar *mainToolBar, QToolBar *panelToolBar )
{
	mainToolBar->addAction(ui_penColorAction);
	mainToolBar->addSeparator();
	mainToolBar->addAction(ui_penColorAction);
}


///
/// Public Slots
///



///
/// Privata Functions
///



///
/// Protected Events
///


//!
//! Handles key press events for the widget.
//!
//! \param event The description of the key event.
//!
void PainterPanel::keyPressEvent ( QKeyEvent *event )
{
    if (event->isAutoRepeat()) {
        event->ignore();
        return;
    }

   /* switch (event->key()) {
        case Qt::Key_A:
            ui_frameAllAction->trigger();
            break;
        case Qt::Key_F:
            ui_frameSelectedAction->trigger();
            break;
        case Qt::Key_H:
        case Qt::Key_Home:
            ui_homeAction->trigger();
            break;
        case Qt::Key_T:
            ui_scrollbarsAction->toggle();
            break;
        default:
            ViewPanel::keyPressEvent(event);
    }*/
}

} // end namespace Frapper