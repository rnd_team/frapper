/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "PainterPanel.h"
//! \brief Header file for PainterPanel class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \version    1.0
//! \date       05.01.2012 (last updated)
//!

#ifndef PAINTERPANEL_H
#define PAINTERPANEL_H

#include "FrapperPrerequisites.h"
#include "ViewPanel.h"
#include "ui_PainterPanel.h"
//#include "PainterGraphicsView.h"
#include "Node.h"
#include <QtGui/QHBoxLayout>
//#include <QtGui/QCheckBox>


namespace Frapper {

//!
//! Class for a panel that contains a curve editor widget for displaying
//! number parameters
//!
class FRAPPER_GUI_EXPORT PainterPanel : public ViewPanel, protected Ui::PainterPanel
{

    Q_OBJECT

public: // constructors and destructors

    //!
    //! Constructor of the PainterPanel class.
    //!
    //! \param parent The parent widget the created instance will be a child of.
    //! \param flags Extra widget options.
    //!
    PainterPanel ( QWidget *parent = 0, Qt::WindowFlags flags = 0 );

    //!
    //! Destructor of the PainterPanel class.
    //!
    //! Defined virtual to guarantee that the destructor of a derived class
    //! will be called if the instance of the derived class is saved in a
    //! variable of its parent class type.
    //!
    virtual ~PainterPanel ();

public: // functions

    //!
    //! Fills the given tool bars with actions for the curve editor panel.
    //!
    //! \param mainToolBar The main tool bar to fill with actions.
    //! \param panelToolBar The panel tool bar to fill with actions.
    //!
    virtual void fillToolBars ( QToolBar *mainToolBar, QToolBar *panelToolBar );

protected: // events

    //!
    //! Handles key press events for the widget.
    //!
    //! \param event The description of the key event.
    //!
    void keyPressEvent ( QKeyEvent *event );

private: // data

    //!
    //! The layout for horizontal arranging the tree an drawing region.
    //!
    QHBoxLayout *m_hboxLayout;
};

} // end namespace Frapper

#endif