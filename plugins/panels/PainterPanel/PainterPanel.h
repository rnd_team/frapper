/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

//!
//! \file "PainterPanel.h"
//! \brief Header file for PainterPanel class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \author     Nils Zweiling <nils.zweiling@filmakademie.de>
//! \author     Sebastian Bach <sebastian.bach@filmakademie.de>
//! \version    1.0
//! \date       24.06.2012 (last updated)
//!

#ifndef PAINTERPANEL_H
#define PAINTERPANEL_H

#include "FrapperPrerequisites.h"
#include "ViewPanel.h"
#include "ImageNode.h"
#include "RenderNode.h"
#include "Parameter.h"
#include "NumberParameter.h"
#include "FilenameParameter.h"
#include "EnumerationParameter.h"
#include "ParameterPlugin.h"
#include "PainterGraphicsView.h"
#include <QListWidget>
#include <QLabel>
#include <QItemSelection>
#include <QHBoxLayout>
#include <QListWidget>


namespace Frapper {

//!
//! Class representing a view for displaying and editing parameters of nodes.
//!
class PainterPanel : public ViewPanel
{

    Q_OBJECT

public: // constructors and destructors

    //!
    //! Constructor of the PainterPanel class.
    //!
    //! \param parent The parent widget the created instance will be a child of.
    //! \param flags Extra widget options.
    //!
    PainterPanel ( QWidget *parent = 0, Qt::WindowFlags flags = 0 );

    //!
    //! Destructor of the PainterPanel class.
    //!
    //! Defined virtual to guarantee that the destructor of a derived class
    //! will be called if the instance of the derived class is saved in a
    //! variable of its parent class type.
    //!
    virtual ~PainterPanel ();

public slots: //


public: // functions


	//!
	//! Connects the panel with the scene.
	//!
	//! \param *nodeModel NodeModel of the scene
	//! \param *sceneModel SceneModel of the scene
	//!
	virtual void registerControl(NodeModel *nodeModel, SceneModel *sceneModel);

	//!
	//! Fills the given tool bars with actions for the PainterPanel view.
	//!
	//! \param mainToolBar The main tool bar to fill with actions.
	//! \param panelToolBar The panel tool bar to fill with actions.
	//!
	void fillToolBars ( QToolBar *mainToolBar, QToolBar *panelToolBar );

private slots: //

	//!
	//! Is called a node is selected
	//!
	//! \param selecedNode the selected wich was selected
	//!
	void updateSelectedNode(Node * selectedNode);

	//!
	//! Updates the list of images if the node model changes
	//!
	void updateImageList();

	//!
	//! 
	//!
	void update(QListWidgetItem *clickedItem);

	//!
	//! 
	//!
	void updateTarget(QListWidgetItem *clickedItem);

	//!
	//! Updates the scene and the panel if the node selection changes
	//!
	//! \param enabled True if selected, false otherwise
	//!
	void selectionChanged(bool selected);

	//!
	//! Updates the scene and the panel if a node is selected in the panel
	//!
	void nodeSelected(QListWidgetItem * listItem);

	//!
	//! Stores the painted scene and the background image to the nodes render target
	//!
	void storeToNode();

signals: //
	void triggerRedraw();

private: // data


	//!
	//! Node model
	//!
	NodeModel *m_nodeModel;

	//!
	//! selected node
	//!
	ImageNode *m_imageNode;

	//!
	//! Scene model
	//!
	SceneModel *m_sceneModel;

	PainterGraphicsView *m_painterArea;

	QHBoxLayout *m_hboxLayout;

	QListWidget *m_imageListWidget;

	QIcon m_inIcon;
	QIcon m_outIcon;
	QIcon m_inOutIcon;

};

} // end namespace Frapper

#endif
