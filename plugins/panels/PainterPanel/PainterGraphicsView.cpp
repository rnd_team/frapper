/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "PainterGraphicsView.cpp"
//! \brief Implementation file for PainterGraphicsView class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \version    1.0
//! \date       16.01.2012 (last updated)
//!

#include "PainterGraphicsView.h"
#include <QtGui/QPainter>
#include <QtGui/QScrollBar>
#include <QtGui/QGraphicsScene>
#include <QtGui/QWheelEvent>


namespace Frapper {

Q_DECLARE_METATYPE(AbstractParameter *)


///
/// Constructors and Destructors
///


//!
//! Constructor of the PainterGraphicsView class.
//!
//! \param parent The parent widget the created instance will be a child of.
//! \param flags Extra widget options.
//!
PainterGraphicsView::PainterGraphicsView ( QWidget *parent /* = 0 */, Qt::WindowFlags flags /* = 0 */ ) :
    BaseGraphicsView(parent),
	m_scribbling(false),
	m_penColor(0,0,255),
	m_lastPoint(0,0)
{
    QGraphicsScene *graphicsScene = new QGraphicsScene(this);
    graphicsScene->setSceneRect(0, 0, width(), height());

    setScene(graphicsScene);

	centerOn(0,scene()->sceneRect().height()/2);

    //setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setTransformationAnchor(AnchorViewCenter);
    setResizeAnchor(AnchorViewCenter);

	m_bgImage = QImage(16, 16, QImage::Format_RGB888);
	m_bgImage.fill(qRgb(0, 0, 0));

	m_fgImage = QImage(16, 16, QImage::Format_ARGB32);
	m_fgImage.fill(qRgba(0, 0, 0, 0));
}


//!
//! Destructor of the PainterGraphicsView class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
PainterGraphicsView::~PainterGraphicsView ()
{
}


///
/// Event Handlers
///

//!
//! The overwritten the event handler for the drag move event.
//!
//! \param event The description of the drag move event.
//!
void PainterGraphicsView::mouseMoveEvent ( QMouseEvent * event )
{
	if ((event->buttons() & Qt::LeftButton) && m_scribbling) {
		drawLineTo(event->pos());
		emit mouseRelease();
	}
}

void PainterGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_lastPoint = event->pos();
        m_scribbling = true;
    }
}

void PainterGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && m_scribbling) {
        drawLineTo(event->pos());
        m_scribbling = false;
    }
	emit mouseRelease();
}


//!
//! The overwritten the event handler for resize events.
//! Adds scene resizing and redrawing.
//!
//! \param event The description of the key event.
//!
void PainterGraphicsView::resizeEvent ( QResizeEvent *event )
{
	scene()->setSceneRect(0, 0, width(), height());
	centerOn(0,scene()->sceneRect().height()/2);
    QGraphicsView::resizeEvent(event);
}

//!
//! The overwritten the event handler for background drawing events.
//! Adds coordinate system to the background.
//!
//! \param painter The QT painter object.
//! \param rect The drawing region painter object.
//!
void PainterGraphicsView::drawBackground (QPainter *painter, const QRectF &rect)
{
	painter->drawImage(sceneRect(), m_bgImage);
}

//!
//! The overwritten the event handler for foreground drawing events.
//! Adds coordinate system to the background.
//!
//! \param painter The QT painter object.
//! \param rect The drawing region painter object.
//!
void PainterGraphicsView::drawForeground (QPainter *painter, const QRectF &rect)
{
	painter->drawImage(sceneRect(), m_fgImage);
}


QImage *PainterGraphicsView::initImage (uchar *data, int width, int height, QImage::Format format)
{
	m_bgImage = QImage(data, width, height, format);
	m_fgImage = QImage(width, height, QImage::Format_ARGB32);
	m_fgImage.fill(qRgba(0, 0, 0, 0));
	return &m_bgImage;
}

QImage &PainterGraphicsView::getImage ()
{
	return m_fgImage;
}

QImage &PainterGraphicsView::getBgImage ()
{
	return m_bgImage;
}


///
/// Public Slots
///


//!
//! Resets the network graphics view's matrix.
//!
void PainterGraphicsView::homeView ()
{
    setScale(1);
	QRectF boundingRect = scene()->itemsBoundingRect();
    centerOn(boundingRect.left() + boundingRect.width() / 2, boundingRect.top() + boundingRect.height() / 2);

	scaleSceneItems(1.0/matrix().m11());
}


//!
//! Changes the viewing transformation so that all items are visible at maximum
//! zoom level.
//!
void PainterGraphicsView::frameAll ()
{
    frame(scene()->itemsBoundingRect());
	scaleSceneItems(1.0/matrix().m11());
}


//!
//! Changes the viewing transformation so that the selected items are visible
//! at maximum zoom level.
//!
void PainterGraphicsView::frameSelected ()
{
    // make sure there is a selection
    QList<QGraphicsItem *> selectedItems = scene()->selectedItems();
    if (selectedItems.size() == 0)
        return;

    // obtain the bounding rectangle that encompasses all selected items
    QRectF boundingRect;
    foreach (QGraphicsItem *item, selectedItems)
        boundingRect = boundingRect.united(item->sceneBoundingRect());

    frame(boundingRect);

	scaleSceneItems(1.0/matrix().m11());
}

//!
//! Event handler for mouse wheel events.
//!
//! \param event The description of the mouse wheel event.
//!
void PainterGraphicsView::wheelEvent ( QWheelEvent *event )
{
	BaseGraphicsView::wheelEvent(event);
	if (event->delta() != 0 && !scene()->items().empty())
		scaleSceneItems(1.0/matrix().m11());
}

//private functions

//!
//! Scales the graphics items in the scene by given value.
//!
//! \param value The value for scaling the scene items.
//!
void PainterGraphicsView::scaleSceneItems ( const float value )
{
	// noob
}

void PainterGraphicsView::drawLineTo(const QPoint &endPoint)
{
	static const int myPenWidth = 3;
    QPainter painter(&m_fgImage);

    painter.setPen(QPen(m_penColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	painter.drawLine(m_lastPoint, endPoint);

    int rad = (myPenWidth / 2) + 2;
    //viewport()->update(QRect(m_lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	viewport()->update();
    m_lastPoint = endPoint;
}

} // end namespace Frapper