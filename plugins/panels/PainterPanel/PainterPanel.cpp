/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

//!
//! \file "PainterPanel.cpp"
//! \brief Implementation file for PainterPanel class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \author     Nils Zweiling <nils.zweiling@filmakademie.de>
//! \author     Sebastian Bach <sebastian.bach@filmakademie.de>
//! \version    1.0
//! \date       07.12.2012 (last updated)
//!

#include "PainterPanel.h"
#include "ParameterTabPage.h"
#include "NodeFactory.h"
#include "Log.h"


Q_DECLARE_METATYPE(Parameter *)

///
/// Constructors and Destructors
///

//!
//! Constructor of the PainterPanel class.
//!
//! \param parent The parent widget the created instance will be a child of.
//! \param flags Extra widget options.
//!
PainterPanel::PainterPanel ( QWidget *parent /* = 0 */, Qt::WindowFlags flags /* = 0 */ ) :
    ViewPanel(ViewPanel::T_PluginPanel, parent, flags),
	m_imageNode(0)
{

	// Creation of labels and List Widgets
	m_hboxLayout = new QHBoxLayout(this);
    m_hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
	m_hboxLayout->setSpacing(3);

	m_painterArea = new PainterGraphicsView(this);
	
	m_imageListWidget = new QListWidget(this);
	m_imageListWidget->setMaximumWidth(150);
	m_imageListWidget->setModelColumn(3);

	m_hboxLayout->addWidget(m_imageListWidget);
	m_hboxLayout->addWidget(m_painterArea);

	m_inIcon.addFile(QString::fromUtf8(":/forwardIcon"), QSize(), QIcon::Normal, QIcon::Off);
	m_outIcon.addFile(QString::fromUtf8(":/backwardIcon"), QSize(), QIcon::Normal, QIcon::Off);
	m_inOutIcon.addFile(QString::fromUtf8(":/undoIcon"), QSize(), QIcon::Normal, QIcon::Off);

	QObject::connect(m_painterArea, SIGNAL(mouseRelease()), this, SLOT(storeToNode()));
}


//!
//! Destructor of the NetworkPanel class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
PainterPanel::~PainterPanel ()
{
}

///
/// Public Funcitons
///


//!
//! Connects the panel with the scene.
//!
//! \param *nodeModel NodeModel of the scene
//! \param *sceneModel SceneModel of the scene
//!
void PainterPanel::registerControl(NodeModel *nodeModel, SceneModel *sceneModel)
{
	m_nodeModel = nodeModel;
	m_sceneModel = sceneModel;
	
	QObject::connect(m_nodeModel, SIGNAL(nodeSelected(Node *)), this, SLOT(updateSelectedNode(Node *)));
	QObject::connect(m_sceneModel, SIGNAL(selectionChanged(bool)), this, SLOT(selectionChanged(bool)));
	QObject::connect(this, SIGNAL(triggerRedraw()), m_sceneModel, SLOT(redrawTriggered()));
	//QObject::connect(m_sceneModel, SIGNAL(modified()), this, SLOT(update()));
	//QObject::connect(m_sceneModel, SIGNAL(objectCreated(const QString &)), this, SLOT(update()));

	QObject::connect(m_imageListWidget, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(update(QListWidgetItem *)));
	QObject::connect(m_imageListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(updateTarget(QListWidgetItem *)));
}


//!
//! Fills the given tool bars with actions for the PainterPanel view.
//!
//! \param mainToolBar The main tool bar to fill with actions.
//! \param panelToolBar The panel tool bar to fill with actions.
//!
void PainterPanel::fillToolBars ( QToolBar *mainToolBar, QToolBar *panelToolBar )
{
	QAction *ui_descriptionAction;
	ui_descriptionAction = new QAction(this);
    ui_descriptionAction->setObjectName(QString::fromUtf8("ui_descriptionAction"));
    QIcon icon1;
    icon1.addFile(QString::fromUtf8(":/infoIcon"), QSize(), QIcon::Normal, QIcon::Off);
    ui_descriptionAction->setIcon(icon1);
	ui_descriptionAction->setToolTip("Show Description");

	mainToolBar->addAction(ui_descriptionAction);
}

///
/// Private Slots
///

//!
//! Is called a node is selected
//!
//! \param selecedNode the selected wich was selected
//!
void PainterPanel::updateSelectedNode(Node * selectedNode)
{
	m_imageNode = dynamic_cast<ImageNode *>(selectedNode);
	updateImageList();
}

//!
//! Updates the list of images if the node model changes
//!
void PainterPanel::updateImageList()
{
	if (m_imageNode) {
		AbstractParameter::List *rootList = m_imageNode->getParameterRoot()->getParameterList();

		foreach (AbstractParameter *absParameter, *rootList) {
			Parameter *parameter = dynamic_cast<Parameter *>(absParameter);
			if (parameter) {
				if (parameter->getType() == Parameter::T_Image) {
					Ogre::TexturePtr texPtr = parameter->getValue(true).value<Ogre::TexturePtr>();
					QListWidgetItem *newItem = new QListWidgetItem(parameter->getName(), m_imageListWidget);
					newItem->setData(1, QVariant::fromValue<Parameter *>(parameter));
					m_imageListWidget->addItem(newItem);
				}
			}
		}
	}
}

//!
//! Updates the panel if the node model changes
//!
void PainterPanel::update(QListWidgetItem *clickedItem)
{
	Parameter *imageParameter = clickedItem->data(1).value<Parameter *>();
	if (imageParameter) {
		Ogre::TexturePtr imgPtr = imageParameter->getValue(true).value<Ogre::TexturePtr>();
		
		if (imgPtr.isNull())
			return;
		
		Ogre::HardwarePixelBufferSharedPtr srcBuffer = imgPtr->getBuffer();
		srcBuffer->lock(Ogre::HardwareBuffer::HBL_NORMAL);

		const Ogre::PixelBox &pixBox = srcBuffer->getCurrentLock();
		const Ogre::PixelFormat format = pixBox.format;
		const int dx = pixBox.getWidth();
		const int dy = pixBox.getHeight();
		//const int dz = Ogre::PixelUtil::getComponentCount(format);
		//const int nbrBits = Ogre::PixelUtil::getNumElemBits(format);

		// source buffer data
		uchar *vData = static_cast<uchar *>(pixBox.data);

		// init target image
		m_painterArea->initImage(vData, dx, dy, QImage::Format_ARGB32);

		srcBuffer->unlock();
		m_painterArea->viewport()->update();

		clickedItem->setIcon(m_outIcon);
	}
}

//!
//! 
//!
void PainterPanel::updateTarget(QListWidgetItem *clickedItem)
{
	clickedItem->setIcon(m_inIcon);
}

//!
//! Updates the scene and the panel if the node selection changes
//!
//! \param enabled True if selected, false otherwise
//!
void PainterPanel::selectionChanged(bool selected)
{
	if (!selected)
		m_imageListWidget->clear();
}

//!
//! Updates the scene and the panel if a node is selected in the panel
//!
void PainterPanel::nodeSelected(QListWidgetItem * listItem)
{
	QString nodeName = listItem->text();
	Node *selectedNode = m_nodeModel->getNode(nodeName);
	m_sceneModel->selectObject(nodeName);
}

//!
//! Stores the painted scene and the background image to the nodes internal render target
//!
void PainterPanel::storeToNode()
{
	if (!m_imageNode)
		return;

	RenderNode *renderNode = dynamic_cast<RenderNode *>(m_imageNode);
	
	if (renderNode) { 
		/*fill me!*/ 
	}
	
	else {
		QImage &bkImage = m_painterArea->getBgImage();
		QImage &fgImage = m_painterArea->getImage();
		Ogre::TexturePtr ptr = m_imageNode->getImage();
		
		Ogre::HardwarePixelBufferSharedPtr tgtBuffer = ptr->getBuffer();
		tgtBuffer->blitFromMemory(Ogre::PixelBox(fgImage.width(), fgImage.height(), 1, Ogre::PF_A8R8G8B8, fgImage.bits()));
	}

	emit triggerRedraw();
}
