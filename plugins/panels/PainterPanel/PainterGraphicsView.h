/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "PainterGraphicsView.h"
//! \brief Header file for PainterGraphicsView class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \version    1.0
//! \date       16.01.2012 (last updated)
//!

#ifndef PAINTERGRAPHICSVIEW_H
#define PAINTERGRAPHICSVIEW_H

#include "FrapperPrerequisites.h"
#include "BaseGraphicsView.h"
#include "ParameterGroup.h"
#include "NumberParameter.h"
#include <QtGui/QGraphicsView>
#include <QtGui/QPen>
#include <QtGui/QPainter>
#include <QtGui/QpainterPath>


namespace Frapper {


class PainterGraphicsView : public BaseGraphicsView
{
    Q_OBJECT

public: // constructors and destructors

    //!
    //! Constructor of the PainterGraphicsView class.
    //!
    //! \param parent The parent widget the created instance will be a child of.
    //! \param flags Extra widget options.
    //!
    PainterGraphicsView ( QWidget *parent = 0, Qt::WindowFlags flags = 0 );

    //!
    //! Destructor of the PainterGraphicsView class.
    //!
    //! Defined virtual to guarantee that the destructor of a derived class
    //! will be called if the instance of the derived class is saved in a
    //! variable of its parent class type.
    //!
    virtual ~PainterGraphicsView ();

public: // functions


    QImage *initImage (uchar *data, int width, int height, QImage::Format format);

	QImage &getImage ();

	QImage &getBgImage ();

public slots: //

	//!
    //! Resets the network graphics view's matrix.
    //!
    void homeView ();

    //!
    //! Changes the viewing transformation so that all items are visible at maximum
    //! zoom level.
    //!
    void frameAll ();

    //!
    //! Changes the viewing transformation so that the selected items are visible
    //! at maximum zoom level.
    //!
    void frameSelected ();


signals:
	//!
	//! Signal that is emitted when a drag event is emited
	//!
	void drag();

	void mouseRelease();

protected: // event handlers

	//!
    //! The overwritten the event handler for the drag move event.
    //!
    //! \param event The description of the drag move event.
    //!
	virtual void mouseMoveEvent ( QMouseEvent * event );

	virtual void mousePressEvent(QMouseEvent *event);

	virtual void mouseReleaseEvent(QMouseEvent *event);


    //!
    //! The overwritten the event handler for resize events.
    //! Adds scene resizing and redrawing.
    //!
    //! \param event The description of the key event.
    //!
    virtual void resizeEvent ( QResizeEvent *event );

    //!
    //! The overwritten the event handler for background drawing events.
    //! Adds coordinate system to the background.
    //!
    //! \param painter The QT painter object.
    //! \param rect The drawing region painter object.
    //!
	virtual void drawForeground (QPainter *painter, const QRectF &rect);

	//!
    //! The overwritten the event handler for background drawing events.
    //! Adds coordinate system to the background.
    //!
    //! \param painter The QT painter object.
    //! \param rect The drawing region painter object.
    //!
    virtual void drawBackground (QPainter *painter, const QRectF &rect);

	//!
	//! Event handler for mouse wheel events.
	//!
	//! \param event The description of the mouse wheel event.
	//!
	virtual void wheelEvent ( QWheelEvent *event );

protected: // functions


private: // functions
	//!
	//! Scales the graphics items in the scene by given value.
	//!
	//! \param value The value for scaling the scene items.
	//!
	void scaleSceneItems ( const float value );

	void drawLineTo(const QPoint &endPoint);

private: // data

	bool m_scribbling;

	QImage m_bgImage;

	QImage m_fgImage;

	QColor m_penColor;

	QPoint m_lastPoint;
};

} // end namespace Frapper

#endif