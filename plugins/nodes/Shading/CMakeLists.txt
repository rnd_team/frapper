project(Shading)

set( VS_PROJECT_FOLDER "Nodes/Shading" )
add_subdirectory(AmbientOcclusion)
add_subdirectory(GeometryBlur)
add_subdirectory(LightWarping)
add_subdirectory(NormalFlatten)
add_subdirectory(SurfaceCurvature)


