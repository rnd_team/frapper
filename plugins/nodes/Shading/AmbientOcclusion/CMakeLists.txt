project(ambientocclusion)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# ambientocclusion
set( ambientocclusion_h ${ambientocclusion_h}		
			AmbientOcclusionNode.h
			AmbientOcclusionNodePlugin.h
			)

set( ambientocclusion_moc ${ambientocclusion_moc}	
			AmbientOcclusionNode.h
			AmbientOcclusionNodePlugin.h
			)

set( ambientocclusion_src ${ambientocclusion_src}	
			AmbientOcclusionNode.cpp
			AmbientOcclusionNodePlugin.cpp
			)

set( ambientocclusion_res ${ambientocclusion_res}	
			ambientocclusion.xml
			)

# Create moc files		   
qt4_wrap_cpp(ambientocclusion_cxx ${ambientocclusion_moc})

# Create source groups
source_group("Moc Files" FILES ${ambientocclusion_cxx})
source_group("Header Files" FILES ${ambientocclusion_h})
source_group("Resources" FILES ${ambientocclusion_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(ambientocclusion_src ${ambientocclusion_src} ${ambientocclusion_h} ${ambientocclusion_res})
ENDIF(WIN32)

# Create static library
add_library(ambientocclusion SHARED ${ambientocclusion_src} ${ambientocclusion_cxx})

# Add library dependencies
target_link_libraries(ambientocclusion
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
install( FILES ${ambientocclusion_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libambientocclusion_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libambientocclusion.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS ambientocclusion RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
