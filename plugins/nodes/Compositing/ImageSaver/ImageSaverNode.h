//!
//! \file "ImageSaverNode.h"
//! \brief Header file for ImageSaverNode class.
//!
//! \author     Christopher Lutz <clutz@animationsnippets.com>
//! \version    1.0
//! \date       15.01.2010
//!

#ifndef IMAGESAVERNODE_H
#define IMAGESAVERNODE_H

#include "Node.h"
#include "InstanceCounterMacros.h"


using namespace Frapper;

//!
//! Class representing nodes that save input images.
//!
//!
class ImageSaverNode : public Node
{

    Q_OBJECT
    ADD_INSTANCE_COUNTER

public: // constructors and destructors

    //!
    //! Constructor of the ImageFilterNode class.
    //!
    //! \param name The name for the new node.
    //! \param parameterRoot A copy of the parameter tree specific for the type of the node.
    //!
    ImageSaverNode ( const QString &name, ParameterGroup *parameterRoot );

    //!
    //! Destructor of the ImageFilterNode class.
    //!
    //! Defined virtual to guarantee that the destructor of a derived class
    //! will be called if the instance of the derived class is saved in a
    //! variable of its parent class type.
    //!
    virtual ~ImageSaverNode ();

protected: // static constants

	//!
	//! number of image files
	//!
	static const unsigned int NumImages;

private:
	//!
	//! Converts a texture to an image and then saves it to a file
	//!
	void saveImage(Ogre::TexturePtr tex, std::string filename);

private slots: //

    //!
    //! Saves the node's input images to file
    //!
    void saveAllImages ();

private: // member variables

    //!
    //! Image counter
    //!
    int m_currentFrame;
};

#endif
