//!
//! \file "ImageSaverNodePlugin.cpp"
//! \brief Implementation file for ImageSaverNodePlugin class.
//!
//! \author     Christopher Lutz <clutz@animationsnippets.com>
//! \version    1.0
//! \date       15.01.2010
//!

#include "ImageSaverNodePlugin.h"
#include "ImageSaverNode.h"
#include <QtCore/QtPlugin>


///
/// Public Functions
///


//!
//! Creates a node of this node type.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//! \return A pointer to the new node.
//!
Node * ImageSaverNodePlugin::createNode ( const QString &name, ParameterGroup *parameterRoot )
{
    return new ImageSaverNode(name, parameterRoot);
}


Q_EXPORT_PLUGIN2(imagesavernodeplugin, ImageSaverNodePlugin)
