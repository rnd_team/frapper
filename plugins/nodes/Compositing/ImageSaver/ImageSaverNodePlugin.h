//!
//! \file "ImageSaverNodePlugin.h"
//! \brief Header file for ImageSaverNodePlugin class.
//!
//! \author     Christopher Lutz <clutz@animationsnippets.com>
//! \version    1.0
//! \date       15.01.2010
//!

#ifndef IMAGESAVERNODEPLUGIN_H
#define IMAGESAVERNODEPLUGIN_H

#include "NodeTypeInterface.h"


//!
//! Plugin class for creating ImageSaverNode objects.
//!
class ImageSaverNodePlugin : public QObject, public NodeTypeInterface
{

    Q_OBJECT
    Q_INTERFACES(NodeTypeInterface)

public: // functions

    //!
    //! Creates a node of this node type.
    //!
    //! \param name The name for the new node.
    //! \param parameterRoot A copy of the parameter tree specific for the type of the node.
    //! \return A pointer to the new node.
    //!
    virtual Node * createNode ( const QString &name, ParameterGroup *parameterRoot );

};


#endif