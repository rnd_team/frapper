/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "MocapMeshNode.h"
//! \brief Implementation file for MocapMeshNode class.
//!
//! \author     Nils Zweiling <nils.zweiling@filmakademie.de>
//! \author     Stefan Habel <stefan.habel@filmakademie.de>
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \version    1.1
//! \date       22.03.2012 (last updated)
//!

#include "MocapMeshNode.h"
#include "OgreManager.h"
#include "OgreTools.h"
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QTime>

#define USE_ANIMATION_WEIGHTS

INIT_INSTANCE_COUNTER(MocapMeshNode)


///
/// Constructors and Destructors
///


//!
//! Constructor of the MocapMeshNode class.
//!
//! \param name The name to give the new mesh node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
MocapMeshNode::MocapMeshNode ( const QString &name, ParameterGroup *parameterRoot ) :
    GeometryNode(name, parameterRoot, "Mesh"),
    m_sceneNode(0),
    m_entity(0),
    m_entityContainer(0),
    m_oldResourceGroupName("")
{
    // set affections and functions
    addAffection("Geometry File", m_outputGeometryName);
    addAffection("Light Description File", m_outputGeometryName);
    setChangeFunction("Geometry File", SLOT(geometryFileChanged()));
    setChangeFunction("Light Description File", SLOT(loadLightDescriptionFile()));
    setCommandFunction("Geometry File", SLOT(geometryFileChanged()));
    setCommandFunction("Light Description File", SLOT(loadLightDescriptionFile()));
	setChangeFunction("useSaC", SLOT(geometryFileChanged()));
    setChangeFunction("Use Spline Interpolation", SLOT(setInterpolationMode()));
 
    connect(this, SIGNAL(frameChanged(int)), SLOT(updateAll()));

	m_manual = getBoolValue("manual");
	m_bones = getBoolValue("bones");
	m_manualMat = getBoolValue("manualMat");
	m_autoPose = getBoolValue("autoPose");

	// change functions for update flags
	setChangeFunction("manual", SLOT(setSwitches()));
	setChangeFunction("bones", SLOT(setSwitches()));
	setChangeFunction("manualMat", SLOT(setSwitches()));
	setChangeFunction("autoPose", SLOT(setSwitches()));

	//reset InitPos if no Trackingdata
	setChangeFunction("initPos", SLOT(resetInitPos()));
	getParameter("initPos")->setSelfEvaluating(true);

	

    // create parameter groups for animation parameters
    m_animationGroup = new ParameterGroup("Skeletal Animations");
    m_animationWeightGroup = new ParameterGroup("Skeletal Animation Weights");
    m_poseGroup = new ParameterGroup("Pose Animations");
    m_poseWeightGroup = new ParameterGroup("Pose Animation Weights");
    m_boneGroup = new ParameterGroup("Bone Parameter");
    m_materialGroup = new ParameterGroup("Material Parameter");
    m_objectIdGroup = new ParameterGroup("Object Id Parameter");
    parameterRoot->addParameter(m_animationGroup);
    parameterRoot->addParameter(m_animationWeightGroup);
    parameterRoot->addParameter(m_poseGroup);
    parameterRoot->addParameter(m_poseWeightGroup);
    parameterRoot->addParameter(m_boneGroup);
    parameterRoot->addParameter(m_materialGroup);
    parameterRoot->addParameter(m_objectIdGroup);

    INC_INSTANCE_COUNTER
}


//!
//! Destructor of the MocapMeshNode class.
//!
MocapMeshNode::~MocapMeshNode ()
{
    destroyEntity();
    OgreTools::destroyResourceGroup(m_oldResourceGroupName);
    emit viewNodeUpdated();

    DEC_INSTANCE_COUNTER
}


///
/// Public Slots
///

//!
//! Update an animation.
//!
void MocapMeshNode::updateAnimation ()
{
    if (m_manual)
        updateAll();
}


//!
//! Update bones.
//!
void MocapMeshNode::updateBones ()
{
    m_entityContainer->updateCopies();

    if (m_manual)
        updateAll();
}


//!
//! Update poses.
//!
void MocapMeshNode::updatePoses ()
{
    if (m_manual)
        updateAll();
}


//!
//! Update animation, bones and poses.
//!
void MocapMeshNode::updateAll ()
{
	// Bone stuff
	if (m_bones && m_entity && m_entity->hasSkeleton()) {
		// begin: update eyeRotation for ambient occlusion (Gaze)
		NumberParameter* Surp = getNumberParameter("Surp");
		NumberParameter* gazeLeft = getNumberParameter("GazeHLeft");
		NumberParameter* gazeRight = getNumberParameter("GazeHRight");
		NumberParameter* gazeDown = getNumberParameter("GazeVDown");
		NumberParameter* gazeUp = getNumberParameter("GazeVUp");
		NumberParameter* rotation1 = getNumberParameter("gazeDirection@RightEye");
		NumberParameter* rotation2 = getNumberParameter("gazeDirection@LeftEye");
		if (!getBoolValue("Global Material Parameter Update") && rotation1 != NULL && gazeLeft != NULL && gazeRight != NULL && rotation2 != NULL && gazeUp != NULL && gazeDown != NULL  && Surp != NULL) {
			float xValue = 127.5;
			float yValue = 127.5;
			float zValue = 127.5;
			xValue += Surp->getValue().toFloat() * 0.05 * 127.5 / 100.0;
			xValue -= gazeUp->getValue().toFloat() * 0.09 * 127.5 / 100.0;		
			xValue += gazeDown->getValue().toFloat() * 0.15 * 127.5 / 100.0;
			yValue += gazeLeft->getValue().toFloat() * 0.565539 * 127.5 / 100.0;
			yValue -= gazeRight->getValue().toFloat() * 0.565539 * 127.5 / 100.0;
			QColor* color = new QColor(xValue, yValue, zValue);
			rotation1->setValue(*color);
			rotation2->setValue(*color);
		}

		// Bone Occlusion
		static const float stretchSpeed = 0.5f;
		static const float moveSpeed = 0.2f;
		static const Ogre::String nameL = "MarkerJoint_LUL9";	
		static const Ogre::String nameR = "MarkerJoint_RUL9";
		NumberParameter* occlusionStretchY = 0;
		NumberParameter* occlusionMoveY = 0;

		Ogre::SkeletonInstance *skeletonInstance = m_entity->getSkeleton();
		if (skeletonInstance->hasBone(nameL)) {
			if ( (occlusionStretchY = getNumberParameter("occlusionStretchY@LeftEye")) &&
				(occlusionMoveY = getNumberParameter("occlusionMoveY@LeftEye")) ) {
					Ogre::Bone *bone = skeletonInstance->getBone(nameL);
					const Ogre::Vector3 &MarkerJoint_LUL9 = bone->getPosition();

					const float stretchY = stretchSpeed * MarkerJoint_LUL9.y + (1.15 - 7.367 * stretchSpeed);
					occlusionStretchY->setValue(stretchY);

					const float moveY = moveSpeed * MarkerJoint_LUL9.y - (7.367 * moveSpeed);
					occlusionMoveY->setValue(moveY);
			}
		}

		if (skeletonInstance->hasBone(nameR)) {
			if ( (occlusionStretchY = getNumberParameter("occlusionStretchY@RightEye")) &&
				(occlusionMoveY = getNumberParameter("occlusionMoveY@RightEye")) ) {
					Ogre::Bone *bone = skeletonInstance->getBone(nameR);
					const Ogre::Vector3 &MarkerJoint_RUL9 = bone->getPosition();

					const float stretchY = stretchSpeed * MarkerJoint_RUL9.y + (1.10 - 7.367 * stretchSpeed);
					occlusionStretchY->setValue(stretchY);

					const float moveY = moveSpeed * MarkerJoint_RUL9.y - (7.367 * moveSpeed);
					occlusionMoveY->setValue(moveY);
			}
		}

		// iterate over all bones in the mesh
		float tx, ty, tz, rx, ry, rz;
		for (int i = 0; i < m_boneNames.size(); ++i) {
			tx = ty = tz = rx = ry = rz = 0.0;
			const QString &boneName = m_boneNames[i];
			Parameter *parameter = m_boneGroup->getParameter(boneName);
			if (!parameter) return;
			const QVariantList &valueList = parameter->getValue(true).toList();
			if (valueList.size() == 6) {
				rx += valueList[0].toFloat();
				ry += valueList[1].toFloat();
				rz += valueList[2].toFloat();
				tx += valueList[3].toFloat();
				ty += valueList[4].toFloat();
				tz += valueList[5].toFloat();

				//std::cout <<"bekommt"<< rx << ry << rz << tx << ty << tz << std::endl;
				//std::cout<< &boneName << std::endl;
				transformBone(boneName, tx, ty, tz, rx, ry, rz);



			} //neu eingefuegt kinect
			else if (valueList.size() == 7) 
			{
				// Interpret valueList as: angle, axis, position
				Ogre::Radian angle( valueList[0].toFloat());
				Ogre::Vector3 axis( valueList[1].toFloat(), valueList[2].toFloat(), valueList[3].toFloat());

				//----		std::cout <<"value0 "<< angle << "value1 "<< axis.x << "value2 "<< axis.y << "value3 "<< axis.z << std::endl;


				Ogre::Quaternion orientation( angle, axis ); //hier werden die daten falsch umgerechnet, die quaternion muss genau das sein was auch �bergeben wird!!
				//-----	std::cout<< "orientation quat bekommt w " << orientation.w <<" x "<< orientation.x <<" y "<< orientation.y <<" z "<<orientation.z << std::endl;

				Ogre::Quaternion orientation2(valueList[0].toFloat(),valueList[1].toFloat(),valueList[2].toFloat(),valueList[3].toFloat());
				//----		std::cout<< "orientation2 quat bekommt w " << orientation2.w <<" x "<< orientation2.x <<" y "<< orientation2.y <<" z "<<orientation2.z << std::endl; //der richtige wert

				Ogre::Quaternion orient = orientation;
				Ogre::Vector3 position (valueList[4].toFloat(), valueList[5].toFloat(), valueList[6].toFloat());

				transformBone( boneName, orientation2, position );


			} //end kinectpart

			// if more than one node is connected
			const QVariantList &parameterValueList = parameter->getValueList();
			for (int i = 1; i < parameterValueList.size(); ++i) {
				const QVariant &value = parameterValueList[i]; 
				QVariantList valueList = value.toList();
				if (valueList.size() == 6) {
					rx += valueList[0].toFloat();
					ry += valueList[1].toFloat();
					rz += valueList[2].toFloat();
					tx += valueList[3].toFloat();
					ty += valueList[4].toFloat();
					tz += valueList[5].toFloat();
				}
			}
			//transformBone(boneName, tx, ty, tz, rx, ry, rz);
		} 


	}

	// other animatons
	for (int i = 0; i < m_animationNames.size(); ++i) {
		const QString &animationName = m_animationNames[i];
		Parameter *parameter = m_animationGroup->getParameter(animationName);

		// If AUs are not manually controlled nothing to do here
		float progress = parameter->getValue(true).toFloat();
		const QVariantList &parameterValueList = parameter->getValueList();
		for (int i = 1; i < parameterValueList.size(); ++i) {
			const QVariant &value = parameterValueList[i]; 
			progress += value.toFloat();
		}


		if (progress > 100.0f)
			progress = 100.0f;

		float weight = 1.0;
#ifdef USE_ANIMATION_WEIGHTS
		QString weightParamName = animationName + "_weight";
		if (m_weightNames.contains(weightParamName)) {
			Parameter *weightParameter = m_animationWeightGroup->getParameter(weightParamName);
			weight = weightParameter->getValue(true).toFloat();
		}
#endif

		progressAnimation(animationName, progress / 100.0, weight);

		if (m_autoPose) {
			progressAnimation("pose_" + animationName, progress / 100.0, weight);
			if (m_poseNames.contains("pose_" + animationName))
				setValue("pose_" + animationName, progress, weight);
		}
	}

	// other poses
	if (m_manual /*&& !m_autoPose*/) {
		for (int i = 0; i < m_poseNames.size(); ++i) {
			const QString &poseName = m_poseNames[i];
			float weight = 1.0;
#ifdef USE_ANIMATION_WEIGHTS
			QString weightParamName = poseName;
			weightParamName.remove("pose_");
			weightParamName = weightParamName + "_weight";
			if (m_weightNames.contains(weightParamName))
				weight = getDoubleValue(weightParamName, true);
#endif
			Parameter *parameter = m_poseGroup->getParameter(poseName);
			double progress = parameter->getValue().toDouble();
			//if (progress == 0.0)
			//    continue;
			if (progress > 100.0f)
				progress = 100.0f;
			progressAnimation(poseName, progress/100.0, weight);
		}
		triggerRedraw();
	}
	//if (isAnimated()) 
	//	triggerRedraw();
}

//!
//! Update object id.
//!
void MocapMeshNode::updateObjectId ()
{
    Parameter* parameter = dynamic_cast<Parameter*>(sender());
    if (parameter) {
        const QString &name = parameter->getName();
        int value = parameter->getValue().toInt();
        Ogre::SubEntity * subEntity = m_entity->getSubEntity(name.toStdString());
        subEntity->setCustomParameter(0, Ogre::Vector4((float) value, 0.0, 0.0, 0.0));
    }
}

///
/// Private Functions
///

//!
//! Initialize all animation states of this object (OGRE-specific).
//!
//! \return True if the animation states where successfully initialized, otherwise False.
//!
bool MocapMeshNode::initAnimationStates ()
{  
    QMap<QString, Ogre::AnimationState*> animStates;
    QStringList animNames = getAnimationNames();
    for(int i = 0; i < animNames.size(); ++i) {
        QString animName = animNames[i];
        animStates[animName] = m_entity->getAnimationState(animName.toStdString());
        //NILZ: TODO: better enable animation states somewhere else
        animStates[animName]->setLoop(false);
        //animStates[animName]->setEnabled(true); 
    }

    setInterpolationMode();
    m_animStates = animStates;
    return true;
}

//!
//! Loads animation mesh.
//!
//! \return True, if successful loading of ogre mesh
//!            False, otherwise.
//!
bool MocapMeshNode::loadMesh ()
{
    QString filename = getStringValue("Geometry File");
    if (filename == "") {
        Log::debug(QString("Geometry file has not been set yet. (\"%1\")").arg(m_name), "MocapMeshNode::loadMesh");
        return false;
    }

    // obtain the OGRE scene manager
    Ogre::SceneManager *sceneManager = OgreManager::getSceneManager();
    if (!sceneManager) {
        Log::error("Could not obtain OGRE scene manager.", "MocapMeshNode::loadMesh");
        return false;
    }

    // destroy an existing OGRE entity for the mesh
    destroyEntity();

    // create new scene node
    m_sceneNode = OgreManager::createSceneNode(m_name);
    if (!m_sceneNode) {
        Log::error(QString("Scene node for node \"%1\" could not be created.").arg(m_name), "MocapMeshNode::loadMesh");
        return false;
    }
    setValue(m_outputGeometryName, m_sceneNode, true);

    // check if the file exists
    if (!QFile::exists(filename)) {
        Log::error(QString("Mesh file \"%1\" not found.").arg(filename), "MocapMeshNode::loadMesh");
        return false;
    }

    // split the absolute filename to path and base filename
    int lastSlashIndex = filename.lastIndexOf('/');
    QString path = "";
    if (lastSlashIndex > -1) {
        path = filename.mid(0, lastSlashIndex);
        filename = filename.mid(lastSlashIndex + 1);
    }
    if (!filename.endsWith(".mesh")) {
        Log::error("The geometry file has to be an OGRE mesh file.", "MocapMeshNode::loadMesh");
        return false;
    }

    //// destroy old resource group and generate new one
    //QString resourceGroupName = filename;
    //resourceGroupName.replace(".mesh", "");
    //destroyResourceGroup();
    //m_oldResourceGroupName = resourceGroupName;
    //createResourceGroup(resourceGroupName, path);

    // destroy old resource group and generate new one
    QString resourceGroupName = QString::fromStdString(createUniqueName("ResourceGroup_" + filename + "_MocapMeshNode"));
    OgreTools::destroyResourceGroup(m_oldResourceGroupName);
    m_oldResourceGroupName = resourceGroupName;
    OgreTools::createResourceGroup(resourceGroupName, path);

    // create a new OGRE entity for the mesh file
    m_entity = sceneManager->createEntity(m_name.toStdString(), filename.toStdString());
    if (m_entity) {
        int numOfSubentities = m_entity->getNumSubEntities();
		bool SaC = getBoolValue("useSaC");
        for (int i = 0; i < numOfSubentities; ++i) {
            // create a new number parameter for the bone
            Ogre::SubEntity *subEntity = m_entity->getSubEntity(i);
            subEntity->setCustomParameter(0, Ogre::Vector4(0.0, 0.0, 0.0, 0.0));

			Ogre::String subMeshMaterialName = subEntity->getMaterialName();
			std::string::size_type loc = subMeshMaterialName.find("Face");
			if( loc != std::string::npos && SaC ) {
				HashMap<Ogre::String, ushort> subMeshNameMap = m_entity->getMesh()->getSubMeshNameMap();
				writeDataToVertices(m_entity->getSubEntity(i)->getSubMesh());
			}
        }

        // set cumulative blend mode instead of Ogre::ANIMBLEND_AVERAGE which is default
        if (m_entity->hasSkeleton()) {
            Ogre::Skeleton *skeleton = m_entity->getSkeleton();
            skeleton->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);
            //skeleton->setBlendMode(Ogre::ANIMBLEND_AVERAGE);
        }
        m_sceneNode->attachObject(m_entity);
    }

    // create a container for the entity
    m_entityContainer = new OgreContainer(m_entity);
    m_entity->setUserAny(Ogre::Any(m_entityContainer));

    updateStatistics();
    Log::info(QString("Mesh file \"%1\" loaded.").arg(filename), "MocapMeshNode::loadMesh");
    return true;
}


//!
//! Retrieves the numbers of vertices and triangles from the mesh and stores
//! them in parameters of this node.
//!
void MocapMeshNode::updateStatistics ()
{
    unsigned int numVertices = 0;
    unsigned int numTriangles = 0;

    if (m_entity) {
        const Ogre::MeshPtr &mesh = m_entity->getMesh();
        if (!mesh.isNull()) {
            bool sharedAdded = false;
            unsigned int indexCount = 0;

            for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i) {
                Ogre::SubMesh* subMesh = mesh->getSubMesh(i);
                if (subMesh->useSharedVertices) {
                    if (!sharedAdded) {
                        numVertices += (unsigned int) mesh->sharedVertexData->vertexCount;
                        sharedAdded = true;
                    }
                } else {
                    numVertices += (unsigned int) subMesh->vertexData->vertexCount;
                }

                indexCount += (unsigned int) subMesh->indexData->indexCount;
            }

            numTriangles = indexCount / 3;
        }
    }

    blockSignals(true);
    setValue("Number of Vertices", QString("%L1").arg(numVertices), true);
    setValue("Number of Triangles", QString("%L1").arg(numTriangles), true);
    blockSignals(false);
}


//!
//! Removes the OGRE entity containing the mesh geometry from the scene and
//! destroys it along with the OGRE scene node.
//!
void MocapMeshNode::destroyEntity ()
{
    if (m_entity) {
        // delete entity container
        if (m_entityContainer) {
            delete m_entityContainer;
            m_entityContainer = 0;
        }

        // remove the entity from the scene node it is attached to
        Ogre::SceneNode *parentSceneNode = m_entity->getParentSceneNode();
        if (parentSceneNode)
            parentSceneNode->detachObject(m_entity);

        // destroy the entity through its scene manager
        Ogre::SceneManager *sceneManager = OgreManager::getSceneManager();
        if (sceneManager) {
            sceneManager->destroyEntity(m_entity);
            m_entity = 0;
        }
    }

    if (m_sceneNode) {
        // destroy the scene node through its scene manager
        Ogre::SceneManager *sceneManager = m_sceneNode->getCreator();
        if (sceneManager) {
            sceneManager->destroySceneNode(m_sceneNode);
            m_sceneNode = 0;
            setValue(m_outputGeometryName, m_sceneNode, true);
        }
    }
}


//!
//! Adds the given time value to the animation state with the given name in
//! order to progess the animation (OGRE-specific).
//!
//! \param aUnitName The name of the animation state to progress.
//! \param timeToAdd The time to add to the animation state with the given name.
//!
void MocapMeshNode::progressAnimation ( const QString &animationName, float timeToAdd, float weight /* = 1.0 */ )
{
    Ogre::SkeletonInstance *skeletonInstance = m_entity->getSkeleton();
    Ogre::AnimationState *animState = m_animStates[animationName];
    const float mult = getDoubleValue("mult");

    if (animState) {
        float animLength = animState->getLength();
        Ogre::Real pos = mult * timeToAdd * animLength;

        //// manually controlled bones might exist - reset them
        //if (skeletonInstance->hasAnimation(aUnitName.toStdString())) {
        //    Ogre::Animation *animation = skeletonInstance->getAnimation(aUnitName.toStdString());
        //    Ogre::Animation::NodeTrackIterator trackIter = animation->getNodeTrackIterator();
        //    while (trackIter.hasMoreElements()) {
        //        Ogre::NodeAnimationTrack* track = trackIter.getNext();
        //        Ogre::Bone *bone = skeletonInstance->getBone(track->getHandle());
        //        if (bone->isManuallyControlled())
        //            bone->reset();
        //    }
        //}

        // update all copies created from the entity through the entity container
        if (m_entityContainer) {
            m_entityContainer->updateAnimationState(animationName, pos, weight);
        }
    }
}

//!
//! Transforms a given bone.
//! 
//! \param name The name of the bone.
//! \param tx Translation x axis.
//! \param ty Translation y axis.
//! \param tz Translation z axis.
//! \param rx Rotation x axis.
//! \param ry Rotation y axis.
//! \param rz Rotation z axis.
//!
void MocapMeshNode::transformBone ( const QString &name, float &tx, float &ty, float &tz, float &rx, float &ry, float &rz )
{
    if (m_entity->hasSkeleton()) {
		const std::string &stdName = name.toStdString();
        Ogre::SkeletonInstance *skeletonInstance = m_entity->getSkeleton();
        if (skeletonInstance->hasBone(stdName)) {
            Ogre::Bone *bone = skeletonInstance->getBone(stdName);
            bone->setManuallyControlled(true);
            bone->reset();
            bone->translate(tx, ty, tz);
			//bone->setPosition(tx,ty,tz);
			//std::cout <<"transformbone bekommt"<< tx <<ty<<tz<<std::endl;
            bone->rotate(Ogre::Vector3::UNIT_X, Ogre::Radian(rx));
            bone->rotate(Ogre::Vector3::UNIT_Y, Ogre::Radian(ry));
            bone->rotate(Ogre::Vector3::UNIT_Z, Ogre::Radian(rz));
            m_entityContainer->updateCopies(name, tx, ty, tz, rx, ry, rz);
        }
    }
}

//Rest InitPos for KinectTracking
void MocapMeshNode::resetInitPos()
{
	Parameter *parameter = dynamic_cast<Parameter *>(sender());
   
	if (!parameter)
        return;

	const QVariantList &valueList = parameter->getValue(true).toList();
	printf("true ");
  if (m_entity->hasSkeleton()) {

	  Ogre::SkeletonInstance *skeletonInstance = m_entity->getSkeleton();
	  skeletonInstance->reset();
  }

}

// new transformbonemethode for kinect (map the data from kinect (real space) to ogre scenespace
void MocapMeshNode::transformBone( const QString &name, const Ogre::Quaternion &rotation, const Ogre::Vector3 &position )
{
    if (m_entity->hasSkeleton()) {
		const std::string &stdName = name.toStdString();
		//std::cout << "string mesh " << &stdName << std::endl;

		
        Ogre::SkeletonInstance *skeletonInstance = m_entity->getSkeleton();
        if (skeletonInstance->hasBone(stdName)) {
            Ogre::Bone *bone = skeletonInstance->getBone(stdName);
			bone->setManuallyControlled(true);  //sehr sehr wichtig sonst bewegt sich nix .. zeigtz nix an !!
			//bone->setInheritOrientation(false);
		
			//std::cout << &stdName <<std::endl;
			Ogre::Quaternion qI = bone->getInitialOrientation();
			//std::cout <<"initialOrientation  "<< &name <<" w "<< qI.w <<"x  "<< qI.x <<"y  "<< qI.y <<"z  "<< qI.z << std::endl;

			Ogre::Quaternion newQ; // = Ogre::Quaternion::IDENTITY;
			newQ = rotation;
		//-----	std::cout << "transformBonerotation" << &stdName <<"w "<< rotation.w <<"x  "<< rotation.x <<"y  "<< rotation.y <<"z  "<< rotation.z << std::endl;
			Ogre::Matrix3 rotM;
			//rotation.ToRotationMatrix(rotM);
			newQ.ToRotationMatrix(rotM);
			Ogre::Radian a,b,c;
			rotM.ToEulerAnglesXYZ(a,b,c);
		//------	std::cout << " winkel vor konvertierung" << a << " b  " <<  b <<" c  "<< c <<std::endl;

			bone->reset();
			/*if(position!= Ogre::Vector3(0.0,0.0,0.0)){
			bone->setPosition(position);
			}*/
			 bone->translate(position);
			//bone->setInitialState();

			newQ = bone->convertWorldToLocalOrientation(newQ);
			//bone->setOrientation(newQ*qI);

			Ogre::Quaternion rotQuat (newQ*qI);

		//-----	std::cout << "rotQuat nach konvertierung" << &stdName <<"w "<< newQ.w <<"x  "<< newQ.x <<"y  "<< newQ.y <<"z  "<< newQ.z << std::endl;
			//bone->setOrientation(test);

			//Ogre::Quaternion resQ = bone->getOrientation();

		
			bone->rotate(rotQuat, Ogre::Node::TS_LOCAL);


			//bring back to float angle and position (rx, ry, rz, tx, ty, tz) for updatecopies call
			//Ogre::Matrix3 rotM;
			//rotation.ToRotationMatrix(rotM);
			rotQuat.ToRotationMatrix(rotM);
			Ogre::Radian x,y,z;
			rotM.ToEulerAnglesXYZ(x,y,z);
			//rotM.ToEulerAnglesYZX(y,z,x);
			
			Ogre::Radian w = x* Ogre::Radian(Ogre::Math::PI); 
			Ogre::Radian q = y*Ogre::Radian(Ogre::Math::PI);
			Ogre::Radian v = z*Ogre::Radian(Ogre::Math::PI);


			//----std::cout << " winkel nach konvertierung " << x <<  y << z <<std::endl;

			float angleX = x.valueRadians();
			float angleY = y.valueRadians(); //*360  2pi
			float angleZ = z.valueRadians();
			float tx =position.x;
			float ty =position.y;
			float tz =position.z;
			//printf("rx %f ry %f rz %f \n", angleX,angleY,angleZ);
			//printf("x %f y %f z %f \n", x,y,z);
			//std::cout << x <<  y << z <<std::endl;
	
			//
			//bone->rotate( Ogre::Vector3::UNIT_X, x, Ogre::Node::TS_LOCAL); //(Ogre::Quaternion (Ogre::Matrix3 rot);	
			//bone->rotate( Ogre::Vector3::UNIT_Y, y, Ogre::Node::TS_LOCAL);
			//bone->rotate( Ogre::Vector3::UNIT_Z, z, Ogre::Node::TS_LOCAL);


		
			// @TODO: update copies auch mit Quaternionen!!!
			m_entityContainer->updateCopies(name, tx, ty, tz, angleX, angleY, angleZ);
			
		}
    }
}




//!
//! Retrieve the names of all animations of skeleton.
//!
//! \return List with names of animations.
//!
QStringList MocapMeshNode::getAnimationNames ()
{
    QStringList animNames;

    // obtain the OGRE scene manager
    Ogre::SceneManager *sceneManager = OgreManager::getSceneManager();
    if (!sceneManager) {
        Log::error("Could not obtain OGRE scene manager.", "MocapMeshNode::getAnimationNames");
        return animNames;
    }

    if (!sceneManager->hasEntity(m_name.toStdString()))
        return QStringList();

    Ogre::Entity *entity = sceneManager->getEntity(m_name.toStdString());

    // proceed only if entity available
    if (!entity)
        return animNames;

    Ogre::AnimationStateSet *animStateSet = m_entity->getAllAnimationStates();
    if (animStateSet) {
        Ogre::AnimationStateIterator animStatesIterator = animStateSet->getAnimationStateIterator();

        while (animStatesIterator.hasMoreElements()) {
            Ogre::AnimationState* animState = animStatesIterator.getNext();
            Ogre::String ogreAnimName = animState->getAnimationName();
            QString animName(ogreAnimName.c_str());
            animNames.append(animName);
        }
    }

    return animNames;
}


//!
//! Returns the names of all bones in the skeleton for the mesh.
//!
//! \return The names of all bones in the skeleton for the mesh.
//!
QStringList MocapMeshNode::getBoneNames ()
{
    QStringList result;

    // obtain the OGRE scene manager
    Ogre::SceneManager *sceneManager = OgreManager::getSceneManager();
    if (!sceneManager) {
        Log::error("Could not obtain OGRE scene manager.", "MocapMeshNode::getBoneNames");
        return result;
    }

    // proceed only if entity available
    if (!sceneManager->hasEntity(m_name.toStdString()))
        return result;
    Ogre::Entity *entity = sceneManager->getEntity(m_name.toStdString());
    if (!entity || !entity->hasSkeleton())
        return result;

    Ogre::Skeleton *skeleton = entity->getSkeleton();
    unsigned short numberOfBones = skeleton->getNumBones();
    for (unsigned short i = 0; i < numberOfBones; ++i) {
        Ogre::Bone *bone = skeleton->getBone(i);
        result.append(QString(bone->getName().c_str()));
    }
    return result;
}


//!
//! Initialize a bunch of bones.
//!
//! \param boneNames The list of names of bones to initialize.
//!
void MocapMeshNode::initializeBones ( QStringList boneNames )
{
    // obtain the OGRE scene manager
    Ogre::SceneManager *sceneManager = OgreManager::getSceneManager();
    if (!sceneManager) {
        Log::error("Could not obtain OGRE scene manager.", "MocapMeshNode::initializeBones");
        return;
    }

    // proceed only if entity and entity skeleton available
    Ogre::Entity *entity = sceneManager->getEntity(m_name.toStdString());
    if (!entity || !entity->hasSkeleton())
        return;

    Ogre::Skeleton *skeleton = entity->getSkeleton();
    for (int i = 0; i < boneNames.size(); ++i) {
        if (skeleton->hasBone(boneNames[i].toStdString())) {
            Ogre::Bone *bone = skeleton->getBone(boneNames[i].toStdString());
            if (bone) {
                bone->setManuallyControlled(true);
				
                //bone->setBindingPose();
            }
        }
    }
}

//!
//! Parse the Material Parameters and add to Material Parameter Group.
//!
void MocapMeshNode::parseMaterialParameters ()
{
    // remove old material parameters
    m_materialGroup->destroyAllParameters();

    // initialize the material number parameters
    const Ogre::MeshPtr meshPtr = m_entity->getMesh();
	if (meshPtr.isNull())
		return;

    Ogre::Mesh::SubMeshIterator subMeshIter = meshPtr->getSubMeshIterator();
    while (subMeshIter.hasMoreElements()) {
        Ogre::SubMesh* subMesh = subMeshIter.getNext();
        Ogre::String subMeshMaterialName = subMesh->getMaterialName();
        ParameterGroup *subMeshGroup = new ParameterGroup(subMeshMaterialName.c_str());
        m_materialGroup->addParameter(subMeshGroup);
        Ogre::MaterialPtr subMeshMaterial = (Ogre::MaterialPtr)Ogre::MaterialManager::getSingleton().getByName(subMeshMaterialName);
        if (!subMeshMaterial.isNull()) {
            Ogre::Technique* tech = subMeshMaterial->getTechnique(0);
            if (!tech)
                continue;
            Ogre::Technique::PassIterator passIter = tech->getPassIterator();
            while (passIter.hasMoreElements()) {
                Ogre::Pass* pass = passIter.getNext();
                ParameterGroup *passGroup = new ParameterGroup("Pass: " + QString(pass->getName().c_str()));
                subMeshGroup->addParameter(passGroup);

				// reload textures
                Ogre::Pass::TextureUnitStateIterator textureUnitStateIterator = pass->getTextureUnitStateIterator();
				if (textureUnitStateIterator.hasMoreElements()) { 
					ParameterGroup *texGroup = new ParameterGroup("Reload Textures:");
					passGroup->addParameter(texGroup);				
					NumberParameter *numberPara = new NumberParameter(QString("Reload All Textures"), Parameter::T_Command, "__rla__"+QString(pass->getName().c_str()) );					 					
					texGroup->addParameter(numberPara);
					numberPara->setCommandFunction(SLOT(reloadTexture()));					
                while (textureUnitStateIterator.hasMoreElements())
                    {
                     Ogre::TextureUnitState* textureUnitState = textureUnitStateIterator.getNext();
					 NumberParameter *numberPara = new NumberParameter(QString(textureUnitState->getTextureName().c_str()), Parameter::T_Command, "reload");					 
					 texGroup->addParameter(numberPara);
					 numberPara->setCommandFunction(SLOT(reloadTexture()));						 
					}
				}				

				// fragment shader
                if (pass->hasFragmentProgram()) {
                    ParameterGroup *progGroup = new ParameterGroup("FP: " + QString(pass->getFragmentProgram()->getName().c_str()));
                    passGroup->addParameter(progGroup);
                    Ogre::GpuProgramParametersSharedPtr fpParams = pass->getFragmentProgramParameters();                   
                    const Ogre::GpuNamedConstants &namedConstants = fpParams->getConstantDefinitions();
                    const Ogre::GpuConstantDefinitionMap &fpParamMap = namedConstants.map;
                    Ogre::GpuConstantDefinitionMap::const_iterator fpParamIter;
					for (fpParamIter = fpParamMap.begin(); fpParamIter != fpParamMap.end(); ++fpParamIter) {
                        // float
						if	(fpParamIter->second.isFloat() && 
                            (fpParamIter->second.constType == Ogre::GCT_FLOAT1) &&
							(fpParamIter->first.find("[0]") == Ogre::String::npos)) {
                                float value = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex);
								NumberParameter *numberPara = new NumberParameter(QString(fpParamIter->first.c_str())+"@"+QString(subMeshMaterialName.c_str()), Parameter::T_Float, value);
                                numberPara->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
                                value = pow(10.0f, (float) ceil(log10(abs(value+0.001))));
								if (value < 0.01)
									value = 1.0;
                                numberPara->setMinValue(-value);
                                numberPara->setMaxValue( value);
                                numberPara->setStepSize(value/100.0);
                                progGroup->addParameter(numberPara);								
                                numberPara->setChangeFunction(SLOT(setMaterialParameter()));								
								numberPara->setDescription(subMeshMaterialName.c_str());
                        }
						// float3						
						if	(fpParamIter->second.isFloat() && 								
							(fpParamIter->second.constType == Ogre::GCT_FLOAT3) &&
							(fpParamIter->first.find("[0]") == Ogre::String::npos)) {	
								float value[3];
								QColor color;
								color.setRedF(*fpParams->getFloatPointer(fpParamIter->second.physicalIndex));		
								color.setGreenF(*fpParams->getFloatPointer(fpParamIter->second.physicalIndex+1));		
								color.setBlueF(*fpParams->getFloatPointer(fpParamIter->second.physicalIndex+2));										
								value[0] = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex);		
								value[1] = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex+1);	
								value[2] = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex+2);	

								NumberParameter *numberPara = new NumberParameter(QString(fpParamIter->first.c_str())+"@"+QString(subMeshMaterialName.c_str()), Parameter::T_Color, color);				
								numberPara->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
                                progGroup->addParameter(numberPara);
                                numberPara->setChangeFunction(SLOT(setMaterialParameter()));
						}
						// float4
						if	(fpParamIter->second.isFloat() && 								
							(fpParamIter->second.constType == Ogre::GCT_FLOAT4) &&
							(fpParamIter->first.find("[0]") == Ogre::String::npos)) {	
								float value[4];
								QColor color;
								color.setRedF(*fpParams->getFloatPointer(fpParamIter->second.physicalIndex));		
								color.setGreenF(*fpParams->getFloatPointer(fpParamIter->second.physicalIndex+1));		
								color.setBlueF(*fpParams->getFloatPointer(fpParamIter->second.physicalIndex+2));										
								color.setAlphaF(*fpParams->getFloatPointer(fpParamIter->second.physicalIndex+3));										
								value[0] = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex);		
								value[1] = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex+1);	
								value[2] = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex+2);	
								value[3] = *fpParams->getFloatPointer(fpParamIter->second.physicalIndex+3);	

								NumberParameter *numberPara = new NumberParameter(QString(fpParamIter->first.c_str())+"@"+QString(subMeshMaterialName.c_str()), Parameter::T_Color, color);				
								numberPara->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
                                progGroup->addParameter(numberPara);
                                numberPara->setChangeFunction(SLOT(setMaterialParameter()));
						}
                    }
                }
				// vertex shader
                if (pass->hasVertexProgram()) {
                    ParameterGroup *progGroup = new ParameterGroup("VP: " + QString(pass->getVertexProgram()->getName().c_str()));
                    passGroup->addParameter(progGroup);
                    Ogre::GpuProgramParametersSharedPtr vpParams = pass->getVertexProgramParameters();
					const Ogre::GpuNamedConstants &namedConstants = vpParams->getConstantDefinitions();
					const Ogre::GpuConstantDefinitionMap &vpParamMap = namedConstants.map;
					Ogre::GpuConstantDefinitionMap::const_iterator vpParamIter;					
					for (vpParamIter = vpParamMap.begin(); vpParamIter != vpParamMap.end(); ++vpParamIter) {
                        if	(vpParamIter->second.isFloat() && 
                            (vpParamIter->second.constType == Ogre::GCT_FLOAT1) &&
							(vpParamIter->first.find("[0]") == Ogre::String::npos)) {
						    float value = *vpParams->getFloatPointer(vpParamIter->second.physicalIndex);
                            NumberParameter *numberPara = new NumberParameter(QString(vpParamIter->first.c_str())+"@"+QString(subMeshMaterialName.c_str()), Parameter::T_Float, value);
                            numberPara->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
                            value = pow(10.0f, (float) ceil(log10(abs(value+0.001))));
                            numberPara->setMinValue(-value);
                            numberPara->setMaxValue( value);
                            numberPara->setStepSize(value/100.0);
                            progGroup->addParameter(numberPara);
                            numberPara->setChangeFunction(SLOT(setMaterialParameter()));
							numberPara->setDescription(subMeshMaterialName.c_str());
						}
                    }
				}
				// geometry shader
				if (pass->hasGeometryProgram()) {
					ParameterGroup *progGroup = new ParameterGroup("GP: " + QString(pass->getGeometryProgram()->getName().c_str()));
                    passGroup->addParameter(progGroup);
					Ogre::GpuProgramParametersSharedPtr gpParams = pass->getGeometryProgramParameters();
					const Ogre::GpuNamedConstants &namedConstants = gpParams->getConstantDefinitions();
					const Ogre::GpuConstantDefinitionMap &gpParamMap = namedConstants.map;
					Ogre::GpuConstantDefinitionMap::const_iterator gpParamIter;							
					for (gpParamIter = gpParamMap.begin(); gpParamIter != gpParamMap.end(); ++gpParamIter) {
                        if	(gpParamIter->second.isFloat() && 
                            (gpParamIter->second.constType == Ogre::GCT_FLOAT1) &&
							(gpParamIter->first.find("[0]") == Ogre::String::npos)) {
						    float value = *gpParams->getFloatPointer(gpParamIter->second.physicalIndex);
                            NumberParameter *numberPara = new NumberParameter(QString(gpParamIter->first.c_str())+"@"+QString(subMeshMaterialName.c_str()), Parameter::T_Float, value);
                            numberPara->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
                            value = pow(10.0f, (float) ceil(log10(abs(value+0.001))));
                            numberPara->setMinValue(-value);
                            numberPara->setMaxValue( value);
                            numberPara->setStepSize(value/100.0);
                            progGroup->addParameter(numberPara);
                            numberPara->setChangeFunction(SLOT(setMaterialParameter()));
							numberPara->setDescription(subMeshMaterialName.c_str());
						}
                    }
				}
            }
        }
    }

    //initializeBones(m_boneNames);
    // Redraw node - new parameters
    //notifyChange();
}


///
/// Private Slots
///

//!
//! Change function for the Geometry File parameter.
//!
void MocapMeshNode::geometryFileChanged ()
{
    // remove old skeletal animation parameters
    const QList<Parameter *> &animationParameterList = m_animationGroup->filterParameters("", true, true);
    for (int i = 0; i < animationParameterList.size(); ++i)
        removeParameter(animationParameterList[i]);

    // remove old pose parameters
    const QList<Parameter *> &poseParameterList = m_poseGroup->filterParameters("", true, true);
    for (int i = 0; i < poseParameterList.size(); ++i)
        removeParameter(poseParameterList[i]);

    // remove old bone parameters
    const QList<Parameter *> &boneParameterList = m_boneGroup->filterParameters("", true, true);
    for (int i = 0; i < boneParameterList.size(); ++i)
        removeParameter(boneParameterList[i]);

    // remove old object id parameters
    const QList<Parameter *> &objectIdParameterList = m_objectIdGroup->filterParameters("", true, true);
    for (int i = 0; i < objectIdParameterList.size(); ++i)
        removeParameter(objectIdParameterList[i]);

    // clear lists
    m_animationNames.clear();
    m_poseNames.clear();
    m_boneNames.clear();

    // load new mesh and skeleton
    loadMesh();

	if (!m_entity)
		return;

    // initialize the OGRE animation states
    initAnimationStates();

    // iterate over the list of animations
    const QStringList &animationNames = getAnimationNames();
    for (int i = 0; i < animationNames.size(); ++i) {
        // create a new number parameter for the AU
        NumberParameter *animationParameter = new NumberParameter(animationNames[i], Parameter::T_Float, 0.0);
        animationParameter->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
        animationParameter->setMinValue(0.0);
        animationParameter->setMaxValue(100.0);
        animationParameter->setStepSize(1.0);
        animationParameter->setPinType(Parameter::PT_Input);
        animationParameter->setMultiplicity(Parameter::M_OneOrMore);

#ifdef USE_ANIMATION_WEIGHTS
        NumberParameter *weightParameter = new NumberParameter(animationNames[i] + "_weight", Parameter::T_Float, 1.0);
        weightParameter->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
        weightParameter->setMinValue(0.0);
        weightParameter->setMaxValue(1.0);
        weightParameter->setStepSize(0.01);
        weightParameter->setPinType(Parameter::PT_Input);
        weightParameter->setMultiplicity(Parameter::M_OneOrMore);
#endif

        // check if the parameter is a pose parameter
        if (animationNames[i].left(5) == "pose_") {
            m_poseGroup->addParameter(animationParameter);
            animationParameter->setChangeFunction(SLOT(updatePoses()));
            m_poseNames.append(animationNames[i]);
            m_poseWeightGroup->addParameter(weightParameter);
            weightParameter->setChangeFunction(SLOT(updateAnimation()));
            m_weightNames.append(animationNames[i] + "_weight");
        } else {
            m_animationGroup->addParameter(animationParameter);
#ifdef USE_ANIMATION_WEIGHTS
            m_animationWeightGroup->addParameter(weightParameter);
            weightParameter->setChangeFunction(SLOT(updateAnimation()));
            m_weightNames.append(animationNames[i] + "_weight");
#endif
            animationParameter->setChangeFunction(SLOT(updateAnimation()));
            m_animationNames.append(animationNames[i]);
        }
    }

    // initialize the list of values for the bone number parameters
    QVariantList values  = QVariantList() << 0.0 << 0.0 << 0.0 << 0.0 << 0.0 << 0.0;

    // iterate over the list of bones
    const QStringList &boneNames = getBoneNames();
    for (int i = 0; i < boneNames.size(); ++i) {
        if (!boneNames[i].contains("MarkerJoint_") ) {
            // create a new number parameter for the bone
            NumberParameter *boneParameter = new NumberParameter(boneNames[i], Parameter::T_Float, 0.0);
            boneParameter->setInputMethod(NumberParameter::IM_SliderPlusSpinBox);
            boneParameter->setMinValue(0.0);
            boneParameter->setMaxValue(100.0);
            boneParameter->setStepSize(0.01);
            boneParameter->setPinType(Parameter::PT_Input);
            boneParameter->setSize(6);
            //boneParameter->setSelfEvaluating(true);
            boneParameter->setMultiplicity(Parameter::M_OneOrMore);
            //boneParameter->setMultiplicity(values.size());
            boneParameter->setValue(QVariant(values));
            m_boneGroup->addParameter(boneParameter);
            boneParameter->setChangeFunction(SLOT(updateBones()));
            m_boneNames.append(boneNames[i]);
        }
    }

    // iterate over the sub entities
    int numOfSubentities = m_entity->getNumSubEntities();
    Ogre::Mesh::SubMeshNameMap nameMap = m_entity->getMesh()->getSubMeshNameMap();
    Ogre::Mesh::SubMeshNameMap::iterator iter;
    
    for (iter = nameMap.begin(); iter != nameMap.end(); iter++) {
    //for (int i = 0; i < numOfSubentities; ++i) {
        // create a new number parameter for the bone
        //NumberParameter *objectIdParameter = new NumberParameter(QString::number(i), Parameter::T_Int, 0);
        NumberParameter *objectIdParameter = new NumberParameter(QString::fromStdString(iter->first), Parameter::T_Int, 0);
        objectIdParameter->setInputMethod(NumberParameter::IM_SpinBox);
        objectIdParameter->setMinValue(0);
        objectIdParameter->setMaxValue(100);
        objectIdParameter->setStepSize(1);
        objectIdParameter->setValue(QVariant((int) iter->second));
        Ogre::SubEntity *subEntity = m_entity->getSubEntity(iter->first);
        subEntity->setCustomParameter(0, Ogre::Vector4((float) (iter->second + 1), 0.0, 0.0, 0.0));
        m_objectIdGroup->addParameter(objectIdParameter);
        objectIdParameter->setChangeFunction(SLOT(updateObjectId()));
        Parameter *affectedParameter = getParameter(m_outputGeometryName);
        objectIdParameter->addAffectedParameter(affectedParameter);
    }

    // initialize the material number parameters
    parseMaterialParameters();
}


//!
//! Set interpolation mode.
//!
void MocapMeshNode::setInterpolationMode ()
{
    Ogre::Animation::InterpolationMode interpolationMode = Ogre::Animation::IM_LINEAR;
    bool useSplineInterpolation = getBoolValue("Use Spline Interpolation");
    if (useSplineInterpolation)
        interpolationMode = Ogre::Animation::IM_SPLINE;
    QStringList animNames = getAnimationNames();
    Ogre::SkeletonInstance *skeletonInstance = m_entity->getSkeleton();
    if (!skeletonInstance)
        return;
    for(int i = 0; i < animNames.size(); ++i) {
        QString animName = animNames[i];
        if (skeletonInstance->hasAnimation(animName.toStdString())) {
            Ogre::Animation *animation = skeletonInstance->getAnimation(animName.toStdString());
            animation->setInterpolationMode(interpolationMode);
        }
    }
}


//!
//! Texture Reload Function
//!
void MocapMeshNode::reloadTexture ()
{
	Parameter *parameter = dynamic_cast<Parameter *>(sender());
	NumberParameter *numberParameter = static_cast<NumberParameter *>(parameter);

	Ogre::TextureManager::ResourceMapIterator textureResourceMapIterator = Ogre::TextureManager::getSingleton().getResourceIterator();
    while (textureResourceMapIterator.hasMoreElements())
        {
            Ogre::TexturePtr texturePtr = textureResourceMapIterator.getNext();
            std::string textureName = texturePtr->getName();
            std::string groupName = texturePtr->getGroup();
            if (textureName.empty())
            {
                continue;
            }                        
			if (numberParameter->getName().toStdString()=="Reload All Textures")
			{
				texturePtr->reload();					         
			}
			if (!textureName.compare(numberParameter->getName().toStdString()))
            {
				texturePtr->reload();
				break;
            }			
	}
	triggerRedraw();
}

//!
//! Change function for the Material parameters.
//!
void MocapMeshNode::setMaterialParameter ()
{
    Parameter *parameter = dynamic_cast<Parameter *>(sender());
    if (!parameter)
        return;
    else {
        NumberParameter *numberParameter = static_cast<NumberParameter *>(parameter);
		const Ogre::MeshPtr meshPtr = m_entity->getMesh();
		Ogre::Mesh::SubMeshIterator subMeshIter = meshPtr->getSubMeshIterator();
        while (subMeshIter.hasMoreElements()) {
            Ogre::SubMesh* subMesh = subMeshIter.getNext();
            Ogre::String subMeshMaterialName = subMesh->getMaterialName();
			Ogre::MaterialPtr subMeshMaterial;
			QStringList parameter_Material = numberParameter->getName().split("@");					
			if (getBoolValue("Global Material Parameter Update")) {
				subMeshMaterial = (Ogre::MaterialPtr)Ogre::MaterialManager::getSingleton().getByName(subMeshMaterialName); }
			else {				
				subMeshMaterial = (Ogre::MaterialPtr)Ogre::MaterialManager::getSingleton().getByName(parameter_Material[1].toStdString()); }
            if (!subMeshMaterial.isNull()) {
                Ogre::Technique* tech = subMeshMaterial->getTechnique(0);
                if (!tech)
                    continue;
                Ogre::Technique::PassIterator passIter = tech->getPassIterator();
				while (passIter.hasMoreElements()) {
                    Ogre::Pass* pass = passIter.getNext();
                    if (pass->hasFragmentProgram()) {
                        Ogre::GpuProgramParametersSharedPtr fpParams = pass->getFragmentProgramParameters();
						if (fpParams->_findNamedConstantDefinition(parameter_Material[0].toStdString()) != 0)
							// float
							if (numberParameter->getType() == Parameter::T_Float) {
								fpParams->setNamedConstant(parameter_Material[0].toStdString(), (Ogre::Real) numberParameter->getValue().toDouble());}
							// color
							if (numberParameter->getType() == Parameter::T_Color) {
								QColor color = getColorValue(numberParameter->getName());								
								Ogre::ColourValue oColor;
								oColor.r = color.redF();
								oColor.g = color.greenF();
								oColor.b = color.blueF();								
								oColor.a = color.alphaF();							
								fpParams->setNamedConstant(parameter_Material[0].toStdString(), oColor)	;			
							}				
                    }
                    if (pass->hasVertexProgram()) {
                        Ogre::GpuProgramParametersSharedPtr vpParams = pass->getVertexProgramParameters();
						if (vpParams->_findNamedConstantDefinition(parameter_Material[0].toStdString()) != 0)
                            vpParams->setNamedConstant(parameter_Material[0].toStdString(), (Ogre::Real) numberParameter->getValue().toDouble());
                    }
					if (pass->hasGeometryProgram()) {
						Ogre::GpuProgramParametersSharedPtr gpParams = pass->getGeometryProgramParameters();
                        if (gpParams->_findNamedConstantDefinition(parameter_Material[0].toStdString()) != 0)
                            gpParams->setNamedConstant(parameter_Material[0].toStdString(), (Ogre::Real) numberParameter->getValue().toDouble());
                    }
				}
            }
        }
    }
	if (m_manualMat)
		triggerRedraw();
}


//!
//! Check existing material for BRDF shader and set the loaded light textures.
//! 
//! \return True when BRDF material was found.
//!
bool MocapMeshNode::setupMaterial ()
{
    bool found = false;

	if (!m_entity)
		return 0;

    const Ogre::MeshPtr meshPtr = m_entity->getMesh();

    Ogre::Mesh::SubMeshIterator subMeshIter = meshPtr->getSubMeshIterator();
    while (subMeshIter.hasMoreElements()) {
        Ogre::SubMesh* subMesh = subMeshIter.getNext();
        Ogre::String subMeshMaterialName = subMesh->getMaterialName();
        Ogre::MaterialPtr subMeshMaterial = (Ogre::MaterialPtr)Ogre::MaterialManager::getSingleton().getByName(subMeshMaterialName);
        if (!subMeshMaterial.isNull()) {
            if (subMeshMaterial->getTechnique("BRDF") && subMeshMaterial->getNumSupportedTechniques() > 1)
                subMeshMaterial->removeTechnique(0);
            Ogre::Material::TechniqueIterator techIter = subMeshMaterial->getTechniqueIterator();
            while (techIter.hasMoreElements()) {
                Ogre::Technique* tech = techIter.getNext();
                Ogre::Technique::PassIterator passIter = tech->getPassIterator();
                while (passIter.hasMoreElements()) {
                    Ogre::Pass* pass = passIter.getNext();
                    if (!pass->hasFragmentProgram())
                        continue;
                    Ogre::GpuProgramParametersSharedPtr fpParams = pass->getFragmentProgramParameters();
                    Ogre::Pass::TextureUnitStateIterator texUnitStateIter = pass->getTextureUnitStateIterator();
                    while (texUnitStateIter.hasMoreElements()) {
                        Ogre::TextureUnitState* texUnitState = texUnitStateIter.getNext();
                        if (texUnitState->getName() == "LightDirs") {
                            texUnitState->setTextureName(m_lightDirsTexture->getName());
                            found = true;
                        }
                        if (texUnitState->getName() == "LightPows") {
                            texUnitState->setTextureName(m_lightPowsTexture->getName());
                            found = true;
                        }
                    }
                }
            }
        }
    }

    parseMaterialParameters ();

    /*if (found)
    meshPtr->reload();*/
    return found;
}


//!
//! Loads an light description file.
//!
//! \return True when light description file has been successfully loaded.
//!
bool MocapMeshNode::loadLightDescriptionFile ( )
{
    QString path = getStringValue("Light Description File");
    bool result = false;
    QFile inFile(path);

    float* pDestDirs = 0;
    float* pDestPows = 0;

    Ogre::HardwarePixelBufferSharedPtr pixelBufferDirs;
    Ogre::HardwarePixelBufferSharedPtr pixelBufferPows;

    if (inFile.open(QIODevice::ReadOnly | QIODevice::Text) && m_entity) {

        if (!inFile.atEnd()) {
            QString line = inFile.readLine();
            int length = line.section(" ", 1, 1).toInt();

            if(!m_lightDirsTexture.isNull() && Ogre::TextureManager::getSingleton().resourceExists(m_lightDirsTexture->getName()))
                Ogre::TextureManager::getSingleton().remove(m_lightDirsTexture->getName());

            if(!m_lightPowsTexture.isNull() && Ogre::TextureManager::getSingleton().resourceExists(m_lightPowsTexture->getName()))
                Ogre::TextureManager::getSingleton().remove(m_lightPowsTexture->getName());

            Ogre::String lightDirsTextureName = createUniqueName("LightDirsTexture");
            Ogre::String lightPowsTextureName = createUniqueName("LightPowsTexture");

            m_lightDirsTexture = Ogre::TextureManager::getSingleton().createManual(lightDirsTextureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_1D, length, 1, 0, Ogre::PF_FLOAT32_RGB, Ogre::TU_STATIC_WRITE_ONLY);
            m_lightPowsTexture = Ogre::TextureManager::getSingleton().createManual(lightPowsTextureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_1D, length, 1, 0, Ogre::PF_FLOAT32_RGB, Ogre::TU_STATIC_WRITE_ONLY);
        }

        if (!m_lightDirsTexture.isNull() && !m_lightDirsTexture.isNull()) {
            // Get the pixel buffer
            pixelBufferDirs = m_lightDirsTexture->getBuffer();
            pixelBufferPows = m_lightPowsTexture->getBuffer();

            // Lock the pixel buffer and get a pixel box
            pixelBufferDirs->lock(Ogre::HardwareBuffer::HBL_DISCARD); // for best performance use HBL_DISCARD!
            pixelBufferPows->lock(Ogre::HardwareBuffer::HBL_DISCARD); // for best performance use HBL_DISCARD!
            const Ogre::PixelBox& pixelBoxDirs = pixelBufferDirs->getCurrentLock();
            const Ogre::PixelBox& pixelBoxPows = pixelBufferPows->getCurrentLock();
            pDestDirs = static_cast<float*>(pixelBoxDirs.data);
            pDestPows = static_cast<float*>(pixelBoxPows.data);
        }

        while (!inFile.atEnd()) {
            QString line = inFile.readLine();
            QString prefix = line.section(" ", 0, 0);
            if (prefix == "Dir:") {
                *pDestDirs++ = (float) line.section(" ", 1, 1).toFloat();
                *pDestDirs++ = (float) line.section(" ", 2, 2).toFloat();
                *pDestDirs++ = (float) line.section(" ", 3, 3).toFloat();
            }
            else if (prefix == "Pow:") {
                *pDestPows++ = (float) line.section(" ", 1, 1).toFloat();
                *pDestPows++ = (float) line.section(" ", 2, 2).toFloat();
                *pDestPows++ = (float) line.section(" ", 3, 3).toFloat();
            }

        }

        // Unlock the pixel buffer
        pixelBufferDirs->unlock();
        pixelBufferPows->unlock();

        result = setupMaterial();
    } 
    else
        Log::error(QString("Document import failed. \"%1\" not found.").arg(path), "LightDescriptionNode::loadReferenceDataFile");

    inFile.close();
    return result;
}


//!
//! Sets the internal config switches
//!
void MocapMeshNode::setSwitches()
{
	Parameter *parameter = dynamic_cast<Parameter *>(sender());
   
	if (!parameter)
        return;

	const QString name = parameter->getName();
	const bool value = parameter->getValue().toBool();

	if (name == "manual")
		m_manual = value;
	else if (name == "bones")
		m_bones = value;
	else if (name == "autoPose")
		m_autoPose = value;
	else if (name == "manualMat")
		m_manualMat = value;
}


//!
//! Insert unique IDs in a texture coordinate for every vertex of the mesh that is
//! on the same spatial position
//!
//! \param mesh Submesh that contains the vertices (typically the skin submesh)
//!	\return Pointer to the VertexBuffer that gets modified by this function
//!
Ogre::HardwareVertexBufferSharedPtr MocapMeshNode::writeDataToVertices(Ogre::SubMesh* mesh) 
{
#ifdef WRITE_VERTEXBUFFER_LOG
	std::ofstream out("MocapMeshNode-WriteDataToVertices.log");
#endif

	// get Buffers
	// In Buffer 0 are normally the POSITIONs and the NORMALS
	Ogre::HardwareVertexBufferSharedPtr vertexBuffer = mesh->vertexData->vertexBufferBinding->getBuffer(0); 
	Ogre::HardwareIndexBufferSharedPtr indexBuffer = mesh->indexData->indexBuffer;

	// some general information about the buffers
	size_t numberOfBuffers = mesh->vertexData->vertexBufferBinding->getBufferCount();
	unsigned short bufferID = short(numberOfBuffers);
	unsigned short coordNr = 1;

	// get some information about the vertices
	size_t vertexAmount = mesh->vertexData->vertexCount;
	size_t vertexSizeInByteOfBuffer0 = mesh->vertexData->vertexDeclaration->getVertexSize(0); // Vertex Size in Byte

	// get some attributes of the indexBuffer
	size_t numberOfIndexBufferEntries = indexBuffer->getNumIndexes();
	size_t sizePerIndex = indexBuffer->getIndexSize();
	size_t IndexBufferSizeInByte = indexBuffer->getSizeInBytes();

#ifdef WRITE_VERTEXBUFFER_LOG
	// Output in file for testing
	out << "Anzahl der gebundenen Buffer: " << numberOfBuffers << std::endl;
	out << "Anzahl der Vertices: " << vertexAmount << std::endl;
	out << "VertexSize von Buffer 0 in Byte davor: " << mesh->vertexData->vertexDeclaration->getVertexSize(0) << std::endl;
	out << "Der neue Buffer hat die ID: " << bufferID << std::endl;
#endif

	// define two Elements for a new HardwareVertexBuffer
	mesh->vertexData->vertexDeclaration->addElement(bufferID, 0, Ogre::VET_FLOAT1, Ogre::VES_BINORMAL);
#ifdef WRITE_VERTEXBUFFER_LOG
    out << "VertexSize vom neuen Buffer in Byte: " << mesh->vertexData->vertexDeclaration->getVertexSize(bufferID) << std::endl;
#endif
	// generate the new HardwareVertexBuffer
	Ogre::HardwareVertexBufferSharedPtr specialDataVertexBufferPtr = Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
		mesh->vertexData->vertexDeclaration->getVertexSize(bufferID),
		mesh->vertexData->vertexCount,
		Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);

	// bind the new Vertexbuffer
	mesh->vertexData->vertexBufferBinding->setBinding(bufferID, specialDataVertexBufferPtr);

	// lock the new vertex buffer for writing
	float* bufferPtr = static_cast<float*>(specialDataVertexBufferPtr->lock(Ogre::HardwareBuffer::HBL_DISCARD));
	float* bufferPtrStart = bufferPtr; // just for a test

	QList<QVector3D*> vertices;


	// Locking the old vertexbuffer
	float* bufferPtrOld = static_cast<float*>(vertexBuffer->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
	float* bufferPtrOldStart = bufferPtrOld;

	// Copy data to new buffer
#ifdef WRITE_VERTEXBUFFER_LOG
	out << std::endl << "### VertexBuffer wird durchlaufen !!!" << std::endl;
#endif
	int numberOfFloatsPerVertex = int(vertexSizeInByteOfBuffer0 / 4); // 4 = sizeof(float)

	float positionX;
	float positionY;
	float positionZ;

	QVector3D temp;

	for(Ogre::uint i=0; i<vertexAmount; i++) {
#ifdef WRITE_VERTEXBUFFER_LOG
		out << "Vertex-Nr. " << i;
#endif
		// 3 floats for the positions (x,y,z)

		positionX = *bufferPtrOld++;
#ifdef WRITE_VERTEXBUFFER_LOG
		out << "  =  PositionX:"<< *bufferPtrOld;
#endif
		positionY = *bufferPtrOld++;
#ifdef WRITE_VERTEXBUFFER_LOG
		out << "  PositionY:"<< *bufferPtrOld;
#endif
		positionZ = *bufferPtrOld;
#ifdef WRITE_VERTEXBUFFER_LOG
		out << "  PositionZ:"<< *bufferPtrOld << std::endl;
#endif		
		temp.setX(positionX);
		temp.setY(positionY);
		temp.setZ(positionZ);

		// generate the list with all vertex positions in it once

		if (vertices.length() == 0){
			vertices.append(new QVector3D(temp));
			*bufferPtr++ = (float) 0;
		} else {
			for (int vi=0; vi<vertices.length(); vi++) {
				if (compareVectors(vertices[vi], &temp)) {
					*bufferPtr++ = (float) vi+1;
					vi = vertices.length();
				} else if(vi == vertices.length()-1){
					vertices.append(new QVector3D(temp));
					*bufferPtr++ = (float) vi+1;
					vi += 2;
				}
			}
		}

	}

#ifdef WRITE_VERTEXBUFFER_LOG
	// Just for a Test
	out << std::endl << std::endl << "QList vertices enth�lt nun " << vertices.length() << " Vertices" << std::endl << std::endl;
	out << std::endl << std::endl << "Probedurchlauf: " << std::endl << std::endl;

	for (int iter1 = 0; iter1 < specialDataVertexBufferPtr->getNumVertices(); iter1++){
		out << "Vertex Nr. ------------------- " << iter1 << std::endl;
		for (int iter2 = 0; iter2 < (specialDataVertexBufferPtr->getVertexSize()/4); iter2++){
			out << *bufferPtrStart++ << std::endl;
		}
	}
#endif
	bufferPtrOld = bufferPtrOldStart;

#ifdef WRITE_VERTEXBUFFER_LOG
	// Output in file for testing
	out << "VertexSize danach: " << mesh->vertexData->vertexDeclaration->getVertexSize(0) << std::endl << std::endl;
	out << "### IndexBuffer wird durchlaufen !!!" << std::endl;
	out << "Operation Type (4 = OT_TRIANGLE_LIST): " << mesh->operationType << std::endl;
	out << "Einr�ge im Indexbuffer: " << numberOfIndexBufferEntries << std::endl;
	out << "Gr��e der Eintr�ge im IndexBuffer: " << sizePerIndex << std::endl;
	out << "Gr��e des IndexBuffer: " << IndexBufferSizeInByte << std::endl;
#endif

	Ogre::HardwareIndexBuffer::IndexType typ = indexBuffer->getType();

	vertexBuffer->unlock();
	specialDataVertexBufferPtr->unlock();

#ifdef WRITE_VERTEXBUFFER_LOG
	// Output in file for testing
	out << std::endl;
	out << "Anzahl der gebundenen Buffer: " << mesh->vertexData->vertexBufferBinding->getBufferCount() << std::endl;
	out << "Anzahl der Vertices: " << mesh->vertexData->vertexCount << std::endl;
	out << "VertexSize von Buffer 0 in Byte danach: " << mesh->vertexData->vertexDeclaration->getVertexSize(0) << std::endl;

	out.close();
#endif

	// Reorganize bufferstructure
#if(OGRE_VERSION >= 0x010800)
	mesh->vertexData->reorganiseBuffers(mesh->vertexData->vertexDeclaration->getAutoOrganisedDeclaration(true, false, false));
#else
	mesh->vertexData->reorganiseBuffers(mesh->vertexData->vertexDeclaration->getAutoOrganisedDeclaration(true, false));
#endif
	return vertexBuffer;
}

//!
//! Compare two QVector3D instances.
//!
//! \param First vector.
//! \param Second vector.
//! \return True if vectors are component-wise equal.
//!
bool MocapMeshNode::compareVectors(QVector3D* v1, QVector3D* v2){
	if (v1->x() == v2->x() && 
		v1->y() == v2->y() && 
		v1->z() == v2->z()){
		return true;
	}
	return false;
}

//!
//! Checks the Ogre entity if it is animatable.
//! \return True if the entity is animated
//!
bool MocapMeshNode::isAnimated () const
{
	return m_entity->hasVertexAnimation() || m_entity->hasSkeleton();
}