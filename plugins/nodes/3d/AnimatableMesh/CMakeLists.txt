project(animatablemesh)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# animatablemesh
set( animatablemesh_h ${animatablemesh_h}		
			AnimatableMeshNode.h
			AnimatableMeshNodePlugin.h
			)

set( animatablemesh_moc ${animatablemesh_moc}	
			AnimatableMeshNode.h
			AnimatableMeshNodePlugin.h
			)

set( animatablemesh_src ${animatablemesh_src}	
			AnimatableMeshNode.cpp
			AnimatableMeshNodePlugin.cpp
			)

set( animatablemesh_res ${animatablemesh_res}	
			animatablemesh.xml
			)

# Create moc files		   
qt4_wrap_cpp(animatablemesh_cxx ${animatablemesh_moc})

# Create source groups
source_group("Moc Files" FILES ${animatablemesh_cxx})
source_group("Header Files" FILES ${animatablemesh_h})
source_group("Resources" FILES ${animatablemesh_res})

# Add header files to sources to make headers visible in Visual Studio
set(animatablemesh_src ${animatablemesh_src} ${animatablemesh_h} ${animatablemesh_res})

# Create static library
add_library(animatablemesh SHARED ${animatablemesh_src} ${animatablemesh_cxx})

# Add library dependencies
target_link_libraries(animatablemesh
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
install( FILES ${animatablemesh_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libanimatablemesh_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libanimatablemesh.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS animatablemesh RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
