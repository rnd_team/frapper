/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2010 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

//!
//! \file "ShadowMapRenderNode.cpp"
//! \brief Implementation file for ShadowMapRenderNode class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \version    1.0
//! \date       11.10.2010 (last updated)
//!

#include "ShadowMapRenderNode.h"
#include "GeometryRenderNode.h"
#include "Parameter.h"
#include "SceneNodeParameter.h"
#include "OgreTools.h"
#include "OgreContainer.h"
#include "OgreManager.h"
#include <QtCore/QFile>



void ShadowListener::shadowTextureCasterPreViewProj (Ogre::Light *light, Ogre::Camera *camera, size_t iteration)
{
	float range = light->getAttenuationRange();
    camera->setNearClipDistance(range * 0.01);
    camera->setFarClipDistance(range);
}

///
/// Constructors and Destructors
///

//!
//! Constructor of the ShadowMapRenderNode class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
ShadowMapRenderNode::ShadowMapRenderNode ( const QString &name, ParameterGroup *parameterRoot ) :
    GeometryRenderNode(name, parameterRoot),
	m_lightScale(30)
{
	setValue(CameraLightToggleParameterName, false);
	if (m_sceneManager) {
		//ShadowListener *shadowListener = new ShadowListener();
		//m_sceneManager->addListener(shadowListener);
        m_sceneManager->setAmbientLight(Ogre::ColourValue(0.0, 0.0, 0.0));
	}

	// Set the change function for light description file parameter.
    setChangeFunction("Light Description File", SLOT(loadLightDescriptionFile()));
    setChangeFunction("Light Scale Pos", SLOT(setLightScale()));
	setChangeFunction("Number of Lights", SLOT(setNbrLights()));
    setChangeFunction("lightPower", SLOT(setShaderParameter()));
    setChangeFunction("vsmEpsilon", SLOT(setShaderParameter()));
    setChangeFunction("sampleRadius", SLOT(setShaderParameter()));
    setChangeFunction("blurSize", SLOT(setShaderParameter()));
    setChangeFunction("scaleBias", SLOT(setShaderParameter()));
 
	m_materialSwitchListener = new MaterialSwitcher();
	Ogre::MaterialManager::getSingleton().addListener( m_materialSwitchListener );	

#ifdef LIGHT_DEBUG
	m_outputSecondImageName = QString("Light Map");
#endif

    // load default texture
    m_defaultTextureImage.load("DefaultRenderImage.png", "General");

	Parameter *outputImageParameter = getParameter(m_outputImageName);
    if (outputImageParameter) {
        outputImageParameter->setProcessingFunction(SLOT(processOutputImage()));
        outputImageParameter->setDirty(true);
		// set up parameter affections
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputGeometryParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputLightsParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputCameraParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::CameraLightToggleParameterName));
		outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::BackgroundColorParameterName));
		outputImageParameter->addAffectingParameter(getParameter("Light Description File"));
    }

#ifdef LIGHT_DEBUG
	addOutputParameter(Parameter::createImageParameter(m_outputSecondImageName));

	Parameter *outputImageParameterLight = getParameter(m_outputSecondImageName);
    if (outputImageParameterLight) {
        outputImageParameterLight->setProcessingFunction(SLOT(processOutputImage()));
		outputImageParameterLight->setAuxProcessingFunction(SLOT(redrawTriggered()));
        outputImageParameterLight->setDirty(true);
		// set up parameter affections
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::InputGeometryParameterName));
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::InputLightsParameterName));
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::InputCameraParameterName));
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::CameraLightToggleParameterName));
		outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::BackgroundColorParameterName));
    }
#endif
}


//!
//! Destructor of the ShadowMapRenderNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
ShadowMapRenderNode::~ShadowMapRenderNode ()
{
	m_sceneManager->setShadowTextureSelfShadow(false);
	m_sceneManager->setShadowTechnique(Ogre::ShadowTechnique::SHADOWTYPE_NONE);
	Ogre::MaterialManager::getSingleton().removeListener(m_materialSwitchListener);	
    if (m_sceneManager)
        // destroy all scene nodes and objects in the render scene
        OgreTools::deepDeleteSceneNode(m_sceneManager->getRootSceneNode(), m_sceneManager);
}


///
/// Public Slots
///


//!
//! Processes the node's input data to generate the node's output image.
//!
void ShadowMapRenderNode::processOutputImage ()
{
	//if (m_lightPositions.empty()) {
	//	Log::error("Light description file not loaded.", "ShadowMapRenderNode::processOutputImage");
	//	return;
	//}
	
    Parameter *tempImageParameter = getParameter(m_outputImageName);
    if (!tempImageParameter) {
        Log::error("Could not obtain output image parameter.", "ShadowMapRenderNode::processOutputImage");
        return;
    }

	m_sceneManager->setShadowTextureSelfShadow(false);
	m_sceneManager->setShadowTechnique(Ogre::ShadowTechnique::SHADOWTYPE_NONE);

    // check if a camera light should be used instead of the input lights
    bool useCameraLight = getBoolValue(CameraLightToggleParameterName);

    // get OGRE texture manager
    Ogre::TextureManager &textureManager = Ogre::TextureManager::getSingleton();

    // destroy all scene nodes and objects in the render scene
    OgreTools::deepDeleteSceneNode(m_sceneManager->getRootSceneNode(), m_sceneManager);

    if (m_cameraSceneNode) {
        OgreContainer *sceneNodeContainer = Ogre::any_cast<OgreContainer *>(m_cameraSceneNode->getUserAny());
        if (sceneNodeContainer)
            delete sceneNodeContainer;
        OgreTools::deepDeleteSceneNode(m_cameraSceneNode, m_sceneManager);
        m_cameraSceneNode->removeAndDestroyAllChildren();
        m_sceneManager->destroySceneNode(m_cameraSceneNode);
        m_cameraSceneNode = 0;
		m_cameraCopy = 0;
    }

    // destroy cameras, lights and entities
    m_sceneManager->destroyAllCameras();
    m_sceneManager->destroyAllEntities();
    m_sceneManager->destroyAllLights();

    // process input geometry
    Ogre::SceneNode *geometrySceneNode = getSceneNodeValue(InputGeometryParameterName);
    if (geometrySceneNode) {
        // duplicate the input geometry
        Ogre::SceneNode *geometrySceneNodeCopy = 0;
        OgreTools::deepCopySceneNode(geometrySceneNode, geometrySceneNodeCopy, m_name, m_sceneManager);
        // add the geometry to the render scene
        m_sceneManager->getRootSceneNode()->addChild(geometrySceneNodeCopy);
    } else
        Log::error("Could not obtain scene node from input geometry parameter.", "ShadowMapRenderNode::processOutputImage");

    // process input lights
    if (!useCameraLight) {
#ifdef LIGHT_PARAMETER_AVAILABLE
        // process input lights
        Ogre::SceneNode *lightsSceneNode = getSceneNodeValue(InputLightsParameterName);
        if (lightsSceneNode) {
             duplicate the input lights
            Ogre::SceneNode *lightsSceneNodeCopy = 0;
            OgreTools::deepCopySceneNode(lightsSceneNode, lightsSceneNodeCopy, m_name, m_sceneManager);
             add the lights to the render scene
            m_sceneManager->getRootSceneNode()->addChild(lightsSceneNodeCopy);
        } else
            Log::error("Could not obtain scene node from input lights parameter.", "ShadowMapRenderNode::processOutputImage");
#endif
    }

    // process input camera
    Ogre::SceneNode *cameraSceneNode = getSceneNodeValue(InputCameraParameterName);
	Ogre::RenderTexture *renderTargetLight;
	renderTargetLight = 0;

	setValue(m_outputImageName, m_defaultTexture);
#ifdef LIGHT_DEBUG
	setValue(m_outputSecondImageName, m_defaultTexture);
#endif
	
	if (cameraSceneNode) {
        // get the first camera attached to the input camera scene node
        Ogre::Camera *camera = OgreTools::getFirstCamera(cameraSceneNode);
        
		if (camera) {
			// duplicate the input camera
			delete m_cameraSceneNode; 
			m_cameraSceneNode = 0;
			OgreTools::deepCopySceneNode(cameraSceneNode, m_cameraSceneNode, m_name, m_sceneManager);
            if (!m_cameraSceneNode) {
                Log::error("The camera's scene node could not be copied.", "ShadowMapRenderNode::processOutputImage");
                return;
            }

			// process input light
			Ogre::SceneNode *lightSceneNode = getSceneNodeValue(InputLightsParameterName);
			m_lightSceneNode = lightSceneNode;
			if (lightSceneNode) {
				if (OgreTools::getFirstLight(lightSceneNode)) {
					m_lightSceneNode = 0;
					OgreTools::deepCopySceneNode(lightSceneNode, m_lightSceneNode, m_name, m_sceneManager);
					if (!m_lightSceneNode) {
						Log::error("The light's scene node could not be copied.", "ShadowMapRenderNode::processOutputImage");
						return;
					}
					// set visibility of light gometry representation to false
					Ogre::Entity *entity = OgreTools::getFirstEntity(m_lightSceneNode);
					if (entity)
						entity->setVisible(false);
					m_lightSceneNode->setInheritScale(false);
					m_lightSceneNode->setInheritOrientation(false);
					// add the lights to the render scene
					m_sceneManager->getRootSceneNode()->addChild(m_lightSceneNode);
					// enable cast shadows
					Ogre::SceneNode::ObjectIterator lightIter = m_lightSceneNode->getAttachedObjectIterator();
					while (lightIter.hasMoreElements()) {
						Ogre::Light *light = dynamic_cast<Ogre::Light *>(lightIter.getNext());
						if (light) {
							light->setCastShadows(true);
							light->setAttenuation(50,1,0,0);
						}
					}
				}
			}
            else if (useCameraLight || m_lightPositions.empty()) {
				// set up names for camera light objects
				QString cameraLightName = QString("%1CameraLight").arg(m_name);
				// create camera light objects
				Ogre::Light *cameraLight = m_sceneManager->createLight(cameraLightName.toStdString());
				cameraLight->setType(Ogre::Light::LT_SPOTLIGHT);
				cameraLight->setSpotlightRange(Ogre::Degree(80), Ogre::Degree(90), 1);
				cameraLight->setDirection(-m_cameraSceneNode->getPosition());
				cameraLight->setCastShadows(true);
				m_cameraSceneNode->attachObject(cameraLight);
            }
			else
				setNbrLights();

            // retrieve render resolution values from custom parameter
            const Ogre::Any &customData = dynamic_cast<Ogre::MovableObject *>(camera)->getUserAny();
            if (!customData.isEmpty()) {
                CameraInfo *cameraInfo = Ogre::any_cast<CameraInfo *>(customData);
                if (cameraInfo) {
                    m_renderWidth = cameraInfo->width;
                    m_renderHeight = cameraInfo->height;
                }
            }	        

			// copy first camera
			m_cameraCopy = OgreTools::getFirstCamera(m_cameraSceneNode);
			m_cameraCopy->setFarClipDistance(200);
			m_cameraCopy->setNearClipDistance(.1);

			// (re-)create the render texture
			initializeRenderTarget(m_cameraCopy, Ogre::ColourValue(1.0f,1.0f,1.0f), Ogre::PF_L8);
			m_viewport->setMaterialScheme("Shadow");

			//Ogre::FocusedShadowCameraSetup *setup = new Ogre::FocusedShadowCameraSetup();
			Ogre::LiSPSMShadowCameraSetup *setup = new Ogre::LiSPSMShadowCameraSetup();
			setup->setOptimalAdjustFactor(0.4);
			Ogre::ShadowCameraSetupPtr shadowCameraSetup = Ogre::ShadowCameraSetupPtr(setup);
			m_sceneManager->setShadowCameraSetup(shadowCameraSetup);

			if (geometrySceneNode) {
				const int nbrLights = getIntValue("Number of Lights");

				m_sceneManager->setShadowTextureSelfShadow(true);
				m_sceneManager->setShadowTextureCasterMaterial("ShadowMapRender/LightZ");
				m_sceneManager->setShadowTextureSettings(512, nbrLights, Ogre::PF_FLOAT32_GR);
				m_sceneManager->setShadowCasterRenderBackFaces(false);

				const unsigned numShadowRTTs = m_sceneManager->getShadowTextureCount();
				for (unsigned i = 0; i < numShadowRTTs; ++i) {
					Ogre::TexturePtr tex = m_sceneManager->getShadowTexture(i);
					Ogre::Viewport *vp = tex->getBuffer()->getRenderTarget()->getViewport(0);
					vp->setBackgroundColour(Ogre::ColourValue(1, 1, 1, 1));
					vp->setClearEveryFrame(true);
				}
				m_sceneManager->setShadowTechnique(Ogre::ShadowTechnique::SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED);
			}

			// update render texture and render target
			setValue(m_outputImageName, m_renderTexture);

			getParameter(m_outputImageName)->setDirty(false);

#ifdef LIGHT_DEBUG
			setValue(m_outputSecondImageName, m_sceneManager->getShadowTexture(0));
			getParameter(m_outputSecondImageName)->setDirty(false);
#endif
		}
    }
	else
		Log::error("First object attached to scene node contained in input camera parameter is not a camera.", "ShadowMapRenderNode::processOutputImage");

		RenderNode::redrawTriggered();
}


//private functions

//!
//! Returns the greatest componen of the vector
//!
//! \return The greatest componen of the vector

void ShadowMapRenderNode::redrawTriggered()
{
	RenderNode::redrawTriggered();

#ifdef LIGHT_DEBUG
	getParameter(m_outputSecondImageName)->setAuxDirty(false);
#endif
}

float ShadowMapRenderNode::greatestComponent(const Ogre::Vector3 &vec) const
{
    return qMax<float>( qMax<float>(vec.x, vec.y), vec.z);
}


//!
//! The less then function for light power sorting
//!
bool ShadowMapRenderNode::greaterThan(const QPair<Ogre::Vector3, Ogre::Vector3> &d1, const QPair<Ogre::Vector3, Ogre::Vector3> &d2)
{
	return d1.second.length() > d2.second.length();
}

//protected functions

//!
//! Loads an XML reference data file.
//!
//! Is called when the value of the Reference Data File parameter has
//! changed.
//!
//! \return True if the reference data was successfully loaded from file, otherwise False.
//!
void ShadowMapRenderNode::loadLightDescriptionFile ()
{
    QString path = getStringValue("Light Description File");
    QFile inFile(path);

	m_lightPositions.clear();

	// parse the file and fill the pos/pow pair into a list
    if (inFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QPair<Ogre::Vector3, Ogre::Vector3> newData(Ogre::Vector3(0,0,0), Ogre::Vector3(0,0,0));
		while (!inFile.atEnd()) {
			QString line = inFile.readLine();
			QString prefix = line.section(" ", 0, 0);
			if (prefix == "Dir:") {
				Ogre::Vector3 pos(
					line.section(" ", 1, 1).toFloat(),
					line.section(" ", 2, 2).toFloat(),
					line.section(" ", 3, 3).toFloat() );
				newData.first = pos;
			}
			else if (prefix == "Pow:") {
				Ogre::Vector3 pow(
					line.section(" ", 1, 1).toFloat(),
					line.section(" ", 2, 2).toFloat(),
					line.section(" ", 3, 3).toFloat() );
				newData.second = pow;
			}
			if (newData.first.length() != 0 && newData.second.length() != 0) {
				m_lightPositions.append(newData);
				newData = QPair<Ogre::Vector3, Ogre::Vector3>(Ogre::Vector3(0,0,0), Ogre::Vector3(0,0,0));
			}
		}
    } else
        Log::error(QString("Document import failed. \"%1\" not found.").arg(path), "LightDescriptionNode::loadReferenceDataFile");
    
	inFile.close();

	// sort the list: greatest pow first
	qSort(m_lightPositions.begin(), m_lightPositions.end(), greaterThan);

	NumberParameter *nbrLights = getNumberParameter("Number of Lights");
	nbrLights->setMaxValue(m_lightPositions.length());
	processOutputImage();
}

//!
//! Sets the scene light scale value
//!
void ShadowMapRenderNode::setLightScale ()
{
	m_lightScale = getDoubleValue("Light Scale Pos");
	setNbrLights();
}

//!
//! Sets the number of lights for the sadow calculation
//!
void ShadowMapRenderNode::setNbrLights()
{
	if (m_lightSceneNode)
		return;

	if (m_lightPositions.empty())
		return;

	const int nbrLights = getIntValue("Number of Lights");
	m_sceneManager->destroyAllLights();

	unsigned int count = 0;
	const float max = greatestComponent(m_lightPositions.first().second);
	for (QList<QPair<Ogre::Vector3, Ogre::Vector3>>::iterator iter = m_lightPositions.begin(); iter != m_lightPositions.end(); ++iter, ++count) {
		const Ogre::Vector3 pos = iter->first * m_lightScale;
		const float length = (iter->second / max).length();
		Ogre::Light *light = m_sceneManager->createLight("Light" + QString::number(count).toStdString() );
		light->setType(Ogre::Light::LT_SPOTLIGHT);
		light->setPosition(pos);
		light->setSpotlightRange(Ogre::Degree(70), Ogre::Degree(90));
		light->setAttenuation(30, 1, 1, 1);
		light->setDirection(-pos);
		light->setDiffuseColour(length, length, length);
		light->setSpecularColour(0,0,0);
		light->setCastShadows(true);
		if (count == nbrLights-1) break;
	}

	m_sceneManager->setShadowTextureCount(nbrLights);
	redrawTriggered();
}

//!
//! Sets the scenes shader parameter
//!
void ShadowMapRenderNode::setShaderParameter()
{
	Parameter* parameter = static_cast<Parameter*>(sender());
	const Ogre::String &name = parameter->getName().toStdString();
    const float value = parameter->getValue().toFloat();

	Ogre::MaterialPtr matPtr = Ogre::MaterialPtr(Ogre::MaterialManager::getSingleton().getByName("ShadowMapRender/Shadow"));
	Ogre::GpuProgramParametersSharedPtr params = matPtr->getTechnique(0)->getPass(1)->getFragmentProgramParameters();

	if (!params->_findNamedConstantDefinition(name))
		params = matPtr->getTechnique(0)->getPass(1)->getVertexProgramParameters();
	if (!params->_findNamedConstantDefinition(name))
		return;

	params->setNamedConstant(name, value);

	redrawTriggered();
}

///
/// Constructors and Destructors
///

//!
//! Constructor of the MaterialSwitcher class.
//!
MaterialSwitcher::MaterialSwitcher () :
    m_shadowTechnique(0)
{
	Ogre::MaterialPtr baseMat = Ogre::MaterialPtr(Ogre::MaterialManager::getSingleton().getByName("ShadowMapRender/Shadow"));
	if (!baseMat.isNull()) {
	    baseMat->load();
	    m_shadowTechnique = baseMat->getBestTechnique();
    }
}

//!
//! Destructor of the MaterialSwitcher class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
MaterialSwitcher::~MaterialSwitcher ()
{
}


///
/// Public Functions
///
	
Ogre::Technique* MaterialSwitcher::handleSchemeNotFound(unsigned short schemeIndex, 
														const Ogre::String& schemeName,
														Ogre::Material* originalMaterial, 
									                    unsigned short lodIndex,
														const Ogre::Renderable* rend)
{
		if (schemeName == "Shadow")
			return m_shadowTechnique;
		return NULL;
}