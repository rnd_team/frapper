/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

//!
//! \file "AdvancedRenderNode.cpp"
//! \brief Implementation file for AdvancedRenderNode class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \author     Nils Zweiling <nils.zweiling@filmakademie.de>
//! \author     Stefan Habel <stefan.habel@filmakademie.de>
//! \author		Felix Bucella <felix.bucella@filmakademie.de>
//! \version    1.6
//! \date       17.05.2011 (last updated)
//!

#include "AdvancedRenderNode.h"
#include "GeometryRenderNode.h"
#include "Parameter.h"
#include "SceneNodeParameter.h"
#include "OgreTools.h"
#include "OgreContainer.h"
#include "OgreManager.h"



///
/// Constructors and Destructors
///


//!
//! Constructor of the AdvancedRenderNode class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
AdvancedRenderNode::AdvancedRenderNode ( const QString &name, ParameterGroup *parameterRoot ) :
    GeometryRenderNode(name, parameterRoot),
	m_multiRenderTargetName(""),
	m_blendWeightTextureName(""),
	m_multiRenderTarget(0)
{
	if (m_sceneManager) {
        m_sceneManager->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
		m_blendWeightTextureName = createUniqueName("BlendWeightTexture");
		m_multiRenderTargetName = createUniqueName("MRT");
	}

	m_materialSwitchListener = new MaterialSwitcher();
	Ogre::MaterialManager::getSingleton().addListener(m_materialSwitchListener);

	// load default texture
    m_defaultTextureImage.load("DefaultRenderImage.png", "General");

	// manually create textue names we'll need
	m_outParameterNameList.append(QString("Normal Map"));
	m_outParameterNameList.append(QString("Depth Map"));
	m_outParameterNameList.append(QString("View Map"));
    m_outParameterNameList.append(QString("Object Id Map"));

	Parameter *outputImageParameter = getParameter(m_outputImageName);
	outputImageParameter->setProcessingFunction(SLOT(processOutputImage()));
	outputImageParameter->setDirty(true);

	// set up parameter affections
    outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputGeometryParameterName));
    outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputLightsParameterName));
    outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputCameraParameterName));
    outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::CameraLightToggleParameterName));
	outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::BackgroundColorParameterName));

	// add second output image parameter for the normal map
	for (size_t i=0; i<m_outParameterNameList.size(); ++i) {
		const QString &name = m_outParameterNameList[i];
		outputImageParameter = Parameter::createImageParameter(name);
		addOutputParameter(outputImageParameter);
		outputImageParameter->setProcessingFunction(SLOT(processOutputImage()));
		outputImageParameter->setAuxProcessingFunction(SLOT(redrawTriggered()));
		outputImageParameter->setDirty(true);
		
		// set up parameter affections
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputGeometryParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputLightsParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputCameraParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::CameraLightToggleParameterName));
		outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::BackgroundColorParameterName));
		outputImageParameter->addAffectingParameter(getParameter("Render Target Multiplier"));
		
		// generate texture
		generateTexture(name, Ogre::TEX_TYPE_2D, m_renderWidth, m_renderHeight, 0, Ogre::PF_FLOAT16_RGB, Ogre::TU_RENDERTARGET);
	}
	
	// for debug...
	/*generateTexture(m_outParameterNameList[0], Ogre::TEX_TYPE_2D, m_renderWidth, m_renderHeight, 0, Ogre::PF_FLOAT16_RGB, Ogre::TU_RENDERTARGET);
	generateTexture(m_outParameterNameList[1], Ogre::TEX_TYPE_2D, m_renderWidth, m_renderHeight, 0, Ogre::PF_FLOAT16_RGB, Ogre::TU_RENDERTARGET);
	generateTexture(m_outParameterNameList[2], Ogre::TEX_TYPE_2D, m_renderWidth, m_renderHeight, 0, Ogre::PF_FLOAT16_RGB, Ogre::TU_RENDERTARGET);
	generateTexture(m_outParameterNameList[3], Ogre::TEX_TYPE_2D, m_renderWidth, m_renderHeight, 0, Ogre::PF_FLOAT16_RGB, Ogre::TU_RENDERTARGET);*/

	setupTextures();

#ifdef WRINKLE_DEBUG
	m_outputWrinkleImageName = "WrinkleWeightMap";
	addOutputParameter(Parameter::createImageParameter(m_outputWrinkleImageName));

	Parameter *outputImageParameterLight = getParameter(m_outputWrinkleImageName);
    if (outputImageParameterLight) {
        outputImageParameterLight->setProcessingFunction(SLOT(processOutputImage()));
		outputImageParameterLight->setAuxProcessingFunction(SLOT(redrawTriggered()));
        outputImageParameterLight->setDirty(true);
		// set up parameter affections
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::InputGeometryParameterName));
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::InputLightsParameterName));
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::InputCameraParameterName));
        outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::CameraLightToggleParameterName));
		outputImageParameterLight->addAffectingParameter(getParameter(GeometryRenderNode::BackgroundColorParameterName));
    }
#endif
}


//!
//! Destructor of the AdvancedRenderNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
AdvancedRenderNode::~AdvancedRenderNode ()
{
	Ogre::TextureManager &textureManager = Ogre::TextureManager::getSingleton();
	if (textureManager.resourceExists(m_blendWeightTextureName)) {
		Ogre::RenderTexture* blendWeightTextureTarget = m_blendWeightTexture->getBuffer()->getRenderTarget();
		blendWeightTextureTarget->removeAllViewports();
		blendWeightTextureTarget->removeAllListeners();
		textureManager.remove(m_blendWeightTextureName);
	}

	if (m_multiRenderTarget) {
		m_multiRenderTarget->removeAllViewports();
		m_multiRenderTarget->removeAllListeners();
		Ogre::Root::getSingleton().getRenderSystem()->destroyRenderTarget(m_multiRenderTargetName);
	}

	Ogre::MaterialManager::getSingleton().removeListener(m_materialSwitchListener);
}


///
/// Public Slots
///


//!
//! Processes the node's input data to generate the node's output image.
//!
void AdvancedRenderNode::processOutputImage ()
{	
    // check if a camera light should be used instead of the input lights
    bool useCameraLight = getBoolValue(CameraLightToggleParameterName);

    // destroy all scene nodes and objects in the render scene
    OgreTools::deepDeleteSceneNode(m_sceneManager->getRootSceneNode(), m_sceneManager);

    if (m_cameraSceneNode) {
        OgreContainer *sceneNodeContainer = Ogre::any_cast<OgreContainer *>(m_cameraSceneNode->getUserAny());
        if (sceneNodeContainer)
            delete sceneNodeContainer;
        OgreTools::deepDeleteSceneNode(m_cameraSceneNode, m_sceneManager);
        m_cameraSceneNode->removeAndDestroyAllChildren();
        m_sceneManager->destroySceneNode(m_cameraSceneNode);
        m_cameraSceneNode = 0;
		m_cameraCopy = 0;
    }

    // destroy cameras, lights and entities
    m_sceneManager->destroyAllCameras();
    m_sceneManager->destroyAllEntities();
    m_sceneManager->destroyAllLights();

	for (size_t i=0; i<m_outParameterNameList.size(); ++i)
		setValue(m_outParameterNameList[i], m_defaultTexture);

    // process input geometry
    Ogre::SceneNode *geometrySceneNode = getSceneNodeValue(InputGeometryParameterName);
    if (geometrySceneNode) {
        // duplicate the input geometry
        Ogre::SceneNode *geometrySceneNodeCopy = 0;
        OgreTools::deepCopySceneNode(geometrySceneNode, geometrySceneNodeCopy, m_name, m_sceneManager);
        
        // add the geometry to the render scene
        m_sceneManager->getRootSceneNode()->addChild(geometrySceneNodeCopy);
	    //m_sceneManager->getRootSceneNode()->addChild(geometrySceneNode);
	} else {
        Log::error("Could not obtain scene node from input geometry parameter.", "AdvancedRenderNode::processOutputImage");
		return;
	}

#ifdef LIGHT_PARAMETER_AVAILABLE
	// process input lights
    if (!useCameraLight) {
        // process input lights
        Ogre::SceneNode *lightsSceneNode = getSceneNodeValue(InputLightsParameterName);
        if (lightsSceneNode) {
             duplicate the input lights
            Ogre::SceneNode *lightsSceneNodeCopy = 0;
            OgreTools::deepCopySceneNode(lightsSceneNode, lightsSceneNodeCopy, m_name, m_sceneManager);
             add the lights to the render scene
            m_sceneManager->getRootSceneNode()->addChild(lightsSceneNodeCopy);
		} else {
            Log::error("Could not obtain scene node from input lights parameter.", "AdvancedRenderNode::processOutputImage");
			return;
		}
    }
#endif

    // process input camera
    Ogre::SceneNode *cameraSceneNode = getSceneNodeValue(InputCameraParameterName);
	if (cameraSceneNode) {
        // get the first camera attached to the input camera scene node
        Ogre::Camera *camera = OgreTools::getFirstCamera(cameraSceneNode);
        
		if (camera) {
			// duplicate the input camera
            m_cameraSceneNode = 0;
            OgreTools::deepCopySceneNode(cameraSceneNode, m_cameraSceneNode, m_name, m_sceneManager);
            if (!m_cameraSceneNode) {
                Log::error("The camera's scene node could not be copied.", "AdvancedRenderNode::processOutputImage");
                return;
            }

            if (useCameraLight) {
                // set up names for camera light objects
                QString cameraLightName = QString("%1CameraLight").arg(m_name);
                //QString cameraLightSceneNodeName = QString("%1SceneNode").arg(cameraLightName);
                // create camera light objects
                Ogre::Light *cameraLight = m_sceneManager->createLight(cameraLightName.toStdString());
                //Ogre::SceneNode *cameraLightSceneNode = m_cameraSceneNode->createChildSceneNode(cameraLightSceneNodeName.toStdString());
                m_cameraSceneNode->attachObject(cameraLight);
            }

			// process input light
			Ogre::SceneNode *lightSceneNode = getSceneNodeValue(InputLightsParameterName);
			if (lightSceneNode) {
				// get the lights attached to the input light scene node
				Ogre::Light *light = OgreTools::getFirstLight(lightSceneNode);
				if (light) {
					// duplicate the input camera
					m_lightSceneNode = 0;
					OgreTools::deepCopySceneNode(lightSceneNode, m_lightSceneNode, m_name, m_sceneManager);
					if (!m_lightSceneNode) {
						Log::error("The light's scene node could not be copied.", "AdvancedRenderNode::processOutputImage");
						return;
					}
					// set visibility of light gometry representation to false
					Ogre::Entity *entity = OgreTools::getFirstEntity(m_lightSceneNode);
					if (entity)
						entity->setVisible(false);
					// add the lights to the render scene
					m_sceneManager->getRootSceneNode()->addChild(m_lightSceneNode);
				}
			}

			// copy first camera
			m_camera = m_cameraCopy = OgreTools::getFirstCamera(m_cameraSceneNode);

            // retrieve render resolution values from custom parameter
            const Ogre::Any &customData = dynamic_cast<Ogre::MovableObject *>(camera)->getUserAny();
            if (!customData.isEmpty()) {
                CameraInfo *cameraInfo = Ogre::any_cast<CameraInfo *>(customData);
                if (cameraInfo) {
                   	resizeAllTargets(cameraInfo->width, cameraInfo->height);
					setupTextures();
                }
            }

			// (re-)create the render textures
			//initializeRenderTarget(m_cameraCopy);

			rebindViewports();

			// update render texture and render target
			setValue(m_outputImageName, m_renderTexture);

			// update add render textures and render targets
			QList<Ogre::TexturePtr> &textures = m_textureHash.values();
			for (size_t i=0; i<m_textureHash.size(); ++i)
				setValue(m_outParameterNameList[i], textures[i]);

			// Look for possibility to use the SaC-mapping and prepare this feature
			setupWrinkleMaterial();

			// undirty the output parameters to prevent multiple updates
			for (size_t i=0; i<m_outParameterNameList.size(); ++i)
				getParameter(m_outParameterNameList[i])->setDirty(false);
		}
    }
	else
		Log::error("First object attached to scene node contained in input camera parameter is not a camera.", "AdvancedRenderNode::processOutputImage");

	AdvancedRenderNode::redrawTriggered();
}


//!
//! Redraw of ogre scene has been triggered.
//!
void AdvancedRenderNode::redrawTriggered ()
{
	if (!m_renderTexture.isNull())
		if (isViewed() || m_outputParameter->isConnected()) {
			m_renderTexture->getBuffer()->getRenderTarget()->update();
			getParameter(m_outputImageName)->setAuxDirty(false);
		}

	if (!m_blendWeightTexture.isNull())
		m_blendWeightTexture->getBuffer()->getRenderTarget()->update();
	
	if (m_multiRenderTarget) {
		m_multiRenderTarget->update();
	
		for (size_t i=0; i<m_outParameterNameList.size(); ++i)
			getParameter(m_outParameterNameList[i])->setAuxDirty(false);
	}

	//Parameter* timeParameter = getTimeParameter();
	//if (timeParameter)
	//	std::cout << "Advanced Node:" << timeParameter->getValue().toInt() << std::endl; 
}


//!
//! Constructor of the MaterialSwitcher class.
//!
MaterialSwitcher::MaterialSwitcher () :
    m_mrtTechnique(0),
	m_wrinkleMappingTechnique(0)
{
	Ogre::MaterialPtr mrtMat = Ogre::MaterialPtr(Ogre::MaterialManager::getSingleton().getByName("AdvancedRender/MRT_HS"));
    if (!mrtMat.isNull()) {
	    mrtMat->load();
	    m_mrtTechnique = mrtMat->getBestTechnique();
    }

	// Material for SaC-mapping
	Ogre::MaterialPtr blendweightMat = Ogre::MaterialPtr(Ogre::MaterialManager::getSingleton().getByName("Calculate_Blendweights"));
	if(!blendweightMat.isNull()){
		blendweightMat->load();
		m_wrinkleMappingTechnique = blendweightMat->getBestTechnique();
	}
}

//!
//! Destructor of the MaterialSwitcher class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
MaterialSwitcher::~MaterialSwitcher ()
{
}


///
/// Public Functions
///
	
Ogre::Technique* MaterialSwitcher::handleSchemeNotFound(unsigned short schemeIndex, 
														const Ogre::String& schemeName,
														Ogre::Material* originalMaterial, 
									                    unsigned short lodIndex,
														const Ogre::Renderable* rend)
{
		if (schemeName == "MRTPass")
            return m_mrtTechnique;
		// for SaC-mapping
		if (schemeName == "blendWeightPass")	
			return m_wrinkleMappingTechnique;

		else return NULL;
}

//!
//! Check existing material for SaC-mapping shader and set the weight-map texture.
//! 
//! \return True when SaC-mapping material was found.
//!
bool AdvancedRenderNode::setupWrinkleMaterial ()
{
	Ogre::SceneNode *geometrySceneNode = getSceneNodeValue(InputGeometryParameterName);
	Ogre::Entity *myEntity = OgreTools::getFirstEntity(geometrySceneNode);

	Ogre::MeshPtr meshPtr; 

	if (myEntity){
		meshPtr = myEntity->getMesh();
	}
	
	if (!meshPtr.isNull()){

		Ogre::Mesh::SubMeshIterator subMeshIter = meshPtr->getSubMeshIterator();
		
		// find special Material for SaC-mapping and set weight-map as texture
		while (subMeshIter.hasMoreElements()) {
			Ogre::SubMesh* subMesh = subMeshIter.getNext();
			Ogre::String subMeshMaterialName = subMesh->getMaterialName();
			Log::debug("Found Material - " + QString(subMeshMaterialName.c_str()));
			
			std::string::size_type loc = subMeshMaterialName.find("Wrinkles"); // name of the skin material of the character
			if( loc != std::string::npos ) {
				Ogre::MaterialPtr subMeshMaterial = (Ogre::MaterialPtr)Ogre::MaterialManager::getSingleton().getByName(subMeshMaterialName);
				if (!subMeshMaterial.isNull()){
					Log::debug("Found Wrinkles-Material !!!");

					m_blendWeightTexture->getBuffer()->getRenderTarget()->getViewport(0)->setMaterialScheme("blendWeightPass");

					// set weightmap as texture
					subMeshMaterial->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureName(m_blendWeightTextureName);
					Log::debug("Setup weight map " + QString(m_blendWeightTextureName.c_str()) + "as texture for \"Wrinkles\" material");
#ifdef WRINKLE_DEBUG
					setValue(m_outputWrinkleImageName, m_blendWeightTexture);
#endif
				}
				return true;
			}
		}
	}
	return false;
}

//!
//! Setup additional textures (blendweight, multi render target).
//!
void AdvancedRenderNode::setupTextures ()
{
	Ogre::TextureManager &textureManager = Ogre::TextureManager::getSingleton();
	Ogre::RenderTarget *blendWeightTextureTarget = 0;

	// setup wrinkle weight map
	if (textureManager.resourceExists(m_blendWeightTextureName)) {
		blendWeightTextureTarget = m_blendWeightTexture->getBuffer()->getRenderTarget();
		blendWeightTextureTarget->removeAllViewports();
		blendWeightTextureTarget->removeAllListeners();
		textureManager.remove(m_blendWeightTextureName);
	}

	m_blendWeightTexture = Ogre::TextureManager::getSingleton().createManual(m_blendWeightTextureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D, 256, 256, 0, Ogre::PF_R8G8B8A8, Ogre::TU_RENDERTARGET);
	blendWeightTextureTarget = m_blendWeightTexture->getBuffer()->getRenderTarget();
	blendWeightTextureTarget->setAutoUpdated(false);

	// setup multiple render target
	if(m_multiRenderTarget) {
		m_multiRenderTarget->removeAllViewports();
		m_multiRenderTarget->removeAllListeners();
		Ogre::Root::getSingleton().getRenderSystem()->destroyRenderTarget(m_multiRenderTargetName);
	}

	m_multiRenderTarget = Ogre::Root::getSingleton().getRenderSystem()->createMultiRenderTarget(m_multiRenderTargetName);
	m_multiRenderTarget->setAutoUpdated(false);

	size_t count = 0;
	QHash<QString, Ogre::TexturePtr>::iterator texHashIter;
	for (texHashIter = m_textureHash.begin(); texHashIter != m_textureHash.end(); ++texHashIter) {
		Ogre::RenderTexture *target = texHashIter.value()->getBuffer()->getRenderTarget();
		m_multiRenderTarget->bindSurface(count++, target);
	}
}


//!
//! Create new viewports and bind them to the additional textures (blendweight, multi render target).
//!
void AdvancedRenderNode::rebindViewports()
{
	Ogre::RenderTexture *blendWeightTextureTarget = m_blendWeightTexture->getBuffer()->getRenderTarget();

	blendWeightTextureTarget->removeAllViewports();
	
	Ogre::Viewport *viewport = blendWeightTextureTarget->addViewport(m_cameraCopy);
	viewport->setClearEveryFrame(true);
	viewport->setOverlaysEnabled(false);
	viewport->setSkiesEnabled(false);
	viewport->setBackgroundColour(Ogre::ColourValue::Black);

	m_multiRenderTarget->removeAllViewports();
	Ogre::Viewport *multiViewport = m_multiRenderTarget->addViewport(m_cameraCopy);
    multiViewport->setMaterialScheme("MRTPass");
    multiViewport->setClearEveryFrame(true);
    multiViewport->setOverlaysEnabled(false);
	multiViewport->setSkiesEnabled(false);
	multiViewport->setBackgroundColour(Ogre::ColourValue::Black);
}