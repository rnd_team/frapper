project(hairrender)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# hairrender
set( hairrender_h ${hairrender_h}
			DualScatterMath.h
			HairRenderNode.h
			HairRenderNodePlugin.h
			)

set( hairrender_moc ${hairrender_moc}
			HairRenderNode.h
			HairRenderNodePlugin.h
			)

set( hairrender_src ${hairrender_src}
			DualScatterMath.cpp	
			HairRenderNode.cpp
			HairRenderNodePlugin.cpp
			)
			
set( hairrender_res ${hairrender_res}	
			hairrender.xml
			)

# Create moc files
qt4_wrap_cpp(hairrender_cxx ${hairrender_moc})

# Create source groups
source_group("Moc Files" FILES ${hairrender_cxx})
source_group("Header Files" FILES ${hairrender_h})
source_group("Resources" FILES ${hairrender_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(hairrender_src ${hairrender_src} ${hairrender_h} ${hairrender_res})
ENDIF(WIN32)

# Create static library
add_library(hairrender SHARED ${hairrender_src} ${hairrender_cxx})

# Add library dependencies
target_link_libraries(hairrender
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
#IF (WIN32)
#	add_custom_command(TARGET hairrender POST_BUILD COMMAND copy \"${hairrender_BINARY_DIR}/$(ConfigurationName)\\*.dll\" \"${hairrender_SOURCE_DIR}/../../../bin/win32/plugins\" COMMAND copy \"${hairrender_SOURCE_DIR}\\*.xml\" \"${hairrender_SOURCE_DIR}/../../../bin/win32/plugins\")
#ENDIF(WIN32)
install( FILES ${hairrender_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libhairrender_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libhairrender.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS hairrender RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
