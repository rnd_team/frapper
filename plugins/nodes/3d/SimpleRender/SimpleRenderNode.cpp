/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2010 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

//!
//! \file "SimpleRenderNode.cpp"
//! \brief Implementation file for SimpleRenderNode class.
//!
//! \author     Simon Spielmann <sspielma@filmakademie.de>
//! \version    0.1
//! \date       30.09.2010 (last updated)
//!

#include "SimpleRenderNode.h"
#include "GeometryRenderNode.h"
#include "Parameter.h"
#include "SceneNodeParameter.h"
#include "OgreTools.h"
#include "OgreContainer.h"
#include "OgreManager.h"



///
/// Constructors and Destructors
///

//!
//! Constructor of the SimpleRenderNode class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
SimpleRenderNode::SimpleRenderNode ( const QString &name, ParameterGroup *parameterRoot ) :
    GeometryRenderNode(name, parameterRoot)
{
    if (m_sceneManager)
        m_sceneManager->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

    // load default texture
    m_defaultTextureImage.load("DefaultRenderImage.png", "General");

	Parameter *outputImageParameter = getParameter(m_outputImageName);
    if (outputImageParameter) {
        outputImageParameter->setProcessingFunction(SLOT(processOutputImage()));
        outputImageParameter->setDirty(true);
		// set up parameter affections
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputGeometryParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputLightsParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::InputCameraParameterName));
        outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::CameraLightToggleParameterName));
		outputImageParameter->addAffectingParameter(getParameter(GeometryRenderNode::BackgroundColorParameterName));
    }

    getParameter(InputGeometryParameterName)->setSelfEvaluating(true);
}


//!
//! Destructor of the SimpleRenderNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
SimpleRenderNode::~SimpleRenderNode ()
{
	// empty
}


///
/// Public Slots
///


//!
//! Processes the node's input data to generate the node's output image.
//!
void SimpleRenderNode::processOutputImage ()
{
    Parameter *tempImageParameter = getParameter(m_outputImageName);
    if (!tempImageParameter) {
        Log::error("Could not obtain output image parameter.", "SimpleRenderNode::processOutputImage");
        return;
    }

    // check if a camera light should be used instead of the input lights
    bool useCameraLight = getBoolValue(CameraLightToggleParameterName);

    // get OGRE texture manager
    Ogre::TextureManager &textureManager = Ogre::TextureManager::getSingleton();

    // destroy all scene nodes and objects in the render scene
    OgreTools::deepDeleteSceneNode(m_sceneManager->getRootSceneNode(), m_sceneManager);

    if (m_cameraSceneNode) {
        OgreContainer *sceneNodeContainer = Ogre::any_cast<OgreContainer *>(m_cameraSceneNode->getUserAny());
        if (sceneNodeContainer)
            delete sceneNodeContainer;
        OgreTools::deepDeleteSceneNode(m_cameraSceneNode, m_sceneManager);
        m_cameraSceneNode->removeAndDestroyAllChildren();
        m_sceneManager->destroySceneNode(m_cameraSceneNode);
        m_cameraSceneNode = 0;
		m_cameraCopy = 0;
    }

    // destroy cameras, lights and entities
    m_sceneManager->destroyAllCameras();
    m_sceneManager->destroyAllEntities();
    m_sceneManager->destroyAllLights();

	setValue(m_outputImageName, m_defaultTexture);

    // process input geometry
    Ogre::SceneNode *geometrySceneNode = getSceneNodeValue(InputGeometryParameterName, true);
    if (geometrySceneNode) {
        // duplicate the input geometry
        Ogre::SceneNode *geometrySceneNodeCopy = 0;
         OgreTools::deepCopySceneNode(geometrySceneNode, geometrySceneNodeCopy, m_name, m_sceneManager);
        // add the geometry to the render scene
        m_sceneManager->getRootSceneNode()->addChild(geometrySceneNodeCopy);
	} else {
        Log::error("Could not obtain scene node from input geometry parameter.", "SimpleRenderNode::processOutputImage");
		return;
	}

    // process input lights
    if (!useCameraLight) {
#ifdef LIGHT_PARAMETER_AVAILABLE
        // process input lights
        Ogre::SceneNode *lightsSceneNode = getSceneNodeValue(InputLightsParameterName);
        if (lightsSceneNode) {
             duplicate the input lights
            Ogre::SceneNode *lightsSceneNodeCopy = 0;
            OgreTools::deepCopySceneNode(lightsSceneNode, lightsSceneNodeCopy, m_name, m_sceneManager);
             add the lights to the render scene
            m_sceneManager->getRootSceneNode()->addChild(lightsSceneNodeCopy);
        } else
            Log::error("Could not obtain scene node from input lights parameter.", "SimpleRenderNode::processOutputImage");
#endif
    }

    // process input camera
    Ogre::SceneNode *cameraSceneNode = getSceneNodeValue(InputCameraParameterName);
	
	if (cameraSceneNode) {
        // get the first camera attached to the input camera scene node
        Ogre::Camera *camera = OgreTools::getFirstCamera(cameraSceneNode);
        
		if (camera) {
			// duplicate the input camera
            m_cameraSceneNode = 0;
            OgreTools::deepCopySceneNode(cameraSceneNode, m_cameraSceneNode, m_name, m_sceneManager);
            if (!m_cameraSceneNode) {
                Log::error("The camera's scene node could not be copied.", "SimpleRenderNode::processOutputImage");
                return;
            }

			// process input light
			Ogre::SceneNode *lightSceneNode = getSceneNodeValue(InputLightsParameterName);
			if (lightSceneNode) 
            {
                m_lightSceneNode = 0;
                OgreTools::deepCopySceneNode( lightSceneNode, m_lightSceneNode, m_name+"LightSceneNode", m_sceneManager);

                m_lightSceneNode->setInheritScale(false);
                m_lightSceneNode->setInheritOrientation(false);

                // get all geometry entities attached to the input light scene node
                QList<Ogre::Entity*> entities;
                OgreTools::getAllEntities(lightSceneNode, entities);

                foreach( Ogre::Entity* entity, entities )
                {
                    if( entity )
                        entity->setVisible(false);
                }
                // add the lights to the render scene
                m_sceneManager->getRootSceneNode()->addChild(m_lightSceneNode);
			}

            // optionally create a camera light
            if (useCameraLight) {
                // set up names for camera light objects
                QString cameraLightName = QString("%1CameraLight").arg(m_name);
                //QString cameraLightSceneNodeName = QString("%1SceneNode").arg(cameraLightName);
                // create camera light objects
                Ogre::Light *cameraLight = m_sceneManager->createLight(cameraLightName.toStdString());
                //Ogre::SceneNode *cameraLightSceneNode = m_cameraSceneNode->createChildSceneNode(cameraLightSceneNodeName.toStdString());
                m_cameraSceneNode->attachObject(cameraLight);

            }

            // retrieve render resolution values from custom parameter
            const Ogre::Any &customData = dynamic_cast<Ogre::MovableObject *>(camera)->getUserAny();
            if (!customData.isEmpty()) {
                CameraInfo *cameraInfo = Ogre::any_cast<CameraInfo *>(customData);
                if (cameraInfo) {
                    m_renderWidth = cameraInfo->width;
                    m_renderHeight = cameraInfo->height;
                }
            }

			// copy first camera
			m_cameraCopy = OgreTools::getFirstCamera(m_cameraSceneNode);
            OgreTools::getOgreContainer(m_cameraSceneNode)->setCamera(m_cameraCopy);


            QColor bgColor = this->getColorValue(BackgroundColorParameterName);
            
			// (re-)create the render texture
			if (bgColor.alphaF() < 1.0f)
				initializeRenderTarget(m_cameraCopy, Ogre::ColourValue(bgColor.redF(), bgColor.greenF(), bgColor.blueF(), bgColor.alphaF()), Ogre::PF_R8G8B8A8);
			else
				initializeRenderTarget(m_cameraCopy, Ogre::ColourValue(bgColor.redF(), bgColor.greenF(), bgColor.blueF()));
	
			// update render texture and render target
			setValue(m_outputImageName, m_renderTexture);

			// undirty the output parameter to prevent multiple updates
			getParameter(m_outputImageName)->setDirty(false);
		}
    }
	else
		Log::error("First object attached to scene node contained in input camera parameter is not a camera.", "SimpleRenderNode::processOutputImage");

	RenderNode::redrawTriggered();
}
