project(simplerender)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# simplerender
set( simplerender_h ${simplerender_h}
			SimpleRenderNode.h
			SimpleRenderNodePlugin.h
			)

set( simplerender_moc ${simplerender_moc}
			SimpleRenderNode.h
			SimpleRenderNodePlugin.h
			)

set( simplerender_src ${simplerender_src}	
			SimpleRenderNode.cpp
			SimpleRenderNodePlugin.cpp
			)
			
set( simplerender_res ${simplerender_res}	
			simplerender.xml
			)

# Create moc files
qt4_wrap_cpp(simplerender_cxx ${simplerender_moc})

# Create source groups
source_group("Moc Files" FILES ${simplerender_cxx})
source_group("Header Files" FILES ${simplerender_h})
source_group("Resources" FILES ${simplerender_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(simplerender_src ${simplerender_src} ${simplerender_h} ${simplerender_res})
ENDIF(WIN32)

# Create static library
add_library(simplerender SHARED ${simplerender_src} ${simplerender_cxx})

# Add library dependencies
target_link_libraries(simplerender
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
#IF (WIN32)
#	add_custom_command(TARGET simplerender POST_BUILD COMMAND copy \"${simplerender_BINARY_DIR}/$(ConfigurationName)\\*.dll\" \"${simplerender_SOURCE_DIR}/../../../bin/win32/plugins\" COMMAND copy \"${simplerender_SOURCE_DIR}\\*.xml\" \"${simplerender_SOURCE_DIR}/../../../bin/win32/plugins\")
#ENDIF(WIN32)
install( FILES ${simplerender_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
install( TARGETS simplerender RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
