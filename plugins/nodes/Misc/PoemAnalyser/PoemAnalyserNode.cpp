/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "PoemAnalyserNode.cpp"
//! \brief Implementation file for PoemAnalyserNode class.
//!
//! \author     Diana Arellano <diana.arellano@filmakademie.de>
//! \version    1.0
//! \date       24.02.2012 (last updated)
//!

#include "PoemAnalyserNode.h"

#define PLEASANT	0
#define NICE		1
#define PASSIVE		2
#define SAD			3
#define UNPLEASANT	4
#define NASTY		5
#define ACTIVE		6
#define FUN			7
#define HIMAGERY	8
#define LOIMAGERY	9

#define NRSTATES	8	//number of emotional states from the Whissell analysis -> 8 because we will not include "Low Imagery" and "high Imagery"
#define POEMSTATES	3	//the number of states to consider for the poem

///
/// Constructors and Destructors
///


//!
//! Constructor of the PoemAnalyserNode class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
PoemAnalyserNode::PoemAnalyserNode ( const QString &name, ParameterGroup *parameterRoot ) :
    Node(name, parameterRoot)
{
	wordAnalysisGroup = new ParameterGroup("Word Analysis");
	poemAnalysisGroup = new ParameterGroup("Poem Analysis");

	FilenameParameter *filenameDAT = getFilenameParameter("Filename DAT"); 
	if (filenameDAT){
        filenameDAT->setChangeFunction(SLOT(readDATFile()));
		filenameDAT->setCommandFunction(SLOT(reloadDATFile()));
	}
    else
        Log::error("Could not obtain all parameters required for parsing the Poems file.", "PoemAnalyserNode::PoemAnalyserNode");

	FilenameParameter *filenameSUM = getFilenameParameter("Filename SUM"); 
	if (filenameSUM){
        filenameSUM->setChangeFunction(SLOT(readSUMFile()));
		filenameSUM->setCommandFunction(SLOT(reloadSUMFile()));
	}
    else
        Log::error("Could not obtain all parameters required for parsing the Poems file.", "PoemAnalyserNode::PoemAnalyserNode");

	buttonParameter = new NumberParameter("Analysis Selected", Parameter::T_Command, "Select to analyse the poem");	
	parameterRoot->addParameter(buttonParameter);
	buttonParameter->setCommandFunction(SLOT(performPoemAnalysis()));

	poemAnalysisTable = new QTableWidget(NRSTATES, 2);	
}


//!
//! Destructor of the PoemAnalyserNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
PoemAnalyserNode::~PoemAnalyserNode ()
{
}

//!
//! Reads the content of the DAT File, which contains the Whissell Dictionary of Affect (WDAL) analysis word by word 
//!
void PoemAnalyserNode::readDATFile(){

	QString filename = getStringValue("Filename DAT");
	if (filename == "") 
		return;

	QFile *file = new QFile(filename);

	if(!file->open(QFile::ReadOnly)){
		Log::error(QString("File %1 could not be loaded!").arg(filename), "PoemReaderNode::readDATFile");
		return;
	}

	QTextStream ts(file);
	QString line = ts.readLine();

	while(!ts.atEnd()){

		line = ts.readLine();
		int firstspace = line.indexOf("\t");
		QString nameParameter = line.left(firstspace).trimmed();
		nameParameter = nameParameter.toLower();
		QString valueParameter = line.right(line.length() - firstspace);
		
		if (!wordAnalysisGroup->contains(nameParameter+"Parameter")){
			
			Parameter *p = new Parameter(nameParameter+"Parameter", Parameter::T_String, QVariant(valueParameter));
			wordAnalysisGroup->addParameter(p);
			Log::warning("wordAnalysisGroup: " + p->getName());
		}
	}
}


//!
//! Reads the content of the SUM File, which contains the WDAL analysis of the whole poem
//!
void PoemAnalyserNode::readSUMFile(){

	QString filename = getStringValue("Filename SUM");
	if (filename == "") 
		return;

	QFile *file = new QFile(filename);

	if(!file->open(QFile::ReadOnly)){
		Log::error(QString("File %1 could not be loaded!").arg(filename), "PoemReaderNode::readSUMFile");
		return;
	}

	QTextStream ts(file);
	QString line;
	int i = 0;

	while(!ts.atEnd()){

		line = ts.readLine();
		int firstspace = line.indexOf("\t");
		QString nameParameter = line.left(firstspace).trimmed();

		// Add this "if" because we do not want to consider "Low Imagery" or "High Imagery"
		if (!nameParameter.contains("imagery", Qt::CaseInsensitive))
		{

			QString valueParameter = line.right(line.length() - firstspace).trimmed();
		
			if (!poemAnalysisGroup->contains(nameParameter+"Parameter"))
			{
				Parameter *p = new Parameter(nameParameter+"Parameter", Parameter::T_Float, QVariant(valueParameter.toFloat()));
				poemAnalysisGroup->addParameter(p);
		
				// We fill the table at the same time ... perhaps we don't need parameters in this case
				QTableWidgetItem *stateItem = new QTableWidgetItem;
				QTableWidgetItem *valueItem = new QTableWidgetItem;

				stateItem->setText(nameParameter);
				valueItem->setData(Qt::DisplayRole, QVariant(valueParameter.toFloat()));

				poemAnalysisTable->setItem(i, 0, stateItem);
				poemAnalysisTable->setItem(i, 1, valueItem);
				i++;
			}
		}
	}
	poemAnalysisTable->sortByColumn(1, Qt::DescendingOrder);
}


//!
//! Reloads the content of the SUM File, which contains the WDAL analysis of the whole poem
//!
void PoemAnalyserNode::reloadSUMFile(){
	
	poemAnalysisGroup->clear();
	overallState = "";

	readSUMFile();
}


//!
//! Reloads the content of the DAT File, which contains the WDAL analysis of the whole poem
//!
void PoemAnalyserNode::reloadDATFile(){
	
	wordAnalysisGroup->clear();

	readDATFile();
}

//!
//! Performs the analysis of the poem based on the Whissell's analysis
//!
void PoemAnalyserNode::performPoemAnalysis(){
	
	for (int i=0; i< poemAnalysisTable->rowCount(); i++) {
		Log::warning(poemAnalysisTable->item(i, 0)->text());
		}

	Log::warning("this is to perform analysis");
	if (!poemAnalysisGroup->isEmpty() && !wordAnalysisGroup->isEmpty()){

		QString poemNoTags = getParameter("String Input")->getValueString();
		QString poemTags;
		QString tag;
		int read = 0;
		QRegExp rx1("[\"!#$%&()*+,./:;?@_{|}~-]");	

		// Each line is read (termination sign: [n])
		QStringList lines = poemNoTags.split("[n]");

		//We tag the with the overallstate
		QString allstate = getOverallState();
		bool first = true;

		foreach(QString currentLine, lines) {

			if (!currentLine.isEmpty()) {

				QStringList *states = new QStringList();
				for (int i=0; i<NRSTATES; i++) {
					states->append("0");
				}

				QStringList words = currentLine.split(" ");

				// We obtain the states of each word and store them as counters in a list
				for (int i=0; i<words.length(); i++){
					Log::warning("words.length: " +  QString::number(words.length()));

					QString currentword = words.at(i);
					currentword = currentword.trimmed();
					currentword = currentword.toLower();	//we work with lower letters only
					currentword.remove(rx1);				//Remove punctuation marks from the strings

					Parameter *currentwordParam = wordAnalysisGroup->getParameter(currentword+"Parameter");
					

					if (currentwordParam) {
						Log::warning("currentParam: " + currentwordParam->getName());
						QStringList currentwordValues = currentwordParam->getValueString().split("\t", QString::SkipEmptyParts);
						for (int j=0; j<states->length(); j++){
							states->replace(j,(QString::number(states->at(j).toInt() + currentwordValues.at(j).toInt())));
						}
						for (int j=0; j<states->length(); j++){
							Log::warning("states[" + QString::number(j) + "] = " + QString::number(states->at(j).toInt()));
						}
					}
				}

				// From the list we obtain the predominant state of the line
				int predominantValue = 0;
				int position = 0;
				QStringList *positions = new QStringList();
				foreach(QString state, *states){
					if (predominantValue < state.toInt()){
						predominantValue = state.toInt();
						positions->clear();	//clean the list with old lower values
						positions->append(QString::number(position));
					}
					else if ( predominantValue == state.toInt() && (state.toInt() != 0) ){	//If all states are 0, the vector "positions" will be empty
						positions->append(QString::number(position));
					}
					position++;
				}
			
				if (positions->size() != 0)
				{
					// We see if this state corresponds to one of the predominant states of the POEM
					QStringList *predominantStates = obtainPredominantStates(positions);

					foreach(QString predState, *predominantStates)
					{
						Log::warning("predState - Line: " + predState);
					}

					QString stateName; 
					tag = "";
				
					for (int i=0; i<POEMSTATES; i++){
						stateName = poemAnalysisTable->item(i,0)->text();
					
						bool exists = (predominantStates->contains(stateName, Qt::CaseInsensitive) && !stateName.contains("imagery", Qt::CaseInsensitive) && !stateName.contains("passive", Qt::CaseInsensitive) && !stateName.contains("active", Qt::CaseInsensitive) );
						if (exists){
							tag = taggingLine(stateName);		//places the tag with the corresponding state at the beginning of the line
							break;
						}
					}

				
					if (tag == "" && overallState!=""){	// we place another label according to the general state of the poem
						for (int i=0; i<predominantStates->length(); i++) {
							stateName = predominantStates->at(i);
							if (!stateName.contains("imagery", Qt::CaseInsensitive) && !stateName.contains("passive", Qt::CaseInsensitive) && !stateName.contains("active", Qt::CaseInsensitive) )
							{
								if (overallState.compare(getLineState(stateName)) == 0){
									tag = taggingLine(stateName);		//places the tag with the corresponding state at the beginning of the line
									break;
								}

							}
						}
						if (tag=="")
							if (first && (overallState.compare(getLineState(stateName)) == 0))
							{
								tag = taggingLine(allstate);
								first = false;
							}
							else
							{
								Log::warning("stateName: " + stateName);
								tag = taggingLine("SHY");
							}
					}
				}
				else
				{
					Log::warning("positions vector was filled with 0s ");
					tag = taggingLine("SHY");
				}
				//We write the new string with the tagged poem
				poemTags.append(tag + currentLine + " [n] \n");
				first = false;
			}
		}
		//pones la etiqueta del query_loop
		tag = taggingLine("LOOP");
		poemTags.append(tag);

		//output in the panel
		getParameter("String Output")->setValue(QVariant(poemTags));
	}
}


QStringList * PoemAnalyserNode::obtainPredominantStates(QStringList *positions){

	QStringList *predominantStates = new QStringList();

	foreach(QString pos, *positions){

		predominantStates->append(transformNumberToString(pos.toInt()));
	}

	return predominantStates;
}


QString PoemAnalyserNode::transformNumberToString(int number){

	QString state;

	switch(number){
		case 0:
			state = "pleasant";
			break;
		case 1:
			state = "nice";
			break;
		case 2:
			state = "passive";
			break;
		case 3:
			state = "sad";
			break;
		case 4:
			state = "unpleasant";
			break;
		case 5:
			state = "nasty";
			break;
		case 6:
			state = "active";
			break;
		case 7:
			state = "fun";
			break;
		case 8:
			state = "himagery";
			break;
		case 9:
			state = "loimagery";
			break;
		default:
			state = "neutral";
			break;
	}
	return state;
}


QString PoemAnalyserNode::taggingLine(QString state){
	
	QString tag;

	if (state.contains("LOOP")) {
		tag = "[synthesis:emotion id='QUERY_LOOP' /] ";
	}
	else if (state.contains("SHY")) {
		tag = "[synthesis:emotion id='SHY-NEUTRAL' /] ";
	}
	else{
		tag = "[synthesis:emotion id='" + state + "' /] ";
	}
	return tag;
}


//!
//! Assigns a value to the global variable "overallState" to see if the poem is emotionally positive or negative
//! and Returns the value of the state of the poem
//!
QString PoemAnalyserNode::getOverallState()
{
	QString stateName;
	for (int i=0; i<POEMSTATES; i++){
		stateName = poemAnalysisTable->item(i,0)->text();
		if (!stateName.contains("imagery", Qt::CaseInsensitive) && !stateName.contains("passive", Qt::CaseInsensitive) && !stateName.contains("active", Qt::CaseInsensitive) ) 
		{
			if (stateName.compare("pleasant", Qt::CaseInsensitive)==0 || 	stateName.compare("nice", Qt::CaseInsensitive)==0 || stateName.compare("fun", Qt::CaseInsensitive)==0)
				overallState = "positive";
			else if (stateName.contains("sad", Qt::CaseInsensitive)	|| stateName.compare("unpleasant", Qt::CaseInsensitive)==0 || stateName.compare("nasty", Qt::CaseInsensitive)==0)		
				overallState = "negative";
			break;
		}	
	}
	return stateName;
}

QString PoemAnalyserNode::getLineState(QString tag)
{
	QString lineState;
	if (tag.compare("pleasant", Qt::CaseInsensitive)==0 || 	tag.compare("nice", Qt::CaseInsensitive)==0 || tag.compare("fun", Qt::CaseInsensitive)==0)
		lineState = "positive";
	else if (tag.contains("sad", Qt::CaseInsensitive)	|| tag.compare("unpleasant", Qt::CaseInsensitive)==0 || tag.compare("nasty", Qt::CaseInsensitive)==0)		
		lineState = "negative";

	return lineState;
}