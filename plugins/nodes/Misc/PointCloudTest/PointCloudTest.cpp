/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "PointCloudTest.cpp"
//! \brief AR
//!
//! \author     Martin Schwarz <martin.schwarz@filmakademie.de>
//! \version    0.1
//! \date       12.1.2011 (last updated)
//!

#include "PointCloudTest.h"
#include "Log.h"


using namespace std;

INIT_INSTANCE_COUNTER(PointCloudTest)

///
/// Public Constructors
///

//!
//! Constructor of the PointCloudTest class.
//!
//! \param name The name to give the new mesh node.
//!
PointCloudTest::PointCloudTest ( QString name, ParameterGroup *parameterRoot ) :
Node(name, parameterRoot)
{
	INC_INSTANCE_COUNTER

	// get output parameter group
	m_vertexBufferGroup = new ParameterGroup("VertexBufferGroup");
	m_pointPositionParameter = new NumberParameter("PointPosition", Parameter::T_Float, 0.0f);
	m_vertexBufferGroup->addParameter(m_pointPositionParameter);
	m_pointColorParameter = new NumberParameter("PointColor", Parameter::T_Float, 0.0f);
	m_vertexBufferGroup->addParameter(m_pointColorParameter);
	m_pointColorParameter->setVisible(false);
	m_pointPositionParameter->setVisible(false);
	m_ouputVertexBuffer = Parameter::createGroupParameter("VertexBuffer", m_vertexBufferGroup);
	m_ouputVertexBuffer->setPinType(Parameter::PT_Output);
	parameterRoot->addParameter(m_ouputVertexBuffer);

	setOutputPoints();
}

//!
//! Destructor of the ARNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
PointCloudTest::~PointCloudTest ()
{
   DEC_INSTANCE_COUNTER
}

///
/// Public Slots
///

///
/// Private Slots
///




///
/// Public Methods
///

///
/// Private Methods
///



void PointCloudTest::setOutputPoints()
{
	QVariantList pointPositionList;
	QVariantList pointColorList;
	int size = 0;
	m_pointPositionParameter->setSize(0);
	m_pointColorParameter->setSize(0);
	for(size_t i=0; i<1000; i++)
	{
		m_pointPositionParameter->setSize(size = size + 3);
		m_pointColorParameter->setSize(size);
		pointPositionList.append(QVariant(((float)rand())/RAND_MAX*10.0f));
		pointPositionList.append(QVariant(((float)rand())/RAND_MAX*10.0f));
		pointPositionList.append(QVariant(((float)rand())/RAND_MAX*10.0f));
		pointColorList.append(QVariant(((float)rand())/(float)RAND_MAX));	//R
		pointColorList.append(QVariant(((float)rand())/(float)RAND_MAX));	//G
		pointColorList.append(QVariant(((float)rand())/(float)RAND_MAX));	//B
	}
	m_pointPositionParameter->setValue(QVariant(pointPositionList));
	m_pointColorParameter->setValue(QVariant(pointColorList));
	m_ouputVertexBuffer->propagateDirty();
}
