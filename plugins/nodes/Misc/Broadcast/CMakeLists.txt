project(broadcast)
#set(CMAKE_CONFIGURATION_TYPES Debug Release CACHE STRING "" FORCE) 
SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# broadcast
set( broadcast_h ${broadcast_h}		
			BroadcastNode.h
			BroadcastNodePlugin.h
			)

set( broadcast_moc ${broadcast_moc}	
			BroadcastNode.h
			BroadcastNodePlugin.h
			)

set( broadcast_src ${broadcast_src}	
			BroadcastNode.cpp
			BroadcastNodePlugin.cpp
			)

set( broadcast_res ${broadcast_res}	
			broadcast.xml
			)

# Create moc files		   
qt4_wrap_cpp(broadcast_cxx ${broadcast_moc})

# Create source groups
source_group("Moc Files" FILES ${broadcast_cxx})
source_group("Header Files" FILES ${broadcast_h})
source_group("Resources" FILES ${broadcast_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(broadcast_src ${broadcast_src} ${broadcast_h} ${broadcast_res})
ENDIF(WIN32)

# Create static library
add_library(broadcast SHARED ${broadcast_src} ${broadcast_cxx})

# Add library dependencies
target_link_libraries(broadcast
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized QtNetwork4 debug QtNetworkd4
			optimized frappercore debug frappercore_d
			)

# Install files
install( FILES ${broadcast_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libbroadcast_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libbroadcast.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS broadcast RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
