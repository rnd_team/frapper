/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "CameraTrackerNode.h"
//! \brief Header file for CameraTrackerNode class.
//!
//! \author     Martin Schwarz <martin.schwarz@filmakademie.de>
//! \version    0.0
//! \date       12.10.2011 (last updated)
//!
//!
#ifndef CAMERATRACKERNODE_H
#define CAMERATRACKERNODE_H


#include "opencvRawInput.h"
#include "InstanceCounterMacros.h"
#include "OgreManager.h"
#include "PTAMM_Wrapper.h"
#include <QDir>

#if (OGRE_PLATFORM  == OGRE_PLATFORM_WIN32)
	#include <windows.h>
#endif

using namespace Frapper;
using namespace PTAMM_Wrap;

//!
//! Class representing CameraTrackerNode
//!
class CameraTrackerNode : public opencvRawInput
{
    Q_OBJECT
	ADD_INSTANCE_COUNTER

public: /// constructors and destructors

    //!
    //! Constructor of the CameraTrackerNode class.
    //!
    //! \param name The name to give the new mesh node.
	//! \param parameterRoot The main Parameter Group of this Node
    //!
    CameraTrackerNode ( QString name, ParameterGroup *parameterRoot );

    //!
    //! Destructor of the CameraTrackerNode class.
    //!
    //! Defined virtual to guarantee that the destructor of a derived class
    //! will be called if the instance of the derived class is saved in a
    //! variable of its parent class type.
    //!
	virtual inline ~CameraTrackerNode ();


public: //methods



private: //methods
	
	//!
	//! This Method is used to do stuff if connectMatrixSlot() has succeed
	//!
	void pastConnectToMatrix();

	//!
	//! Writes Informations from PTAMM_Wrapper into the Logfile
	//!
	void writeLog();

	//!
	//! Write Informations from PTAMM_Wrapper:tracking() into the Logfile
	//!
	void writeTrackerInformation();

	//!
	//! set the outputParameters Position and Orientation
	//!
	void setPositionAndOrientation();

	//!
	//! sets the outputParameters pointPosition and pointColor
	//!
	void setOutputPoints();

private slots:

	//!
	//! Set a keyFrame
	//!
	void setFrame();

	//!
	//! Reset Tracking
	//!
	void resetTracking();

	//!
	//! Sets a new Map
	//!
	void setNewMap();

	//!
	//! Loads a Map
	//!
	void loadMap();

	//!
	//! Saves current Map
	//!
	void saveMap();

	//!
	//! Switch to the next Map
	//!
	void switchToNextMap();

	//!
	//! Switch to previous Map
	//!
	void switchToPreviousMap();

	//!
	//! Delete current Map
	//!
	void deleteMap();

	//!
	//! SLOT that is used on the matrixParameter
	//!
	void processInputMatrix();

	//!
	//! Sets the TransformationsMatrix
	//!	
	void setTransformationMatrix();

private: //data
	//!
	//! The PTAMM Object
	//!
	PTAMM_Wrapper* m_ptamm;

	//!
	//! Gives the orentation of the camera
	//!
	SE3<> m_CameraPose;

	//!
	//! The actual Pose of the Camera in PTAMM
	//!
	TooN::SE3<> m_toonCamPose;

	//!
	//! The Position of the Camera in PTAMM
	//!
	TooN::Matrix<3,3> m_toonRotateMatrix;

	//!
	//! The Position of the Camera in PTAMM
	//!
	TooN::Vector<3, double> m_toonPosition;

	//!
	//! The posit�on of the camera
	//!
	Ogre::Vector3 m_position;

	//!
	//! The scale of the translation of the cam
	//!
	Ogre::Vector3 m_scale;

	//!
	//! The Zero-Offset of the Camera
	//!
	Ogre::Vector3 m_zeroPosition;

	//!
	//! The quaterionen of the camera
	//!
	Ogre::Quaternion m_quat;

	//!
	//! The matrix with the values from PTAMM
	//!
	Ogre::Matrix4 m_ptammMatrix;

	//!
	//! The Transformation Matrix
	//!
	Ogre::Matrix4 m_transMatrix;

	//!
	//! The Parametergroup for the outgoing pointParameters
	//!
	ParameterGroup *m_outputPointParameter;

	//!
	//! This Parameter contains the GroupParameter m_ouputPointParameter
	//!
	Parameter* m_vertexBuffer; 

	//!
	//! This Parameter contains a List of floats with the position of some points
	//!
	NumberParameter *m_pointPositionParameter;

	//!
	//! This Parameter contains a List of floats with the color of some points
	//!
	NumberParameter *m_pointColorParameter;
	
	//!
	//! This integer tells you how many Points have been drawn last time
	//!
	int m_sizeofLastDrawnMap;

	//!
	//! 
	//!
	NumberParameter *m_outputMapIdParameter;

	//!
	//! The actual mapID
	//!
	int m_mapID;
};
Q_DECLARE_METATYPE(Ogre::Quaternion)

#endif
