//!
//! \file "PTAMM_Wrapper.h"
//! \brief Header file for PTAMM wrapper class, implementing PTAMM for CameraTracker.
//!
//! \author     Martin Schwarz <martin.schwarz@filmakademie.de>
//! \version    0.0
//! \date       25.04.2012 (last updated)
//!

#ifndef PTAMM_WRAPPER_H
#define PTAMM_WRAPPER_H
#include "VideoSource.h"
#include "Map.h"
#include "ATANCamera.h"
#include "Tracker.h"
#include "MapMaker.h"
#include "MapSerializer.h"
#include <cxcore.h>


namespace PTAMM_Wrap{

	using namespace PTAMM;
	using namespace std;

//!
//! Class implementing PTAMM for ARNode
//!
class __declspec(dllimport) PTAMM_Wrapper{


public: // constructors and destructors
	//!
    //! Constructor of the PTAMM_Wrapper class.
    //!
	PTAMM_Wrapper(int x, int y);

	//!
    //! Destructor of the PTAMM_Wrapper class.
    //!
	~PTAMM_Wrapper();	

public: //methods

	//!
	//! Tracks the Cameraimage
	//! \param Pointer to the Picture
	//!
	void tracking(cv::Mat *mptr);

	//!
	//! get UserMessage
	//!
	const string& getUserMessage();

	//!
	//! get TrackerMessage
	//!
	const string& getTrackerMessage();

	//!
	//! get CameraParameters
	//!
	void getCameraParameters(std::vector<float>& ret);

	//!
	//! sets a Keyframe 
	//!
	void setKeyframe();

	//!
	//! Create a new map and switch all
	//! threads and objects to it.
	//!
	void NewMap();

	//!
	//! Loads a Map 
	//! \param The name of the folder of the map to load
	//!
	void loadMap(std::string filename);

	//!
	//! Save the current map 
	//! \param The name of the folder of the map to save
	//!
	void saveMap(std::string filename );

	//!
	//! Save all maps that are loaded 
	//! \param The name of the folder of the map to save
	//!
	void saveMaps(std::string filename );

	//!
	//! gets the actual CameraPosition
	//! \return the actual CameraPosition
	//!
	SE3<> getCurrentCamPose();

	//!
	//! Moves all objects and threads to the first map, and resets it.
	//! Then deletes the rest of the maps, placing PTAMM in its
	//! original state.
	//! This reset ignores the edit lock status on all maps
	//!
	void ResetAll();

	//!
	//! Delete a specified map.
	//! \param nMapNum map to delete
	//!
	bool DeleteMap( int nMapNum );

	//!
	//! Switch to the map with ID nMapNum
	//! \param  nMapNum Map ID
	//! \param bForce This is only used by DeleteMap and ResetAll, and is
	//! to ensure that MapViewer is looking at a safe map.
	//!
	bool SwitchMap( int nMapNum, bool bForce=false );



private: //methods

	//!
	//! set UserMessage
	//!
	void setUserMessage(std::string);

	//!
	//! Set up the map serialization thread for saving/loading and the start the thread
	//! \param sCommand the function that was called (eg. SaveMap)
	//! \param sParams the params string, which may contain a filename and/or a map number
	//!
	void StartMapSerialization(std::string sCommand, std::string sParams);

	//!
	//! Parse commands sent via the GVars command system.
	//! \param ptr Object callback
	//! \param sCommand command string
	//! \param sParams parameters
	//!
	static void GUICommandCallBack(void *ptr, string sCommand, string sParams);

	//!
	//! Parse and allocate a single integer variable from a string parameter
	//! \param nAnswer the result
	//! \param sCommand the command (used to display usage info)
	//! \param sParams  the parameters to parse
	//! \return success or failure.
	//!
	bool GetSingleParam(int &nAnswer, std::string sCommand, std::string sParams);        

public: //data
	//!
	//! The current map
	//!
	Map *mpMap;

private: //data
	//!
    //! The Black and white image for tracking/mapping
	//!
	CVD::Image<CVD::byte> mimFrameBW;  

	//!
	//! The Tracker
	//!
	Tracker *mpTracker;

	//!
	//! The video image source
	//!
	VideoSource *mVideoSource; 

	//!
	//! The map maker
	//!
	MapMaker *mpMapMaker;

	//!
	//! The map serializer for saving and loading maps
	//!
	MapSerializer *mpMapSerializer;                 
	
	//!
	//! The set of maps
	//!
	std::vector<Map*> mvpMaps;  
		
	//!
	//! The camera model
	//!
	ATANCamera *mpCamera;

	//!
	//! Stop a map being edited - i.e. keyframes added, points updated
	//!
	GVars3::gvar3<int> mgvnLockMap;

	//!
	//! Is used for Messages for Tracker
	//!
	string mTrackerMessage;

	//!
	//! Is used for LogMessages
	//!
	string mUserMessage;

};
}
#endif