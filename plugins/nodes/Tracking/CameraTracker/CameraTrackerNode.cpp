/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "CameraTrackerNode.cpp"
//! \brief AR
//!
//! \author     Martin Schwarz <martin.schwarz@filmakademie.de>
//! \version    0.1
//! \date       10.04.2012 (last updated)
//!

#include "CameraTrackerNode.h"
#include "Log.h"
#include <string>
#include <ios>
	//#include <ctime>
	//clock_t start, finish;
	//start = clock();
	// operation
	//finish = clock();
	//cout << (long) (finish - start) << " clock ticks" << endl; //Information wenn nicht memcpy verwendet wird dauert das Setzen der pixel im Farbmodus 4 clock ticks

using namespace std;

INIT_INSTANCE_COUNTER(CameraTrackerNode)

///
/// Public Constructors
///

//!
//! Constructor of the CameraTrackerNode class.
//!
//! \param name The name to give the new mesh node.
//!
CameraTrackerNode::CameraTrackerNode ( QString name, ParameterGroup *parameterRoot ) :
opencvRawInput(name, parameterRoot),
m_ptamm(0)
{
	INC_INSTANCE_COUNTER
	m_sizeofLastDrawnMap = 0;
    m_mapID = 0;
	m_scale = Ogre::Vector3(1.0,1.0,1.0);
	setCommandFunction("SetKeyframe", SLOT(setFrame()));
	setCommandFunction("Reset Tracking", SLOT(resetTracking()));
	setCommandFunction("SetNewMap", SLOT(setNewMap()));
	
	setChangeFunction("Load Map", SLOT(loadMap()));
	setCommandFunction("Save Map", SLOT(saveMap()));
	setCommandFunction("Switch to next Map", SLOT(switchToNextMap()));
	setCommandFunction("Switch to previous Map", SLOT(switchToPreviousMap()));
	setCommandFunction("Delete Map", SLOT(deleteMap()));

	
	Parameter *outCameraTranslationParameter = getParameter("Position");
	if(outCameraTranslationParameter){
		outCameraTranslationParameter->setDirty(true);
	}
	Parameter *outCameraRotationParameter = getParameter("Orientation");
	if(outCameraRotationParameter){
		outCameraRotationParameter->setDirty(true);
	}

	m_outputMapIdParameter = getNumberParameter("MapID");
	if(m_outputMapIdParameter){
		m_outputMapIdParameter->setValue(QVariant::fromValue(m_mapID), true);
	}
	Parameter *outCamParameter = getNumberParameter("CameraParameters");

	Parameter *translateParameter = getParameter("Translate");
	Parameter *rotateParameter = getParameter("Rotate");
    Parameter *scaleParameter = getParameter("Scale");
	

	setChangeFunction("Translate", SLOT(setTransformationMatrix()));
	setChangeFunction("Rotate", SLOT(setTransformationMatrix()));
    setChangeFunction("Scale", SLOT(setTransformationMatrix()));


	// get output parameter group
	m_outputPointParameter = new ParameterGroup("Points");
	m_pointColorParameter = new NumberParameter("PointColor", Parameter::T_Float, 0.0);
	m_pointColorParameter->setSize(3);
	m_pointColorParameter->setVisible(false);
	m_pointPositionParameter = new NumberParameter("PointPosition", Parameter::T_Float, 0.0);
	m_pointPositionParameter->setSize(3);
	m_pointPositionParameter->setVisible(false);

	m_outputPointParameter->addParameter(m_pointPositionParameter);
	m_outputPointParameter->addParameter(m_pointColorParameter);

	m_vertexBuffer = Parameter::createGroupParameter("VertexBuffer", m_outputPointParameter);
	m_vertexBuffer ->setPinType(Parameter::PT_Output);
	parameterRoot->addParameter(m_vertexBuffer);

	setTransformationMatrix(); 

	//connect(this, SIGNAL(frameChanged(int)), SLOT(cam())); //trigger with realtime mode
	//connect(this, SIGNAL(newFrame()), SLOT(updateVideoTexture())); //trigger with Signal newFrame

}

//!
//! Destructor of the CameraTrackerNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
CameraTrackerNode::~CameraTrackerNode ()
{
   if(m_ptamm){
	 delete m_ptamm;
   }
   DEC_INSTANCE_COUNTER
}

///
/// Public Slots
///

///
/// Private Slots
///

//!
//! Sets a keyFrame
//!
void CameraTrackerNode::setFrame()
{	
	if(m_ptamm)
		m_ptamm->setKeyframe();
	else
		Log::error("No Camera connected to node.");
}

//!
//! Reset Tracking
//!
void CameraTrackerNode::resetTracking()
{
	if(m_ptamm)
	{
		m_ptamm->ResetAll();
		writeLog();
	}else
		Log::error("No Camera connected to node.");
}

//!
//! Sets map new
//!
void CameraTrackerNode::setNewMap()
{
	if(m_ptamm)
	{
		m_ptamm->NewMap();
		writeLog();
	} else
		Log::error("No Camera connected to node.");
}

//!
//! Loads a Map
//!
void CameraTrackerNode::loadMap()
{
	if(m_ptamm)
	{
		QString dir = getStringValue("Load Map");
		QString mapdir = dir.section('/', -1);
		dir.replace("/" + mapdir, "");
		mapdir = dir.section('/', -1);
		m_ptamm->loadMap(mapdir.toStdString());
		m_sizeofLastDrawnMap = 0;
	} else
		Log::error("No Camera connected to node.");
	
}

//!
//! Saves current Map
//!
void CameraTrackerNode::saveMap()
{
	if (m_ptamm)
	{
		QString dir = getStringValue("Filename");		
		m_ptamm->saveMap(dir.toStdString());
	} else 
		Log::error("No Camera connected to node.");
}
//!
//! Switch to next Map
//!
void CameraTrackerNode::switchToNextMap()
{
	if (m_ptamm)
	{
		int nextMap = m_mapID + 1;
		if(!m_ptamm->SwitchMap(nextMap, false))
			writeLog();
	} else 
		Log::error("No Camera connected to node.");
}

//!
//! Switch to previous Map
//!
void CameraTrackerNode::switchToPreviousMap()
{
	if (m_ptamm)
	{
		int previousMap = m_mapID - 1;
		if(!m_ptamm->SwitchMap(previousMap, false))
			writeLog();
	} else 
		Log::error("No Camera connected to node.");
}

//!
//! Delete current Map
//!
void CameraTrackerNode::deleteMap()
{
	if (m_ptamm)
	{
		m_ptamm->DeleteMap(m_ptamm->mpMap->MapID());
		writeLog();
	} else 
		Log::error("No Camera connected to node.");
}

//!
//! SLOT that is used on the matrixParameter
//!
void CameraTrackerNode::processInputMatrix()
{

	if(m_matrixParameter->isConnected() && m_imagePtr && m_ptamm)
	{
		cv::Mat* mat = m_imagePtr;
		m_ptamm->tracking(mat);
		if(m_ptamm->mpMap->IsGood()) 
		{
			m_toonCamPose = m_ptamm->getCurrentCamPose();

			m_toonPosition =  m_toonCamPose.inverse().get_translation(); //die inverse entspricht der direkten Kamerabewegung

			m_toonRotateMatrix =  m_toonCamPose.get_rotation().get_matrix();
			
			Ogre::Quaternion quat(Ogre::Matrix3   ( m_toonRotateMatrix(0,0), m_toonRotateMatrix(1,0), m_toonRotateMatrix(2,0), 
													m_toonRotateMatrix(0,1), m_toonRotateMatrix(1,1), m_toonRotateMatrix(2,1), 
													m_toonRotateMatrix(0,2), m_toonRotateMatrix(1,2), m_toonRotateMatrix(2,2) ));

			m_ptammMatrix.makeTransform(Ogre::Vector3(m_toonPosition[0], m_toonPosition[1], m_toonPosition[2]), Ogre::Vector3(1, 1, 1), quat);
			m_ptammMatrix = m_ptammMatrix * m_transMatrix;

			setPositionAndOrientation();
			setOutputPoints();
			if(m_mapID != m_ptamm->mpMap->MapID()){
				m_mapID = m_ptamm->mpMap->MapID();
				m_outputMapIdParameter->setValue(QVariant::fromValue(m_mapID), true);
			}
		}
		writeTrackerInformation();
	}
}

//!
//! Sets the TransformationsMatrix
//!	
void CameraTrackerNode::setTransformationMatrix()
{
	m_scale = getVectorValue("Scale");
	Ogre::Vector3 rotation = getVectorValue("Rotate");
	Ogre::Vector3 translation = getVectorValue("Translate");
	Ogre::Quaternion xRotation = Ogre::Quaternion(Ogre::Radian(Ogre::Degree(rotation.x)), Ogre::Vector3::UNIT_X);
    Ogre::Quaternion yRotation = Ogre::Quaternion(Ogre::Radian(Ogre::Degree(rotation.y)), Ogre::Vector3::UNIT_Y);
    Ogre::Quaternion zRotation = Ogre::Quaternion(Ogre::Radian(Ogre::Degree(rotation.z)), Ogre::Vector3::UNIT_Z);
    Ogre::Quaternion orientation = xRotation * yRotation * zRotation;
	m_transMatrix.makeTransform(translation, Ogre::Vector3(1.0,1.0,1.0), orientation);
}

///
/// Public Methods
///

///
/// Private Methods
///

//!
//! This Method is used to do stuff if connectMatrixSlot() has succeed
//!
void CameraTrackerNode::pastConnectToMatrix()
{
	if(m_ptamm)
		delete m_ptamm;
	m_ptamm = new PTAMM_Wrapper(m_width, m_height);
	QVariantList camList;
	std::vector<float> camParams(5,0.0);
	m_ptamm->getCameraParameters(camParams);
	for(int i=0; camParams.size() > i; i++){
		camList.append(QVariant(camParams[i]));
	}
	setValue("CameraParameters", camList, true);

	writeLog();
}


//!
//! Write Informations from PTAMM_Wrapper into the Logfile
//!
void CameraTrackerNode::writeLog()
{
	const string &msg = m_ptamm->getUserMessage();
	Log::info(QString::fromStdString(msg));
}

//!
//! Write Informations from PTAMM_Wrapper:tracking() into the Logfile
//!
void CameraTrackerNode::writeTrackerInformation()
{
	const string &msg = m_ptamm->getTrackerMessage();
	setValue("Tracker Info", QString::fromStdString(msg), true);
}

//!
//! set the outputParameters Position and Orientation
//!
void CameraTrackerNode::setPositionAndOrientation()
{
	setValue("Position", m_ptammMatrix.getTrans()*m_scale, true); //set Position

	setValue("Orientation", QVariant::fromValue<Ogre::Quaternion>(m_ptammMatrix.extractQuaternion()), true); //set Orientation
}

//!
//! sets the outputParameters pointPosition and pointColor
//!
void CameraTrackerNode::setOutputPoints()
{
	if(m_vertexBuffer->isConnected() && m_ptamm )
	{
		std::vector<MapPoint*> mapPoints = m_ptamm->mpMap->vpPoints;

		if(m_sizeofLastDrawnMap != (int) mapPoints.size())
		{
			m_sizeofLastDrawnMap = (int) mapPoints.size();
			QVariantList pointPositionList;
			QVariantList pointColorList;
			int size = 0;
			m_pointPositionParameter->setSize(0);
			m_pointColorParameter->setSize(0);
				for(size_t i=0; i<mapPoints.size(); i++)
				{
					m_pointPositionParameter->setSize(size = size + 3);
					m_pointColorParameter->setSize(size);
					TooN::Vector<3> v3Pos = mapPoints[i]->v3WorldPos;
					pointPositionList.append(QVariant(v3Pos[0]));
					pointPositionList.append(QVariant(v3Pos[1]));
					pointPositionList.append(QVariant(v3Pos[2]));

					switch(mapPoints[i]->nSourceLevel)
					{
					case 0: pointColorList.append(QVariant(1.0));	//R
							pointColorList.append(QVariant(0.0));	//G
							pointColorList.append(QVariant(0.0));	//B
							break;

					case 1: pointColorList.append(QVariant(1.0));	//R
							pointColorList.append(QVariant(1.0));	//G
							pointColorList.append(QVariant(0.0));	//B
							break;

					case 2:	pointColorList.append(QVariant(0.0));	//R
							pointColorList.append(QVariant(1.0));	//G
							pointColorList.append(QVariant(0.0));	//B
							break;

					default:pointColorList.append(QVariant(0.0));	//R
							pointColorList.append(QVariant(0.0));	//G
							pointColorList.append(QVariant(0.0));	//B
							break;
					}
				}
				m_pointPositionParameter->setValue(QVariant(pointPositionList), false);
				m_pointColorParameter->setValue(QVariant(pointColorList), false);
				m_vertexBuffer->propagateDirty();
		}
	}
}
