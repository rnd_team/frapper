project(poemreader)
#set(CMAKE_CONFIGURATION_TYPES Debug Release CACHE STRING "" FORCE) 
SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# poemreader
set( poemreader_h ${poemreader_h}		
			PoemReaderNode.h
			PoemReaderNodePlugin.h
			TagCloudView.h
			)

set( poemreader_moc ${poemreader_moc}	
			PoemReaderNode.h
			PoemReaderNodePlugin.h
			TagCloudView.h
			)

set( poemreader_src ${poemreader_src}	
			PoemReaderNode.cpp
			PoemReaderNodePlugin.cpp
			TagCloudView.cpp
			)

set( poemreader_res ${poemreader_res}	
			poemreader.xml
			)

# Create moc files		   
qt4_wrap_cpp(poemreader_cxx ${poemreader_moc})

# Create source groups
source_group("Moc Files" FILES ${poemreader_cxx})
source_group("Header Files" FILES ${poemreader_h})
source_group("Resources" FILES ${poemreader_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(poemreader_src ${poemreader_src} ${poemreader_h} ${poemreader_res})
ENDIF(WIN32)

# Create static library
add_library(poemreader SHARED ${poemreader_src} ${poemreader_cxx})

# Add library dependencies
target_link_libraries(poemreader
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			optimized frappergui debug frappergui_d
			)

# Install files
install( FILES ${poemreader_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libpoemreader_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libpoemreader.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS poemreader RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
