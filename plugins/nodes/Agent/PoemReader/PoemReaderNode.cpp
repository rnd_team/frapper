/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "PoemReaderNode.h"
//! \brief Implementation file for PoemReaderNode class.
//!
//! \author     Diana Arellano <diana.arellano@filmakademie.de>
//! \date       12.06.2012 (last updated)
//!

#include "PoemReaderNode.h"
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <iostream>

#define MAXWORDSSELECTED 3
#define WIDTHSCENE 768
#define HEIGHTSCENE 1280

///
/// Constructors and Destructors
///

//!

//! Constructor of the PoemReaderNode class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
PoemReaderNode::PoemReaderNode ( QString name, ParameterGroup *parameterRoot ) :
    ImageNode(name, parameterRoot),
	parameterRoot(parameterRoot)
{

	m_wordsGroup = new ParameterGroup("wordsTable");
	//parameterRoot->addParameter(m_wordsGroup);
	m_poemsGroup = new ParameterGroup("poemsGroup");
	parameterRoot->addParameter(m_poemsGroup);
	m_poemLinesGroup = new ParameterGroup("Poem Lines");
	parameterRoot->addParameter(m_poemLinesGroup);
	
	inputStringParameter = parameterRoot->getParameter("WordIn");
	inputTagParameter = parameterRoot->getParameter("TagIn");
	m_poemSelected = parameterRoot->getParameter("Poem Selected");
	animStateParameter = parameterRoot->getParameter("Animation State");
	m_alphaParameter = parameterRoot->getParameter("Alpha");

	groupParameter = Parameter::createGroupParameter("wordsGroup", m_wordsGroup);
	parameterRoot->addParameter(groupParameter);
    //groupParameter->setPinType(Parameter::PT_Output);
    groupParameter->setSelfEvaluating(true);
	groupParameter->setVisible(false);


	outputImageParameter = getParameter(m_outputImageName);	
	Ogre::TexturePtr m_texture;
	// create unique name for the reder texture
    Ogre::String uniqueRenderTextureName = createUniqueName("wordCloudTexture");
    // convert Ogre::String to QString
    QString qtUniqueRenderTextureName = QString::fromStdString(uniqueRenderTextureName);
	//m_texture = Ogre::TextureManager::getSingleton().createManual(uniqueRenderTextureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D, 700, 500, 0, Ogre::PixelFormat::PF_A8B8G8R8, Ogre::TU_RENDERTARGET, 0, false, 0);	
	m_texture = Ogre::TextureManager::getSingleton().createManual(uniqueRenderTextureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D, WIDTHSCENE, HEIGHTSCENE, 0, Ogre::PixelFormat::PF_A8B8G8R8, Ogre::TU_RENDERTARGET, 0, false, 0);	
	setValue(m_outputImageName, m_texture);

	// Connect the XML-File Dialog
	Parameter *filenameParameter = getParameter("XML File");
	if (filenameParameter){
        filenameParameter->setChangeFunction(SLOT(loadXmlFile()));
		filenameParameter->setCommandFunction(SLOT(reloadXmlFile()));
	}
    else
        Log::error("Could not obtain all parameters required for parsing the Poems file.", "PoemReaderNode::PoemReaderNode");

	m_tagCloudView = new TagCloudView();

	m_wordsTable = new QTableWidget(400, 3);
	rowsInTable = 0;
	poemLine = 1;
	m_numberWordsSelected = 0;

	 // set animation timer
    m_foutTimer = new QTimer(); 
	m_finTimer = new QTimer();
    connect(m_foutTimer, SIGNAL(timeout()), SLOT(updateFadingOutTimer()));
	connect(m_finTimer, SIGNAL(timeout()), SLOT(updateFadingInTimer()));
	connect(this, SIGNAL(emitGrammar(QString )), this, SLOT(readGrammar(QString )));
    
	// Function to be executed when the parameter changes its value
	setProcessingFunction("WordIn", SLOT(reduceWordsCloud()));
	setProcessingFunction("Animation State", SLOT(triggerPoemLines()));
}


//!
//! Destructor of the PoemReaderNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
PoemReaderNode::~PoemReaderNode ()
{
	m_wordsGroup->destroyAllParameters();
	m_poemsGroup->destroyAllParameters();
	
}


///
/// Public Methods
///



///
/// Private Methods
///


//!
//! Creates the parameters that will contain the poems data
//!
//!
void PoemReaderNode::createPoemsData() {
	
	int poemNumber = 1;
	m_numberWords = 0;

	m_wordsGroup->clear();
	
	//We get the poem to be analyzed according to the indexes in the list poemsindex
	const AbstractParameter::List *parameterList;
	parameterList = m_poemsGroup->getParameterList();

	foreach(AbstractParameter *aPoemParameter, *parameterList) {

		Parameter *poemParameter = dynamic_cast<Parameter *>(aPoemParameter);
		QString name = "poem" + QString::number(poemNumber);
		
		if(poemParameter->getName().compare(name) == 0) {

			QStringList poemdata = poemParameter->getValueString().split('&');
			
			QString poem = poemdata.at(1);

			//Remove punctuation marks from the strings
			QRegExp rx1("[\"!#$%&'()*+,./:;?@_`{|}~-]");
			poem.remove(rx1); 
		
			QStringList list = poem.split(" ");
			foreach (QString str, list)
			{
			//we don�t want words of less than 3 characters
			if (str.size() > 3 && !str.startsWith("<") && !str.endsWith(">") && !str.contains("=") && !str.startsWith(">")){
				
				QString parameterName = str + "Parameter";
				ParameterGroup *rowGroup = m_wordsGroup->getParameterGroup(parameterName);

				if (rowGroup == 0)
				{

					rowGroup = new ParameterGroup(str + "Parameter");
					//Parameter *wordParameter = new Parameter("word", Parameter::T_String, QVariant(str));
					NumberParameter *frequencyParameter = new NumberParameter("frequency", Parameter::T_Int, QVariant(1));
					Parameter *poemIndexesParameter = new Parameter("poemIndexes", Parameter::T_String, QVariant(QString::number(poemNumber)) );

					frequencyParameter->setVisible(false);
					poemIndexesParameter->setVisible(false);

					//rowGroup->addParameter(wordParameter);
					rowGroup->addParameter(frequencyParameter);
					rowGroup->addParameter(poemIndexesParameter);	

					m_wordsGroup->addParameter(rowGroup);

					m_numberWords++;
				}
				else
				{	//the word already exists in the table
					//ParameterGroup *rowGroup = m_wordsGroup->getParameterGroup(str + "Parameter");

					int frequency = rowGroup->getParameter("frequency")->getValue().toInt();
					frequency++;
					rowGroup->setValue("frequency", QVariant(frequency));
					
					QString prevPoemIndexes = rowGroup->getParameter("poemIndexes")->getValueString();
		
					if ( !(prevPoemIndexes.contains(QString::number(poemNumber))) ) { //different poem
						QString s = prevPoemIndexes + "," + QString::number(poemNumber);
						rowGroup->setValue("poemIndexes", QVariant(s));
					}				
				}
			}
		}
		poemNumber++;
		}
	}
	
	
}

//!
//! Finds a given word in the ParameterList of the Node
//!
bool PoemReaderNode::findWord(QString str){

	const AbstractParameter::List *w_list, *r_list;
	
	w_list = m_wordsGroup->getParameterList();

	foreach (AbstractParameter *row, *w_list) {
        r_list = (dynamic_cast<ParameterGroup *>(row))->getParameterList();

		// The first parameter "word" (wordParameter) will be evaluated
		AbstractParameter *word = r_list->at(0);
		QString parameterName = dynamic_cast<Parameter *>(word)->getName();
		QString strInList = (dynamic_cast<Parameter *>(word))->getValueString();
		if (strInList.compare(str) == 0){
			return true;
		}
    }
	return false;
}

//!
//! Eliminates the wildcards "I choose" and "use ... please"
//!
QString PoemReaderNode::removeWildCards(QString word)
{
	if (word.contains("I choose"))
		word.remove("I choose");
	else if (word.contains("use "))
	{
		word.remove("use");
		word.remove("please");
	}
	word = word.trimmed();
	return word;
}

//!
//! This function allows to elicit an empathic response from the character
//!
void PoemReaderNode::replyFromCharacter(QString word)
{
	QString tmp = "That is a beautiful word";
	QStringList list;
	list << "That is a beautiful word" << "Great Choice" << "Ok, I got that" << "Interesting" << "Let me see if I can work with that" << "That sound wonderful";

	//Randomization							
	int nsentences = list.count();
	int low = 0;
	int chosen = qrand() % ((nsentences) - low) + low;
	tmp = list.at(chosen);
	
	setValue("Poem Selected", tmp, true);
}

//!
//! Reduces the amount of words in the wordCloud 
//!
void PoemReaderNode::reduceWordsCloud()
{
	QString word = inputStringParameter->getValueString();
	QString tag = inputTagParameter->getValueString();
	bool fade = false;


	if (word.startsWith("pcw__")) {		//is a command to reduce the cloud

		word.remove("pcw__");
		word = removeWildCards(word);		

		QString parameterName = word + "Parameter";
		ParameterGroup *parameterGroup = m_wordsGroup->getParameterGroup(parameterName);

		if (parameterGroup != 0){

			poemsIndexStr = parameterGroup->getParameter("poemIndexes")->getValueString();
			QStringList m_poemsindex = poemsIndexStr.split(",");
			
			if (m_numberWordsSelected < MAXWORDSSELECTED-1) {	//The word exists

				//we clean the parameter with all the words of the poems
				//m_wordsGroup->clear(); // simple version
				m_wordsGroup = getGroupValue("wordsGroup", false);
				m_wordsGroup->destroyAllParameters();
				
				foreach(QString id, m_poemsindex){
				
					QString name = "poem" + id;
					Parameter *poemParameter = m_poemsGroup->getParameter(name);
					QString poemstring = poemParameter->getValueString();

					QStringList poemdata = poemstring.split('&');
					QString poem = poemdata.at(1);
					
					
					//QString poem = poemParameter->getValueString();


					//Remove punctuation marks from the strings
					QRegExp rx1("[\"!#$%&'()*+,./:;?@_`{|}~-]");
					poem.remove(rx1);

					QStringList poemList = poem.split(" ");

					foreach (QString str, poemList){
						//we don�t want words of less than 3 characters
						//if (str.size() > 3){
						if (str.size() > 3 && !str.startsWith("<") && !str.endsWith(">") && !str.contains("=") && !str.startsWith(">")){
							
							parameterName = str + "Parameter";
							ParameterGroup  *rowGroup = m_wordsGroup->getParameterGroup(parameterName);
							if ( rowGroup == 0 ) {

								rowGroup = new ParameterGroup(parameterName);

								//Parameter *wordParameter = new Parameter("word", Parameter::T_String, QVariant(str));
								NumberParameter *frequencyParameter = new NumberParameter("frequency", Parameter::T_Int, QVariant(1));
								Parameter *poemIndexesParameter = new Parameter("poemIndexes", Parameter::T_String, QVariant(QString::number(id.toInt())) );

								//rowGroup->addParameter(wordParameter);
								rowGroup->addParameter(frequencyParameter);
								rowGroup->addParameter(poemIndexesParameter);

								m_wordsGroup->addParameter(rowGroup);
							
							}
							else{	//the word already exists in the table
								int frequency = rowGroup->getParameter("frequency")->getValue().toInt();
								frequency++;
								rowGroup->setValue("frequency", QVariant(frequency));
								
								QString prevPoemIndexes = rowGroup->getParameter("poemIndexes")->getValueString();
					
								if ( !(prevPoemIndexes.contains(QString::number(id.toInt()))) ) { //different poem
									QString s = prevPoemIndexes + "," + QString::number(id.toInt());
									rowGroup->setValue("poemIndexes", QVariant(s));
								}				
							}
						}
					}			
				}
				m_numberWordsSelected++;

				createWordsTable(word);

				// Rendering and Visualization of the TagCloud
				m_tagCloudView->RenderCloud(m_wordsTable, rowsInTable);
				m_tagCloudView->createImage();
				m_tagCloudView->setImage(this);	// Creation of the image
				triggerRedraw();
				replyFromCharacter(word);
				
			}
			else{
				
				//Randomization							
				int npoems = m_poemsindex.count();
				int low = 0;
				int chosen = qrand() % ((npoems) - low) + low;
				QString name = "poem" + m_poemsindex.takeAt(chosen); // QString::number(chosen);
				setPoemSelected(name);	
			}
			
		}
	}
}

bool PoemReaderNode::wordAccepted(QString word)
{
	//TODO: this should read from XML file or something more elegant
	QStringList words;
	words << "this" << "there" << "that" << "where" << "when" << "what" << "would" << "could" << "will" << "with" << "yours" << "your" << "mine" << "their" << "blue" << "must" << "without"
			<< "into" << "each" << "we'd";

	if (words.contains(word, Qt::CaseInsensitive))
		return false;
	else
		return true;
}

//! 
//! Creates the widget that holds the words to be represented in the cloud
//!
void PoemReaderNode::createWordsTable(QString wordselected)
{
	int i = 0;
	rowsInTable=0;
	
	
	const AbstractParameter::List *parameterList, *wordList;
	QString grammar;

	parameterList = getGroupValue("wordsGroup", false)->getParameterList();
	
	m_tagCloudView->scene()->clear();
	m_wordsTable->clear();

	foreach(AbstractParameter *rowList, *parameterList) {

		wordList = (dynamic_cast<ParameterGroup *>(rowList))->getParameterList();
		QString wordString = (dynamic_cast<ParameterGroup *>(rowList))->getName().remove("Parameter");
		QString freqString = dynamic_cast<Parameter *>(wordList->at(0))->getValueString();
		qreal freqNumber = dynamic_cast<NumberParameter *>(wordList->at(0))->getValue().toReal();
		QString indexString = dynamic_cast<Parameter *>(wordList->at(1))->getValueString();

		if (freqNumber > 1 && wordAccepted(wordString) && wordString.compare(wordselected, Qt::CaseInsensitive) != 0) {
			
			QTableWidgetItem *wordItem = new QTableWidgetItem;
			QTableWidgetItem *frequencyItem = new QTableWidgetItem;
			QTableWidgetItem *indexesItem = new QTableWidgetItem;

			wordItem->setText(wordString);
			frequencyItem->setText(freqString);
			indexesItem->setText(indexString);

			m_wordsTable->insertRow(rowsInTable);
			m_wordsTable->setItem(rowsInTable,0,wordItem);
			m_wordsTable->setItem(rowsInTable,1,frequencyItem);
			m_wordsTable->setItem(rowsInTable,2,indexesItem);

			rowsInTable++;

			grammar.append(wordString + "&");

		}			
	}	
	emit emitGrammar(grammar);
}

///
/// Private Slots
///

//!
//! load node settings from XML File
//!
void PoemReaderNode::loadXmlFile()
{
	Q_SLOT

	int index = 1;
	

	// get Filename from Input Parameter
	QString filename = getStringValue("XML File");
    if (filename == "") { return; }

	// load file or return on error
	QFile* xmlfile = new QFile(filename);
	if (!xmlfile->open(QIODevice::ReadOnly | QIODevice::Text)) {
		Log::error(QString("File %1 could not be loaded!").arg(filename), "PoemReaderNode::loadXmlFile");
		return;
	}
	m_poemsGroup->clear();
	m_numberWordsSelected = 0;
	m_poemSelected->setValue(QVariant(""));
	setValue("Alpha", QVariant(100.0), true);
	
	QXmlStreamReader xml(xmlfile);
	
	// read contents
	while(!xml.atEnd() &&
	      !xml.hasError()) {

		// traverse XML-Tree 
		xml.readNext();
		QString str = xml.name().toString();

		if(xml.name() == "poems" && xml.isStartElement()) {

			bool inToken = true;

			while(inToken){

				xml.readNext();

				if(xml.name() == "poem"){

					QXmlStreamAttributes attrs = xml.attributes();
					//QString title = attrs.value("title").toString();
					//QString author = attrs.value("author").toString();
					QString poem = attrs.value("defaultValue").toString();

					poem.insert(0,  attrs.value("title").toString() + ". By " +  attrs.value("author").toString() + "&");

					poem.replace('[', '<');
					poem.replace(']', '>');
					poem.replace('\'', '\"');					

					if(!attrs.value("title").toString().isEmpty()){
						// set the values for the table that contains the data of the cloud
						QString name = "poem" + QString::number(index);
						Parameter *poemParameter = new Parameter( name, Parameter::T_String, QVariant(poem) ); 
						NumberParameter *buttonParameter = new NumberParameter("Select "+QString(name), Parameter::T_Command, name);

						m_poemsGroup->addParameter(poemParameter);
						m_poemsGroup->addParameter(buttonParameter);

						buttonParameter->setCommandFunction(SLOT(updatePoemSelected()));

						poemsIndexStr.append(QString::number(index)+",");
						index++;						
					}
				}
			
				if(xml.name() == "poems" && xml.isEndElement()) {
					inToken = false;
					poemsIndexStr.chop(1);
				}
			}
		}
	}

	// Give Feedback to the log
	if(xml.hasError())
		Log::error(QString("File %1 contains Errors!").arg(filename), "PoemReaderNode::loadXmlFile");
	else {
		Log::info(QString("File %1 successfully loaded.").arg(filename), "PoemReaderNode::loadXmlFile");
		createPoemsData();
		createWordsTable("");

		// Rendering and Visualization of the TagCloud
		m_tagCloudView->RenderCloud(m_wordsTable, rowsInTable);
		m_tagCloudView->createImage();
		m_tagCloudView->setImage(this);	// Creation of the image
		triggerRedraw();
	}
}


//!
//! wrapper function for loadXmlFile()
//!
void PoemReaderNode::reloadXmlFile()
{

	
	// return if no file to reload is selected
    if (getStringValue("XML File") == "") { return; }
	
	// get output parameter group
	ParameterGroup *m_OutputParameters = parameterRoot->getParameterGroup("poemsGroup");
	ParameterGroup *m_referenceData;

	/*
	// clear output parameters
	//m_OutputParameters->destroyAllParameters();
	parameterRoot->getParameterGroup("poemsGroup")->destroyAllParameters();
	
	//parameterRoot->getParameterGroup("wordsTable")->destroyAllParameters();
	m_referenceData = getGroupValue("wordsGroup", false);
	m_referenceData->destroyAllParameters();
	*/
	
	// continue with loadXmlFile()
	loadXmlFile();
	// starts unfading
	m_finTimer->start(70);
}


//!
//! Notifies that the output parameter with the line of the selected poem has changed
//!
void PoemReaderNode::notifySelection(){

	notifyChange();
}


//!
//! Notifies that the output parameter with the line of the selected poem has changed
//!
void PoemReaderNode::updatePoemSelected(){

	Parameter *parameter = dynamic_cast<Parameter *>(sender());
	NumberParameter *numberParameter = static_cast<NumberParameter *>(parameter);

	setPoemSelected(numberParameter->getValueString());
}


//!
//! Updates the value of the output parameter selected poem.
//! Due to issues with the voice synthesis the poem is split in lines and the update is done by lines 
//! and passed to the TcpClient line by line 
//!
void PoemReaderNode::setPoemSelected(QString name){

	// starts fading
	m_foutTimer->start(70);

	Parameter *poemParameter = m_poemsGroup->getParameter(name);
	QString poem = poemParameter->getValueString();
	poem.replace('&', ' ');
	m_poemLinesGroup->clear();
	poemLine = 0;
	
	//Break the poem by its lines
	QStringList poemLines = poem.split("<n>");
	int i=0;

	foreach(QString line, poemLines){
		
		m_poemLinesGroup->addParameter(new Parameter("Line"+QString::number(i), Parameter::T_String, QVariant(line)));
		i++;
	}
	//setValue("Poem Selected", m_poemLinesGroup->getParameter("Line0")->getValueString());
	
	triggerPoemLines();
}


//!
//! Triggers the poem line when the input value regarding the state of the animation is false
//!
void PoemReaderNode::triggerPoemLines(){
	
	Parameter *param = parameterRoot->getParameter("Animation State");
	bool active = param->getValue().toBool();
	
	if (!active){

		Parameter *line = parameterRoot->getParameterGroup("Poem Lines")->getParameter("Line"+QString::number(poemLine));
		
		if (line)  {
			poemLine++;
			QString tmp = line->getValueString();

			setValue("Poem Selected", tmp, true);
			if (tmp.contains("QUERY_LOOP")) {
			
				reloadXmlFile();			
			}
			
		}		
	}
}



void PoemReaderNode::updateFadingOutTimer() {

	qreal alpha = getParameter("Alpha")->getValue().toFloat();

	if (alpha > 0.0 && alpha <= 100.0) {
		alpha -= 5.0;
		setValue("Alpha", QVariant(alpha), true);
		//triggerRedraw();

		if (alpha <= 0.0){
			m_foutTimer->stop();
		}
	}	
}


void PoemReaderNode::updateFadingInTimer() {

	qreal alpha = getParameter("Alpha")->getValue().toFloat();

	if (alpha >= 0.0 && alpha < 100.0) {
		alpha += 5.0;
		setValue("Alpha", QVariant(alpha), true);
		//triggerRedraw();

		if (alpha >= 100.0){
			m_finTimer->stop();
		}
	}	
}

void PoemReaderNode::fadingOut() {

	qreal alpha = getParameter("Alpha")->getValue().toFloat();

	while(alpha > 0.0 && alpha <= 100.0) {
		qreal alpha = getParameter("Alpha")->getValue().toFloat();
		alpha -= 5.0;
		setValue("Alpha", QVariant(alpha), true);
		//triggerRedraw();

		if (alpha <= 0.0){
			m_foutTimer->stop();
		}
	}	
}

void PoemReaderNode::fadingIn() 
{

	qreal alpha = getParameter("Alpha")->getValue().toFloat();

	while(alpha >= 0.0 && alpha < 100.0) {
		qreal alpha = getParameter("Alpha")->getValue().toFloat();
		alpha += 5.0;
		setValue("Alpha", QVariant(alpha), true);
		//triggerRedraw();

		if (alpha >= 100.0){
			m_foutTimer->stop();
		}
	}	
}

void PoemReaderNode::readGrammar(QString grammar)
{
	
	setValue("Grammar", grammar, true);
	
}