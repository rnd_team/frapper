project(Agent)

set( VS_PROJECT_FOLDER "Nodes/Agent" )
add_subdirectory(HeadLogic)
add_subdirectory(RandomBlink)
add_subdirectory(PoemReader)

