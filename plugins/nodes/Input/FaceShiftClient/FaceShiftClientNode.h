/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "FaceShiftClientNode.h"
//! \brief Header file for FaceShiftClientNode class.
//!
//! \author     Michael Bu�ler <michael.bussler@filmakademie.de>
//! \version    1.0
//! \date       06.06.2012 (last updated)
//!

#ifndef FACESHIFTCLIENTNODE_H
#define FACESHIFTCLIENTNODE_H

#include "Node.h"
#include "ParameterGroup.h"
#include <QtCore/QDataStream>
#include <QtNetwork/QTcpSocket>

// FaceShift binary network stream format
#include <fsstream.h>

using namespace Frapper;

//!
//! Input node for FaceShift tracking data via tcp
//!
class FaceShiftClientNode : public Node //, public QThread
{

    Q_OBJECT
    ADD_INSTANCE_COUNTER

public: // constructors and destructors

    //!
    //! Constructor of the FaceShiftClientNode class.
    //!
    //! \param name The name for the new node.
    //! \param parameterRoot A copy of the parameter tree specific for the type of the node.
    //!
    FaceShiftClientNode ( QString name, ParameterGroup *parameterRoot );

    //!
    //! Destructor of the FaceShiftClientNode class.
    //!
    ~FaceShiftClientNode ();

public: // functions

    virtual void run();


private slots: //

    //!
    //! Slot which is called when running flag on node is toggled.
    //!
    void toggleRun();

    //!
    //! Slot which is called when running flag on node is toggled.
    //!
    void processInputData();

    //!
    //! Slot which is called when thread has finished.
    //!
    void threadFinished();

private: // data

    //!
    //! The udp socket.
    //!
    QTcpSocket m_tcpSocket;

    //!
    //! Decoder for binary FaceShift network stream
    //!
    fs::fsBinaryTrackingStream m_trackingData;

    //!
    //! The parameter group of animation parameters
    //!
    ParameterGroup* m_animParams;


};


#endif
