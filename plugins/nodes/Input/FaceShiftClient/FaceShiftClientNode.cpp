/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "FaceShiftClientNode.cpp"
//! \brief Implementation file for FaceShiftClientNode class.
//!
//! \author     Michael Bu�ler <michael.bussler@filmakademie.de>
//! \version    1.0
//! \date       06.06.2012 (last updated)
//!

#include "FaceShiftClientNode.h"
#include "NumberParameter.h"

INIT_INSTANCE_COUNTER(FaceShiftClientNode)


///
/// Constructors and Destructors
///


//!
//! Constructor of the FaceShiftClientNode class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
FaceShiftClientNode::FaceShiftClientNode ( QString name, ParameterGroup *parameterRoot ) :
    Node(name, parameterRoot),
    m_animParams(NULL)
{   
    setChangeFunction("Run", SLOT(toggleRun()));

    m_animParams = getParameterGroup("Animations");
    assert( m_animParams );
}

//!
//! Destructor of the FaceShiftClientNode class.
//!
FaceShiftClientNode::~FaceShiftClientNode ()
{
    quit();
    wait();
}

///
/// Public Slots
///

//!
//! Function which is called to establish a connection
//! Executed by QThread::start()
//!
void FaceShiftClientNode::run()
{
    unsigned int port = getValue("Port").toUInt();
    QString host = getValue("Host").toString();

    if( port > 0 && host != "" )
    {
        m_tcpSocket.connectToHost(host, port, QIODevice::ReadOnly);
        Log::debug("Trying to connect to "+host+":"+QString::number(port)+"...", "FaceShiftClientNode::run()");

        if (m_tcpSocket.waitForConnected(5000))
        {
            // connected
            Log::debug("Connection established!", "FaceShiftClientNode::run()");

            // connect events
            connect(&m_tcpSocket, SIGNAL(readyRead()), this, SLOT(processInputData()), Qt::DirectConnection);
            connect(this, SIGNAL(finished()), this, SLOT(threadFinished()), Qt::DirectConnection);

            // enter execution loop
            exec();
        }
        else
        {
            Log::error("Connection timed out after 5s! "+m_tcpSocket.errorString());
            setValue("Run", false );
            toggleRun();
        }
    }
}


//!
//! Slot which is called when new datagrams are availabled on udp socket.
//!
void FaceShiftClientNode::processInputData()
{
    m_trackingData.append(m_tcpSocket.readAll());
    //Log::debug("Tracking data received!", "FaceShiftClientNode::processInputData");

    fs::fsTrackingData td;
    if( m_trackingData.decode( td, true))
    {
        //Log::debug("Tracking data decoded!", "FaceShiftClientNode::processInputData");
        int numBlendshapes = std::min<int>(m_animParams->getParameterList()->size(), td.m_coeffs.size());

        for ( int i=0; i< numBlendshapes; i++)
        {
            Parameter* blendshapeParam = dynamic_cast<Parameter *>(m_animParams->getParameterList()->at(i));
            assert( blendshapeParam );
            blendshapeParam->setValue( QVariant( td.m_coeffs[i]*100.0f), true);
        }
    };
}

///
/// Private Slots
///


//!
//! Slot which is called when running flag on node is toggled.
//!
void FaceShiftClientNode::toggleRun()
{
    bool run = getBoolValue("Run");
    if (run || !isRunning()) {
        start();   
    }
    else {
        quit();
    }

}

//!
//! Slot which is called when thread has finished.
//!
void FaceShiftClientNode::threadFinished()
{
    Log::debug("Closing Connection", "FaceShiftClientNode::threadFinished");
    m_tcpSocket.abort();
    m_trackingData.clear();
}


