/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "CameraInputNode.cpp"
//! \brief Camera Input
//!
//! \author     Martin Schwarz <martin.schwarz@filmakademie.de>
//! \version    1.0
//! \date       27.4.2012 (last updated)
//!

#include "CameraInputNode.h"
#include "Log.h"

using namespace std;
#define MAXNUMBEROFCAMERAS 1 //maximum of connected cameras - set this higher if you have more connected capture 

INIT_INSTANCE_COUNTER(CameraInputNode)

///
/// Public Constructors
///

//!
//! Constructor of the CameraInputNode class.
//!
//! \param name The name to give the new mesh node.
//!
CameraInputNode::CameraInputNode ( QString name, ParameterGroup *parameterRoot) :
ImageNode(name, parameterRoot)
{
	INC_INSTANCE_COUNTER
	m_video = 0;
	Frapper::EnumerationParameter *availableCameraEnumeration = new Frapper::EnumerationParameter("Choose Camera", 0);
	parameterRoot->addParameter(availableCameraEnumeration);
	QStringList str; 
	QStringList cam;
	bool ret = true;
	for(int i=0; (MAXNUMBEROFCAMERAS-1) >= i; i++){
		ret = VideoSource::checkCamera(i);
		if (!ret && i==0){ // no cam available
			cam.append("No Cam Available");
			str.append(QString::number(i));
		}
		if (i == MAXNUMBEROFCAMERAS && i > 0){ //debug some cameras dont work propper with opencv and put them self on every id
			cam.append("Something is wrong with one of your connected cameras");
			str.append(QString::number(i));
			Frapper::Log::debug("Something is wrong with on of your connected cameras");
			ret = false;
		}
		if (ret){
			cam.append("CameraID");
			str.append(QString::number(i));	
		} else 
			ret = false;
	}

	availableCameraEnumeration->setLiterals(QStringList() << cam);
	availableCameraEnumeration->setValues(QStringList() << str);
	availableCameraEnumeration->setChangeFunction(SLOT(changeCameraSource()));

	Frapper::Parameter *sleepParameter = getParameter("ThreadSleepInMiliseconds");
	if(sleepParameter){
		sleepParameter->setChangeFunction(SLOT(setSleepingThreadTime()));
		m_sleepTime = getDoubleValue("ThreadSleepInMiliseconds");
	}
	
	//use Signal newFrame to trigger produceNewFrame -> interprocess communication
	connect(this, SIGNAL(newFrame()), SLOT(updateVideoTexture()));
	initialise(0); //intialise cam with the the id 0 - 0 is the first capture device 
}

//!
//! Destructor of the CameraInputNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
CameraInputNode::~CameraInputNode ()
{
	m_endThreadRequested = true;
	m_shallThreadSleep = false;
	m_texture.setNull();
	
	while(isRunning())
	{
		m_moreFrames.wakeOne();
		m_sleepThread.wakeOne();
	}
	delete m_video;
	DEC_INSTANCE_COUNTER
}

///
/// Public Slots
///



///
/// Private Slots
///

//!
//! Sets Thread in Sleeping Mode or wakes the thread
//!
void CameraInputNode::sleepOrWakeupThread()
{
	if( isViewed() || m_pictureParameter->isConnected() || m_matrixParameter->isConnected() || m_matrixRawParameter->isConnected()){
		m_shallThreadSleep = false;
		m_sleepThread.wakeOne();
	} else {
		m_shallThreadSleep = true;
	}
}

//!
//! Changes the Camera Source
//!
void CameraInputNode::changeCameraSource()
{
   // obtain the render resolution preset parameter 
    EnumerationParameter *availableCamera = getEnumerationParameter("Choose Camera");
    if (!availableCamera) {
        Log::error("Available Camera parameter could not be obtained.", "CameraInputNode::changeCameraSource");
        return;
    }

    // get the value of the currently selected item in the enumeration
    QString value = availableCamera->getCurrentValue(); 
	if (value.isNull()){
        //> no value is available for the selected render resolution preset
        return;
	}
	int camid = value.toInt();
	m_endThreadRequested = true;
	m_shallThreadSleep = false;
	while(isRunning()) {m_moreFrames.wakeOne(); m_sleepThread.wakeOne();};
	delete m_video;
	m_matrixParameter->setDirty(false);
	getParameterRoot()->removeParameter(m_matrixParameter);
	m_matrixRawParameter->setDirty(false);
	getParameterRoot()->removeParameter(m_matrixRawParameter);
	initialise(camid);
}

//!
//! Updates the videoTexture
//!
void CameraInputNode::updateVideoTexture()
{	
	
	QMutexLocker locker(&m_frameVideoMutex); //lock Mutex
	if(m_colorImage.data != 0 && ( isViewed() || m_pictureParameter->isConnected()))
	{
		Ogre::HardwarePixelBufferSharedPtr pixelBuffer = m_texture->getBuffer();
		// lock the pixel buffer 
		pixelBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);// for best performance use HBL_DISCARD!
		const Ogre::PixelBox pixelBox = pixelBuffer->getCurrentLock();
		unsigned char* pDest = static_cast<unsigned char*>(pixelBox.data);
		//copy imageData
		memcpy(pDest, m_colorImage.data, m_videoSize); //copy image.data into pDest
		//this is just an example how you can set every pixel
		////for(int row=0;row< colorImage.rows ;row++)
		////{
		////	uchar* ptr = (uchar*)(colorImage.data + colorImage.row*colorImage.step);
		////	for(int col=0;col<colorImage.cols;col++)
		////	{
		////		*pDest++ = *ptr++;
		////		*pDest++ = *ptr++;
		////		*pDest++ = *ptr++; 
		////		*pDest++ = *ptr++;
		////	}
		////}
		// unlock pixel buffer
		pixelBuffer->unlock();
		this->triggerRedraw(); // redraw the ogreScene
	}

	m_moreFrames.wakeOne(); //trigger waiting condition once
}

void CameraInputNode::setSleepingThreadTime()
{
	m_sleepTime = getDoubleValue("ThreadSleepInMiliseconds");
}

///
/// Public Methods
///


///
/// Private Methods
///

//!
//! Thread to get newFrame from Camera
//!
void CameraInputNode::run()
{
	forever {	
		if ( m_endThreadRequested ) {//if stopping of thread has been requested, stop the thread
			quit();
			return;		
		}
		QMutexLocker locker(&m_frameVideoMutex); //lock mutex
		if( m_shallThreadSleep){
			m_sleepThread.wait(&m_frameVideoMutex);
		}
		m_video->getRawAndColorImage(m_colorImage, m_rawImage);

		if(m_matrixParameter->isConnected() && !m_matrixParameter->isDirty()){
			m_matrixParameter->propagateDirty();
		}

		if(m_matrixRawParameter->isConnected() && !m_matrixRawParameter->isDirty()){
			//m_rawImage = m_video->getNewRawImage();
			m_matrixRawParameter->propagateDirty();
		}

		emit newFrame(); //trigger mainthread updateVideoTexture()
		m_moreFrames.wait(&m_frameVideoMutex);	//wait until waitcondition is fullfilled
		msleep(m_sleepTime);
	}
}


//!
//! Is used for initialising or reinitialing
//! \param CameraId this value is used to setup the camera 
//!
void CameraInputNode::initialise(int CameraId)
{    
	m_video = new VideoSource(CameraId);
	if(m_video->isCamAvailable()){
		//initialising successful
		m_CameraID = CameraId; //save CameraID
		m_colorImage = m_video->getNewColorImage(); //get a new ColorImage
		m_rawImage = m_video->getNewRawImage();
		m_colorImagePtr = &m_colorImage; 
		m_rawImagePtr = &m_rawImage;
		m_pictureParameter = getParameter(m_outputImageName);

		m_matrixParameter = new Frapper::GenericParameter("OpencvMatrix", 0); //gernerate a new GenericParameter
		m_matrixParameter->setTypeInfo("cv::Mat");
		m_matrixParameter->setDescription("ColorMat");
		m_matrixParameter->setPinType(Frapper::Parameter::PT_Output);	
		getParameterRoot()->addParameter(m_matrixParameter);
		m_matrixParameter->setValue(QVariant::fromValue<cv::Mat*>(m_colorImagePtr), true);
		m_matrixParameter->setOnDisconnectFunction(SLOT(sleepOrWakeupThread()));
		m_matrixParameter->setOnConnectFunction(SLOT(sleepOrWakeupThread()));

		m_matrixRawParameter = new Frapper::GenericParameter("OpencvRawMatrix", 0); //gernerate a new GenericParameter
		m_matrixRawParameter->setTypeInfo("cv::Mat");
		m_matrixRawParameter->setDescription("RawMat");
		m_matrixRawParameter->setPinType(Frapper::Parameter::PT_Output);	
		getParameterRoot()->addParameter(m_matrixRawParameter);
		m_matrixRawParameter->setValue(QVariant::fromValue<cv::Mat*>(m_rawImagePtr), true);
		m_matrixRawParameter->setOnDisconnectFunction(SLOT(sleepOrWakeupThread()));
		m_matrixRawParameter->setOnConnectFunction(SLOT(sleepOrWakeupThread()));

		m_pictureParameter->setOnDisconnectFunction(SLOT(sleepOrWakeupThread()));
		m_pictureParameter->setOnConnectFunction(SLOT(sleepOrWakeupThread()));
		emit nodeChanged(); //is needed because the node has changed new Parameter
		connect(this, SIGNAL( nodeChanged ()), SLOT(sleepOrWakeupThread())); //if node changes sleepOrWakeupThread is triggered
		m_videoSize = m_colorImage.rows*m_colorImage.cols*m_colorImage.channels(); 
		setValue("Resolution", "Native Resolution " + QString::number(m_colorImage.cols) + " x " + QString::number(m_colorImage.rows) + " width x height in pixels", true);
		Ogre::TextureManager &textureManager = Ogre::TextureManager::getSingleton();
		if (textureManager.resourceExists(m_uniqueRenderTextureName)) {
			textureManager.remove(m_uniqueRenderTextureName);
		}
		// create unique name for the cam texture
		m_uniqueRenderTextureName = createUniqueName("CameraTexture");
		m_texture = Ogre::TextureManager::getSingleton().createManual(m_uniqueRenderTextureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D, m_colorImage.cols, m_colorImage.rows, 0, Ogre::PF_X8R8G8B8, Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
		setValue(m_outputImageName, m_texture, true);
		//thread shall sleep at the beginning
		m_shallThreadSleep = false;
		m_endThreadRequested = false;
		start(QThread::LowestPriority);
	} else {
		//initialising not successful
		Ogre::TextureManager &textureManager = Ogre::TextureManager::getSingleton();
		m_defaultImage.load("DefaultRenderImage.png", "General");
		m_texture = textureManager.loadImage("MissingCameraTexture", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, m_defaultImage);
		setValue(m_outputImageName, m_texture);	
	}

}