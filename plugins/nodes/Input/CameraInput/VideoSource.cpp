/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "VideoSource.cpp"
//! \brief Implementation file for VideoSource class.
//!
//! \author     Martin Schwarz <martin.schwarz@filmakademie.de>
//! \version    1.0
//! \date       30.01.2012 (last updated)
//!
//!

#include "VideoSource.h"

using namespace cv;

///
/// Public Constructors
///
//!
//! Constructor of the VideoSource class.
//! \param camid The camid is used to setup VideoCapture. The camid says which cam should be used
//!
VideoSource::VideoSource(int camid)
{

	cap = new VideoCapture(camid);

	if(cap->isOpened()){
		camAvailable = true;
	} else {
		camAvailable = false;
	}
}

//!
//! Destructor of the VideoSource class.
//!
VideoSource::~VideoSource()
{
	if(cap->isOpened()){
		cap->release();
	}
}

//!
//! This static function is used to check if camera with given ID is available
//! \param The camid to check if available
//! \return true if Camera with ID i is available
//!
bool VideoSource::checkCamera(int camid)
{
	VideoCapture* test;
	bool ret;
	test = new VideoCapture(camid);	
	if(!test->isOpened()){
		ret = false;
	} else {
		test->release();
		ret = true;
	}
	delete test;
	return ret;
}

//!
//! Is used to grab a ColorImage from the camera
//! \return Mat newColorFrame from the camera. The returned matrix has 4 Channels (=BGRA)
//!
Mat VideoSource::getNewColorImage()
{
	*cap >> newColorFrame;
	cvtColor(newColorFrame, newColorFrame, CV_8U, 4);
	return newColorFrame;
}

//!
//! Is used to grab a GrayImage from the camera
//! \return Mat newGrayFrame from the camera. The returned matrix has 1 Channel
//!
Mat VideoSource::getNewGrayImage()
{
	*cap >> newGrayFrame;
	cvtColor(newGrayFrame, newGrayFrame, CV_RGB2GRAY, 1);
	return newGrayFrame;
}

//!
//! Is used to grab a RawImage from the camera
//! \return Mat newRawFrame from the camera. 
//!
Mat VideoSource::getNewRawImage()
{
	*cap >> newRawFrame;
	return newRawFrame;
}

//!
//! Is used to grab a colorImage and a RawImage
//! \param The colorImage
//! \param The rawImage
//!
void VideoSource::getRawAndColorImage(Mat &colorimage, Mat &raw)
{
	*cap >> raw;
	cvtColor(raw, colorimage, CV_8U, 4);
}

//!
//! \return True if Camera is available
//!
bool VideoSource::isCamAvailable()
{
	return camAvailable;
}

//!
//!
//!
double VideoSource::getFPS()
{
	return cap->get(CV_CAP_PROP_FPS)/1000;
}