/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2012 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "CameraInputNode.h"
//! \brief Header file for CameraInputNode class.
//!
//! \author     Martin Schwarz <martin.schwarz@filmakademie.de>
//! \version    1.0
//! \date       30.01.2011 (last updated)
//!
//!
#ifndef CAMERAINPUTNODE_H
#define CAMERAINPUTNODE_H


#include "ImageNode.h"
#include "InstanceCounterMacros.h"
#include <QtCore/QThread>
#include "OgreManager.h"
#include "VideoSource.h"
#include <QWaitCondition>
#include <QMutex>
#include "GenericParameter.h"

#if (OGRE_PLATFORM  == OGRE_PLATFORM_WIN32)
	#include <windows.h>
#endif

using namespace Frapper;

//!
//! Class representing CameraInput
//!
class CameraInputNode : public ImageNode
{
    Q_OBJECT
	ADD_INSTANCE_COUNTER

public: /// constructors and destructors

    //!
    //! Constructor of the CameraInputNode class.
    //!
    //! \param name The name to give the new mesh node.
	//! \param parameterRoot The main Parameter Group of this Node
    //!
    CameraInputNode ( QString name, ParameterGroup *parameterRoot );

    //!
    //! Destructor of the CameraInputNode class.
    //!
    //! Defined virtual to guarantee that the destructor of a derived class
    //! will be called if the instance of the derived class is saved in a
    //! variable of its parent class type.
    //!
	virtual inline ~CameraInputNode ();

private: //methods

	//!
	//! Thread to get newFrame from Camera
	//!
	virtual void run();

	//!
	//! Is used for initialising or reinitialing
	//! \param CameraId this value is used to setup the camera 
	//!
	void initialise(int CameraId);
   
signals:

	//!
	//! This signal is used to trigger the method updateVideoTexture() from the runThread
	//!
	void newFrame();

private slots:

	//!
	//! Updates the videoTexture
	//!
	void updateVideoTexture();

	//!
	//! Sets Workerthread in sleeping mode or wakes the thread
	//!
	void sleepOrWakeupThread();

	//!
	//! Changes the Camera Source
	//!
	void changeCameraSource();

	//!
	//! Sets the time the thread should sleep after a execution
	//!
	void setSleepingThreadTime();

private: //data

	//!
    //! The camTexture
    //!
	Ogre::TexturePtr m_texture;

    //!
    //! Is termination of run-thread requested?
    //!
	bool m_endThreadRequested;

	//!
	//! used to save unique name for the cam texture
	//!
	Ogre::String m_uniqueRenderTextureName;

	//!
	//! VideoSource
	//!
	VideoSource* m_video;

	//!
	//! Mat from VideoSource color
	//!
	Mat m_colorImage;

	//!
	//! Mat from VideoSource raw
	//!
	Mat m_rawImage;

	//!
	//! Pointer m_colorImage for generic Parameter
	//!
	Mat* m_colorImagePtr;

	//!
	//! Pointer m_rawImage for generic Parameter
	//!
	Mat* m_rawImagePtr;

	//!
	//! Is used to save the size of the videoinput -> pSrc.rows*pSrc.cols*4
	//!
	int m_videoSize;

    //!
    //!  Image that is used if no Camera is available
    //!
	Ogre::Image m_defaultImage;

	//!
	//! WaitCondition is used to syncronize Mainthread with workerthread 
	//!
	QWaitCondition m_moreFrames;

	//!
	//! Is used by moreFrames to syncronize Mainthread with workerthread
	//!
	QMutex m_frameVideoMutex;

	//!
	//! The Generic Parameter for the colorMatrix
	//!
	GenericParameter *m_matrixParameter;

	//!
	//! The Generic Parameter for the rawMatrix
	//!
	GenericParameter *m_matrixRawParameter;

	//!
	//! The Picture Parameter
	//!
	Parameter *m_pictureParameter;	

	//!
	//! Is used to set the workerthread to sleep
	//!
	QWaitCondition m_sleepThread;

	//!
	//! If true the workthread gets to sleep
	//!
	bool m_shallThreadSleep;

	//!
	//! Stores the Choosen CameraID
	//!
	int m_CameraID;

	//!
	//! How long shall the thread sleep
	//!
	unsigned long m_sleepTime;
};
Q_DECLARE_METATYPE(cv::Mat*);

#endif
