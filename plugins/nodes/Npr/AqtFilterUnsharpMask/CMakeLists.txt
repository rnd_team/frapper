project(aqtfilterunsharpmask)
#set(CMAKE_CONFIGURATION_TYPES Debug Release CACHE STRING "" FORCE) 
SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# aqtfilterunsharpmask
set( aqtfilterunsharpmask_h ${aqtfilterunsharpmask_h}		
			AqtFilterUnsharpMaskNode.h
			AqtFilterUnsharpMaskNodePlugin.h
			)

set( aqtfilterunsharpmask_moc ${aqtfilterunsharpmask_moc}	
			AqtFilterUnsharpMaskNode.h
			AqtFilterUnsharpMaskNodePlugin.h
			)

set( aqtfilterunsharpmask_src ${aqtfilterunsharpmask_src}	
			AqtFilterUnsharpMaskNode.cpp
			AqtFilterUnsharpMaskNodePlugin.cpp
			)

set( aqtfilterunsharpmask_res ${aqtfilterunsharpmask_res}	
			aqtfilterunsharpmask.xml
			)

# Create moc files		   
qt4_wrap_cpp(aqtfilterunsharpmask_cxx ${aqtfilterunsharpmask_moc})

# Create source groups
source_group("Moc Files" FILES ${aqtfilterunsharpmask_cxx})
source_group("Header Files" FILES ${aqtfilterunsharpmask_h})
source_group("Resources" FILES ${aqtfilterunsharpmask_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(aqtfilterunsharpmask_src ${aqtfilterunsharpmask_src} ${aqtfilterunsharpmask_h} ${aqtfilterunsharpmask_res})
ENDIF(WIN32)

# Create static library
add_library(aqtfilterunsharpmask SHARED ${aqtfilterunsharpmask_src} ${aqtfilterunsharpmask_cxx})

# Add library dependencies
target_link_libraries(aqtfilterunsharpmask
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
install( FILES ${aqtfilterunsharpmask_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libaqtfilterunsharpmask_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libaqtfilterunsharpmask.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS aqtfilterunsharpmask RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
