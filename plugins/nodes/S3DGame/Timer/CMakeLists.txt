project(timer)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# timer
set( timer_h ${timer_h}
			timerNode.h
			timerNodePlugin.h
			)

set( timer_moc ${timer_moc}	
			timerNode.h
			timerNodePlugin.h
			)

set( timer_src ${timer_src}	
			timerNode.cpp
			timerNodePlugin.cpp
			)

set( timer_res ${timer_res}	
			timer.xml
			)

# Create moc files		   
qt4_wrap_cpp(timer_cxx ${timer_moc})

# Create source groups
source_group("Moc Files" FILES ${timer_cxx})
source_group("Header Files" FILES ${timer_h})
source_group("Resources" FILES ${timer_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(timer_src ${timer_src} ${timer_h} ${timer_res})
ENDIF(WIN32)

# Create static library
add_library(${PROJECT_NAME} SHARED ${timer_src} ${timer_cxx})

# Add library dependencies
target_link_libraries(${PROJECT_NAME}
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
install( FILES ${timer_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libtimer_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libtimer.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
