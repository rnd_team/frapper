/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2009 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; version 2.1 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
-----------------------------------------------------------------------------
*/

//!
//! \file "TimerNode.cpp"
//! \brief Implementation file for TimerNode class.
//!
//! \author     Michael Bu�ler <michael.bussler@filmakademie.de>
//! \author     Simon Spielmann <simon.spielmann@filmakademie.de>
//! \version    1.0
//! \date       16.12.2011 (last updated)
//!

#include "TimerNode.h"
#include <QDateTime>
#include <QTimer>

///
/// Constructors and Destructors
///


//!
//! Constructor of the TimerNode class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
TimerNode::TimerNode ( const QString &name, ParameterGroup *parameterRoot ) :
    Node(name, parameterRoot),
        m_pTimer(NULL),
        m_pSystemTime(NULL),
        m_pNormalizedTime(NULL),
        m_pNormalizedTimeInterval(NULL),
        m_pUpdate(NULL),
        m_baseTime(0),
        m_interval(0)
{
    // store references to parameters for faster access
    m_pSystemTime = dynamic_cast<NumberParameter *>(getParameter("SystemTime"));
    assert(m_pSystemTime);

    m_pNormalizedTime = dynamic_cast<NumberParameter *>(getParameter("NormalizedTime"));
    assert(m_pNormalizedTime);

    m_pNormalizedTimeInterval = dynamic_cast<NumberParameter *>(getParameter("NormalizedTimeInterval"));
    assert(m_pNormalizedTimeInterval);

    m_pUpdate = getParameter("Update");
    assert(m_pUpdate);

    // Override default range given by NumberParameter
    m_pSystemTime->setMinValue(0);
    m_pSystemTime->setMaxValue(std::numeric_limits<int>::max()); // STL needs to be used here, as there is no analogon in Qt

    // create a new QTimer
    m_pTimer = new QTimer(this);
    connect (m_pTimer, SIGNAL(timeout()), this, SLOT(timerEvent()));

    // Connect change functions of parameters with timer activation
	setChangeFunction("Active", SLOT(activateTimer()));
    setChangeFunction("Interval", SLOT(changeInterval()));

    // store the current interval length
    m_interval = getIntValue("Interval");
}


//!
//! Destructor of the TimerNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
TimerNode::~TimerNode ()
{
    delete m_pTimer;
}

void TimerNode::timerEvent()
{
    // get current system time in msec
    const int currentTime = QTime().msecsTo(QTime::currentTime());

    // write system time to "SystemTime" parameter
    m_pSystemTime->setValue(QVariant(currentTime), true);

    // check if current time is within range of the normalized time
    int normalizedTimeInterval = m_pNormalizedTimeInterval->getValue().toInt();
    if ((currentTime - m_baseTime) > normalizedTimeInterval )
        m_baseTime = currentTime;

    float normalizedTime = (currentTime - m_baseTime ) / (float) normalizedTimeInterval;
    m_pNormalizedTime->setValue(QVariant(normalizedTime), true);

    // trigger update chain
    m_pUpdate->getValue(true);
}

void TimerNode::activateTimer()
{
    const bool activeParameter = getBoolValue("Active");

    if( activeParameter ) {
        m_pTimer->start(m_interval);
    } else {
        m_pTimer->stop();
        m_baseTime = 0;
    }
}

void TimerNode::changeInterval()
{
    // update timer interval
    m_interval = getIntValue("Interval");
    m_pTimer->setInterval(m_interval);
}
