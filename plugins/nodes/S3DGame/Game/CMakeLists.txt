
project(S3DGame)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )

# s3dgame
set( s3dgame_h ${s3dgame_h}
	S3DGame.h
	S3DGameEngine.h
	S3DGamePaths.h
	S3DGameNode.h
	S3DGameNodePlugin.h
	)

set( s3dgame_moc ${s3dgame_moc}	
	S3DGame.h
	S3DGameNode.h
	S3DGameNodePlugin.h
	)

set( s3dgame_src ${s3dgame_src}	
	S3DGame.cpp
	S3DGameEngine.cpp
	S3DGameNode.cpp
	S3DGameNodePlugin.cpp
	)

set( s3dgame_res 
     s3dgame.xml
	 )

# Create moc files
qt4_wrap_cpp(s3dgame_cxx ${s3dgame_moc})

# Create source groups
source_group("Moc Files" FILES ${s3dgame_cxx})
source_group("Header Files" FILES ${s3dgame_h})
source_group("Resources" FILES ${s3dgame_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(s3dgame_src ${s3dgame_src} ${s3dgame_h} ${s3dgame_res})
ENDIF(WIN32)

# Create static library
add_library(S3DGame SHARED ${s3dgame_src} ${s3dgame_cxx})

# Add library dependencies
target_link_libraries( S3DGame
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
install( FILES ${s3dgame_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libS3DGame_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libS3DGame.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS S3DGame RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")