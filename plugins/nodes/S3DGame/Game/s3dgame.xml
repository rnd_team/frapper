<?xml version="1.0" encoding="utf-8" ?>
<!--
  Project:      Filmakademie Application Framework
  File:         s3dgame.xml
  Description:  Contains the XML description of S3DGame nodes.
  Author:       Michael Bußler <michael.bussler@filmakademie.de>
  Copyright:    (c) 2011 Filmakademie Baden-Württemberg
-->

<!DOCTYPE nodetype SYSTEM "nodetype.dtd">
<nodetype name="S3DGame" category="S3D" color="40, 40, 40" plugin="S3DGame.dll">
  <parameters>
  <!-- input parameter -->
    <parameter name="P1_Action" type="Bool" pin="in" visible="true" selfEvaluating="true"/>
    <parameter name="P2_Action" type="Bool" pin="in" visible="true" selfEvaluating="true"/>
    <parameter name="Time"      type="Int"      pin="in" visible="false"/>

    <parameters name="Input Level" visible="false">
      <parameter name="Level"          type="Geometry" pin="in" visible="false"/>
      <parameter name="Player1"        type="Geometry" pin="in" visible="false"/>
      <parameter name="Player2"        type="Geometry" pin="in" visible="false"/>
      <parameter name="Logic"          type="Geometry" pin="in" visible="false"/>
      <parameter name="ObjectMarker"   type="Geometry" pin="in" visible="false"/>
    </parameters>

    <parameter name="PlayerCharacter"      type="Geometry" pin="in" multiplicity="*"/>
    <parameter name="PlayerCharacterLight" type="Light"    pin="in" multiplicity="*"/>

    <parameter name="Reset1" type="Bool" pin="in" defaultValue="false" selfEvaluating="true" visible="false"/>
    <parameter name="Reset2" type="Bool" pin="in" defaultValue="false" selfEvaluating="true" visible="false"/>
    
    <parameter name="Copy Animated Objects" type="Command"/>
    <parameter name="Reset" type="Command"/>
    <parameters name="Animated Objects">
      <parameter name="Platform1"       type="Geometry" pin="in" />
      <parameter name="Platform2"       type="Geometry" pin="in" />
      <parameter name="Door"            type="Geometry" pin="in" />
      <parameter name="Windwheel"       type="Geometry" pin="in" />
      <parameter name="Windwheel_Small" type="Geometry" pin="in" />
      <parameter name="Sign"            type="Geometry" pin="in" />
      <parameter name="Wheel"           type="Geometry" pin="in" />
      <parameter name="Windwheel_Big"   type="Geometry" pin="in" />
    </parameters>

    <!-- putput parameter -->
    <parameter name="ShowGameLogic"     type="Bool"    defaultValue="false"/>
    <parameter name="ShowLightGeometry" type="Bool"    defaultValue="true"/>
    <parameter name="StepSize"       type="Int"   minValue="10"   maxValue="100"    defaultValue="40"     stepSize="1"   inputMethod="SliderPlusSpinBox"/>
    <parameter name="IntervalLength" type="Int"   minValue="1"    maxValue="100"    defaultValue="20"    stepSize="10" inputMethod="SliderPlusSpinBox"/>
    <parameter name="GameInterval"   type="Int"   minValue="1000" maxValue="20000"  defaultValue="2000"   stepSize="100" inputMethod="SliderPlusSpinBox"/>
    <parameter name="PCScale"        type="Float" minValue="0.1"  maxValue="10.0"   defaultValue="0.5"    stepSize="0.1" inputMethod="SliderPlusSpinBox"/>
    <parameter name="Obstacle Scale" type="Float" minValue="0.1"  maxValue="10.0"   defaultValue="1.0"    stepSize="0.1" inputMethod="SliderPlusSpinBox"/>
    <parameter name="AnimScale"      type="Float" minValue="1.0"  maxValue="1000.0" defaultValue="100.0"  stepSize="1.0" inputMethod="SliderPlusSpinBox"/>
    <parameter name="LavaStep"       type="Float" minValue="0.001"  maxValue="0.2"   defaultValue="0.01"   stepSize="0.001" inputMethod="SliderPlusSpinBox"/>
    <parameter name="JumpAnimLength" type="Int"   minValue="60"   maxValue="10000"   defaultValue="300"    stepSize="50"  inputMethod="SliderPlusSpinBox"/>
    <parameter name="WalkAnimLength" type="Int"   minValue="60"   maxValue="10000"   defaultValue="300"    stepSize="50"  inputMethod="SliderPlusSpinBox"/>
    <parameter name="FallAnimLength" type="Int"   minValue="60"   maxValue="10000"   defaultValue="300"    stepSize="50"  inputMethod="SliderPlusSpinBox"/>
    <parameter name="DeathAnimLength" type="Int"   minValue="60"   maxValue="10000"   defaultValue="1000"    stepSize="50"  inputMethod="SliderPlusSpinBox"/>
    <parameter name="Platform1Time" type="Int"   minValue="50"   maxValue="10000"   defaultValue="1000"    stepSize="100"  inputMethod="SliderPlusSpinBox"/>
    <parameter name="Platform2Time" type="Int"   minValue="50"   maxValue="10000"   defaultValue="2000"    stepSize="100"  inputMethod="SliderPlusSpinBox"/>
    <parameter name="Platform3Time" type="Int"   minValue="50"   maxValue="10000"   defaultValue="2000"    stepSize="100"  inputMethod="SliderPlusSpinBox"/>
    <parameter name="StartPath"     type="String" defaultValue="Path1" />
    
    <parameters name="Camera">
    <parameter name="CameraTranslate" type="Float" size="3" inputMethod="SliderPlusSpinBox" minValue="-100" maxValue="100" defaultValue="0, 0, 0" stepSize="0.25" />
    <parameter name="CameraRotate"    type="Float" size="3" inputMethod="SliderPlusSpinBox" minValue="-180" maxValue="180" defaultValue="0, 0, 0" stepSize="0.25" />
  </parameters>

  <!-- output parameter -->
    <parameter name="Timer" type="Generic" pin="out" />
    <parameter name="Stopwatch" type="String" pin="out" />

    <!-- output animations -->
    <parameters name="Animations">
      <parameter name="DeathAnim"     type="Float" pin="out" defaultValue="0.0" minValue="0.0" maxValue="1000.0" />
      <parameter name="WalkAnim"      type="Float" pin="out" defaultValue="0.0" minValue="0.0" maxValue="1000.0" />
      <parameter name="JumpAnim"      type="Float" pin="out" defaultValue="0.0" minValue="0.0" maxValue="1000.0" />
      <parameter name="FallAnim"      type="Float" pin="out" defaultValue="0.0" minValue="0.0" maxValue="1000.0" />
      <parameter name="DiveAnim"      type="Float" pin="out" defaultValue="0.0" minValue="0.0" maxValue="1000.0" />
      <parameter name="JumpStartAnim" type="Float" pin="out" defaultValue="0.0" minValue="0.0" maxValue="1000.0" />
      <parameter name="JumpEndAnim"   type="Float" pin="out" defaultValue="0.0" minValue="0.0" maxValue="1000.0" />
      <parameter name="DoorOpenAnim"  type="Float" pin="out" />
      <parameter name="Platform1Anim" type="Float" pin="out" />
      <parameter name="Platform2Anim" type="Float" pin="out" />
      <parameter name="Platform3Anim" type="Float" pin="out" />
      <parameter name="Rings"         type="Float" pin="out" defaultValue="100.0"/>
      <parameter name="EndScreen"     type="Float" pin="out" defaultValue="0.0"/>
      <parameter name="WaitIdleAnim"  type="Bool"  pin="out" />
      <parameter name="JumpIdleAnim"  type="Bool"  pin="out" />
    </parameters>

    <!-- output geometry -->
    <parameters name="Output Geometry">
      <parameter name="Game_P1"   type="Geometry" pin="out" />
      <parameter name="Game_P2"   type="Geometry" pin="out" />
      <parameter name="Game_Both" type="Geometry" pin="out" />
    </parameters>
    <parameters name="PlayerCharacterData">
      <parameter name="Position"    type="Float" size="3" minValue="-100" maxValue="100" defaultValue="0, 0, 0" stepSize="1.0" pin="out" visible="false"/>
      <parameter name="Orientation" type="Float" size="3" minValue="-360" maxValue="360" defaultValue="0, 0, 0" stepSize="1.0" pin="out" visible="false"/>
    </parameters>
  </parameters>
</nodetype>
