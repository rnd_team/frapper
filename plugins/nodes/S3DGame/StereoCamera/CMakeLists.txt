project(stereocamera)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

# StereoCamera
set( StereoCamera_h ${StereoCamera_h}
			StereoCamera.h
			StereoCameraPlugin.h
			)

set( StereoCamera_moc ${StereoCamera_moc}
			StereoCamera.h
			StereoCameraPlugin.h
			)

set( StereoCamera_src ${StereoCamera_src}	
			StereoCamera.cpp
			StereoCameraPlugin.cpp
			)
			
set( StereoCamera_res ${StereoCamera_res}	
			stereocamera.xml
			)

# Create moc files
qt4_wrap_cpp(StereoCamera_cxx ${StereoCamera_moc})

# Create source groups
source_group("Moc Files" FILES ${StereoCamera_cxx})
source_group("Header Files" FILES ${StereoCamera_h})
source_group("Resources" FILES ${StereoCamera_res})

# Add header files to sources to make headers visible in Visual Studio
IF (WIN32)
  set(StereoCamera_src ${StereoCamera_src} ${StereoCamera_h} ${StereoCamera_res})
ENDIF(WIN32)

# Create static library
add_library( 
	${PROJECT_NAME} 
	SHARED 
	${StereoCamera_src} 
	${StereoCamera_cxx}
)

# Add library dependencies
target_link_libraries( 
	${PROJECT_NAME}
	${QT_LIBRARIES}
	${OGRE_LIBRARIES}
	optimized frappercore debug frappercore_d
)

# Install files

install( FILES ${StereoCamera_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
install( TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
