/*
-----------------------------------------------------------------------------
This source file is part of FRAPPER
research.animationsinstitut.de
sourceforge.net/projects/frapper

Copyright (c) 2008-2010 Filmakademie Baden-Wuerttemberg, Institute of Animation 

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

//!
//! \file "TextRender.cpp"
//! \brief Implementation file for TextRender class.
//!
//! \author     Michael Bu�ler <michael.bussler@filmakademie.de>
//! \version    0.1
//! \date       07.02.2012 (created)
//! \date       07.02.2012 (last updated)
//!

#include "TextRenderNode.h"
#include "OgreManager.h"
#include "Node.h"
#include <QPainter>

///
/// Constructors and Destructors
///

//!
//! Constructor of the TextRender class.
//!
//! \param name The name for the new node.
//! \param parameterRoot A copy of the parameter tree specific for the type of the node.
//!
TextRenderNode::TextRenderNode ( const QString &name, ParameterGroup *parameterRoot ) :
    ImageNode( name, parameterRoot)
{

    setChangeFunction("Text", SLOT(processOutputImage()));
    setChangeFunction("Font Size", SLOT(processOutputImage()));
    setChangeFunction("Position X", SLOT(processOutputImage()));
    setChangeFunction("Position Y", SLOT(processOutputImage()));

    Parameter *outputImageParameter = getParameter(m_outputImageName);
    if (outputImageParameter) {
        outputImageParameter->setProcessingFunction(SLOT(processOutputImage()));
        outputImageParameter->setDirty(true);
        // set up parameter affections
        outputImageParameter->addAffectingParameter(getParameter("Text"));
    }


}

void TextRenderNode::processOutputImage()
{

    unsigned int width = getUnsignedIntValue("Resolution > Width");
    unsigned int height = getUnsignedIntValue("Resolution > Height");

    QString text = this->getStringValue("Text");
    int xPos = this->getIntValue("Position X");
    int yPos = this->getIntValue("Position Y");
    int fontsize = this->getIntValue("Font Size");
    QString name = this->getStringValue("Font Name");
    QColor color = this->getColorValue("Font Color");
    bool bold = this->getBoolValue("Bold");

    QImage qImage(QSize(width, height), QImage::Format_ARGB32);
    qImage.fill(Qt::transparent);
    
    QPainter painter( &qImage);

    QFont font;
    font.setFamily(name);
    font.setPixelSize(fontsize);
    font.setBold(bold);
    painter.setFont(font);
    painter.setPen(color);

    painter.drawText(xPos, yPos+fontsize, text);

    Ogre::Image* oImage = new Ogre::Image();

    oImage->loadDynamicImage(
        qImage.bits(), 
        qImage.width(), 
        qImage.height(), 
        Ogre::PF_A8B8G8R8 );

    this->setOutputImage(oImage);

}

//!
//! Destructor of the TextRenderNode class.
//!
//! Defined virtual to guarantee that the destructor of a derived class
//! will be called if the instance of the derived class is saved in a
//! variable of its parent class type.
//!
TextRenderNode::~TextRenderNode ()
{
}
