project(sceneLoader)

SET(CMAKE_DEBUG_POSTFIX _d)

include_directories(
			${CMAKE_CURRENT_SOURCE_DIR}
			)

set( sceneLoader_h ${sceneLoader_h}		
			SceneLoaderNode.h
			SceneLoaderNodePlugin.h
      DotsceneLoader.h
			tinystr.h
      tinyxml.h
			)

set( sceneLoader_moc ${sceneLoader_moc}	
			SceneLoaderNode.h
			SceneLoaderNodePlugin.h
			)

set( sceneLoader_src ${sceneLoader_src}	
			SceneLoaderNode.cpp
			SceneLoaderNodePlugin.cpp
      DotSceneLoader.cpp
      tinyxml.cpp
      tinystr.cpp
      tinyxmlerror.cpp
      tinyxmlparser.cpp
			)

set( sceneLoader_res ${sceneLoader_res}	
			sceneLoader.xml
			)

# Create moc files		   
qt4_wrap_cpp(sceneLoader_cxx ${sceneLoader_moc})

# Create source groups
source_group("Moc Files" FILES ${sceneLoader_cxx})
source_group("Header Files" FILES ${sceneLoader_h})
source_group("Resources" FILES ${sceneLoader_res})

# Add header files to sources to make headers visible in Visual Studio
set(sceneLoader_src ${sceneLoader_src} ${sceneLoader_h} ${sceneLoader_res})

# Create static library
add_library(${PROJECT_NAME} SHARED ${sceneLoader_src} ${sceneLoader_cxx})

# Add library dependencies
target_link_libraries(${PROJECT_NAME}
			${QT_LIBRARIES}
			${OGRE_LIBRARIES}
			optimized frappercore debug frappercore_d
			)

# Install files
install( FILES ${sceneLoader_res} DESTINATION ${NODE_PLUGIN_INSTALL} )
if (MINGW)
if (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libsceneLoader_d.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
else (CMAKE_BUILD_TYPE MATCHES Debug)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libsceneLoader.dll DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (CMAKE_BUILD_TYPE MATCHES Debug)
else (MINGW)
install( TARGETS sceneLoader RUNTIME DESTINATION ${NODE_PLUGIN_INSTALL} )
endif (MINGW)

# Set Solution folder in Visual Studio
set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER ${VS_PROJECT_FOLDER})
message( STATUS " * Adding project ${PROJECT_NAME} to folder ${VS_PROJECT_FOLDER}")
