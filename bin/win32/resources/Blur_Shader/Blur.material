vertex_program Blur/QuadVP cg
{
	source Blur.cg
	profiles vs_1_1 arbvp1
	entry_point quad_vs
	default_params
    {
        param_named_auto worldViewProj worldviewproj_matrix
    }
}

fragment_program Blur/SceneFP cg
{
    source ../CompositorTemplateNode_Shader/CompositorTemplate.cg
    entry_point scene_ps
    profiles ps_3_0 fp40
}


fragment_program Blur/BoxBlurFP cg
{
    source Blur.cg
    entry_point boxBlur_ps
    profiles ps_3_0 fp40
}

fragment_program Blur/BlurHFP cg
{
    source Blur.cg
    entry_point blurH_ps
    profiles ps_3_0 fp40
}

fragment_program Blur/BlurVFP cg
{
    source Blur.cg
    entry_point blurV_ps
    profiles ps_3_0 gp4fp
}


// scene pass
material Blur/ScenePass
{
	technique
	{
		pass
		{
			cull_hardware none
			cull_software none
			depth_check off

			fragment_program_ref Blur/SceneFP
			{
			}
			texture_unit
			{
				tex_coord_set 0
				tex_address_mode clamp
				filtering none
			}
		}
	}
}


material Blur/BoxBlurMaterial
{
	technique
	{
		pass
		{
			cull_hardware none
			cull_software none
			depth_check off

			fragment_program_ref Blur/BoxBlurFP
			{
			}
						
			texture_unit	//(inputMap0)
			{
                tex_coord_set 0
				tex_address_mode clamp
				filtering none
			}
		}
	}
}


//Blur: Horizontal Gaussian pass
material Blur/BlurH
{
	technique
	{
		pass
		{
			cull_hardware none
			cull_software none
			depth_check off

			fragment_program_ref Blur/BlurHFP
			{
				param_named BlurWidth float 1.0
			}
			texture_unit
			{
				tex_coord_set 0
				tex_address_mode clamp
				filtering bilinear
			}
		}
	}
}

//Blur: Vertical Gaussian pass
material Blur/BlurV
{
	technique
	{
		pass
		{
			cull_hardware none
			cull_software none
			depth_check off

			fragment_program_ref Blur/BlurVFP
			{
				param_named BlurWidth float 1.0
			}
			texture_unit
			{
				tex_coord_set 0
				tex_address_mode clamp
				filtering bilinear
			}
		}
	}
}

//Final Bloom pass: Blends the blurred with the sharp image
material Blur/Out
{
    technique
    {
        pass
        {
			cull_hardware none
			cull_software none
			depth_check off

			fragment_program_ref Blur/SceneFP
			{
			}
			texture_unit
			{
				tex_coord_set 0
				tex_address_mode clamp
				filtering bilinear
			}
		}
    }
}