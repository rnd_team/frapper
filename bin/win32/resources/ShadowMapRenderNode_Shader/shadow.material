vertex_program ShadowMapRender/base_vs cg
{
    source shadow.cg
    profiles vs_1_1 arbvp1
    entry_point base_vs
}

vertex_program ShadowMapRender/lightZ_vs cg
{
    source shadow.cg
    profiles vs_2_0 arbvp1 vp20
    entry_point lightZ_vs
}

vertex_program ShadowMapRender/shadow_vs cg
{
    source shadow.cg
	profiles vs_2_0 arbvp1 vp20
    entry_point shadow_vs
}

fragment_program ShadowMapRender/base_fs cg
{
    source shadow.cg
    profiles ps_2_0 arbfp1
    entry_point base_fs
}

fragment_program ShadowMapRender/lightZ_fs cg
{
    source shadow.cg
    profiles ps_2_0 arbfp1 fp20
    entry_point lightZ_fs
}

fragment_program ShadowMapRender/shadow_fs cg
{
    source shadow.cg
    profiles ps_3_0 fp40
    entry_point shadow_fs
}


material ShadowMapRender/Base
{
	technique
	{
	    pass
	    {
	        vertex_program_ref ShadowMapRender/base_vs
	        {
				param_named_auto wvpMat worldviewproj_matrix
	        }
	    }
	}
}

// this is the technique for the scheme "LightZ"
material ShadowMapRender/LightZ
{
	technique
	{
	    pass
	    {
	    	transparent_sorting off
	        vertex_program_ref ShadowMapRender/lightZ_vs  
	        {
	        	param_named_auto wvpMat worldviewproj_matrix
				param_named_auto depthRange scene_depth_range
	        }

	        fragment_program_ref ShadowMapRender/lightZ_fs
	        {
	        }
	    }
	}
}

// this is the technique for the scheme "Shadow" 
material ShadowMapRender/Shadow
{
	scheme Shadow
	technique
	{
		pass
	    {
			transparent_sorting off
            vertex_program_ref ShadowMapRender/base_vs  
            {
				param_named_auto wvpMat worldviewproj_matrix
            }
	    }

	    pass
	    {
			transparent_sorting off
			max_lights 16
			scene_blend add
            iteration once_per_light

            vertex_program_ref ShadowMapRender/shadow_vs
	        {
				param_named_auto wMat world_matrix
				param_named_auto wvpMat worldviewproj_matrix
				param_named_auto tvpMat texture_viewproj_matrix 0
				param_named_auto spotDir light_direction_object_space 0
				param_named_auto depthRange shadow_scene_depth_range 0
	        }

	        fragment_program_ref ShadowMapRender/shadow_fs
	        {
				param_named_auto lightDif0 light_diffuse_colour 0
				param_named_auto lightPos0 light_position 0
				param_named_auto lightAtt0 light_attenuation 0
				param_named_auto invSMSize inverse_texture_size 0
				param_named_auto spotlightParams spotlight_params 0
				param_named vsmEpsilon float 0.0001
				param_named sampleRadius float 2
				param_named blurSize float 1
				param_named lightPower float 1
	        }

			texture_unit ShadowMap
			{
				content_type shadow
                filtering anisotropic
                max_anisotropy 8
                tex_address_mode border
                tex_border_colour 1 1 1
			}
		}
	}
}